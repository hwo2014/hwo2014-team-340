package noobbot.engine;

import java.util.ArrayList;

public class Route implements Comparable<Route>{
	private double length;
	private ArrayList<Integer> lanes;
	
	public Route(double length, ArrayList<Integer> lanes) {
		this.length = length;
		this.lanes = lanes;
	}
	
	public Route(Route other) {
		this(other.getLength(), new ArrayList<Integer>(other.getLanes()));
	}
	
	public void addNewNode(double length, int lane) {
		this.length += length;
		this.lanes.add(lane);
	}
	
	public double getLength() {
		return this.length;
	}
	
	public ArrayList<Integer> getLanes() {
		return this.lanes;
	}

	@Override
	public int compareTo(Route other) {
	    return Double.compare(this.length, ((Route) other).getLength());
	}
}
