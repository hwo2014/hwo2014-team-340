package noobbot.engine;

import java.util.ArrayList;
import java.util.Random;

import noobbot.network.pojo.Car;
import noobbot.network.pojo.PiecePositionLane;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;

public class RouteCommander {
	
	public enum RouteMode {
		SHORTESTROUTE, CHOOSERANDOMLANE, NOTTHESHORTEST, NOTTHECOMMON
	}
	
	//private AIState state;
	private Random randomGenerator;
	private RaceState raceState;
	private RouteFinder routeFinder;
	private Track track;
	
	private boolean useRouting;
	private boolean waitingForRoute;
	private int laneCommand;
	
	private int latestSwitchIndex;
	
	private RouteMode routeMode;
	
	public RouteCommander(RaceState state, Track track) {
		this.routeFinder = new RouteFinder(track);
		this.raceState = state;
		this.track = track;
		//this.track.outputTrack();
		
		this.routeMode = RouteMode.NOTTHECOMMON;
		//this.routeMode = RouteMode.CHOOSERANDOMLANE;
		
		this.waitingForRoute = true;
		this.useRouting = true;
		
		this.randomGenerator = new Random();
	}

	public boolean handleRouting() {
		//raceState.getMyLane().getEndLaneIndex()
		if (this.useRouting /*&& state.getCurrentLane() != -1*/) {
			return this.isNewRouteCommandNeeded();
		}
		
		return false;
	}
	
	public boolean isGoodTimeForTurbo(int pieceIndex) {
		if (!this.raceState.isMyTurboAvailable() || this.raceState.isMyCarCrashed() 
				|| this.track.getPieces()[pieceIndex].getRadius() != 0  
				|| this.raceState.isMyTurboActive())
			return false;
		
		double distance[] = track.getDistanceToNextTurn(pieceIndex, 0, 0);
		
		//use "epsilon" 1 in the comparison due to floating point rounding
		if (distance[0] >= this.routeFinder.longestStraightLength()-1.0d) {
			System.out.println("Index: "+ pieceIndex +"Distance to turn: "+distance[0]+" longest: "+this.routeFinder.longestStraightLength());
			return true;
		}
		
		return false;
	}
	
	public PiecePositionLane getLaneForNextPiece(int pieceIndex, PiecePositionLane currentLane) {
		TrackPiece nextPiece = track.getNextPiece(pieceIndex);
		
		if (!nextPiece.hasLaneSwitch())
			return new PiecePositionLane(currentLane.getEndLaneIndex(), currentLane.getEndLaneIndex());
		else {
			int action = routeFinder.calculateNextLane(pieceIndex, 
					currentLane.getEndLaneIndex(), 0);
			return new PiecePositionLane(currentLane.getEndLaneIndex(), currentLane.getEndLaneIndex()+action); 
		}
	}
	
	public boolean isNewRouteCommandNeeded() {
		int currentPieceIndex = raceState.getMyPiecePosition().getPieceIndex();
		TrackPiece nextPiece = track.getNextPiece(currentPieceIndex);
		
		if (raceState.getMyTrackPiece().hasLaneSwitch() && latestSwitchIndex != currentPieceIndex) {
			waitingForRoute = true;
			latestSwitchIndex = currentPieceIndex;
		}
		
		//process lane commands only if next piece is switch
		if (!waitingForRoute || !nextPiece.hasLaneSwitch())
			return false;
		
		//Calculate new route
		laneCommand = routeFinder.calculateNextLane(currentPieceIndex, raceState.getMyLane().getEndLaneIndex(), 0);
		
		Car front = this.raceState.getCarInFrontOfMe();
		int nextPieceIndex = this.track.getNextPieceIndex(currentPieceIndex);
		
		int frontPosition = -1;
		int frontLane = -1;
		boolean frontIsInNextPiece = false;
		boolean frontIsInCurrentPiece = false;
		if (front != null && !this.raceState.isCarCrashed(front)) {
			frontPosition = this.raceState.getPiecePosition(front).getPieceIndex();
			frontLane = this.raceState.getLane(front).getEndLaneIndex();
			frontIsInNextPiece = frontPosition == nextPieceIndex;
		}
		
		frontIsInCurrentPiece = frontPosition  == currentPieceIndex;
		
		//Check that we are truly behind
		if (frontIsInCurrentPiece) {
			if (this.raceState.getPiecePosition(front).getInPieceDistance() < this.raceState.getMyPiecePosition().getInPieceDistance())
				frontIsInCurrentPiece = false;
		}
		
		
		
		//Check for overtaking if the front opponent is in the current or in the next piece 
		if (frontIsInCurrentPiece || frontIsInNextPiece) {
			//next piece will be the switch
			int commonLane = this.raceState.getMostCommonLane(front, nextPieceIndex);
			int myCurrentLane = this.raceState.getMyLane().getEndLaneIndex();
			if (commonLane != -1 && this.routeMode == RouteMode.NOTTHECOMMON &&
					(
						(myCurrentLane + laneCommand == commonLane &&			//We are going to the same lane
						this.possileToGetToSameLane(frontIsInCurrentPiece, myCurrentLane, frontLane)) ||
						(frontIsInNextPiece && myCurrentLane + laneCommand == frontLane)
					)
				)
				checkLaneRouteOptions(myCurrentLane-commonLane);
		}
				
		
		if (this.routeMode == RouteMode.NOTTHESHORTEST)
			checkLaneRouteOptions(laneCommand);
		else if(this.routeMode == RouteMode.CHOOSERANDOMLANE)
			checkLaneRouteOptions(-99);
		
		return true;
	}
	
	//It is physically possible that we end up to same lane
	private boolean possileToGetToSameLane(boolean bothInSamePiece, int myCurrentLane, int OpponentCurrentLane) {
		if (bothInSamePiece) {
			return Math.abs(myCurrentLane + laneCommand - OpponentCurrentLane) <= 1;
		}
		else if (myCurrentLane + laneCommand == OpponentCurrentLane) {
			return true;
		}
		
		return false;
	}
	
	public RouteMode getRouteMode() {
		return routeMode;
	}

	public void setRouteMode(RouteMode routeMode) {
		this.routeMode = routeMode;
	}

	public void checkLaneRouteOptions(int laneCommandToAvoid) {
		int currentLane = raceState.getMyLane().getEndLaneIndex();
		ArrayList<Integer> possibleCommands = new ArrayList<Integer>();
		for (int i = -1; i < 2; i++) {
			if (currentLane+i >= 0 && currentLane+i < track.getLanes().length) {
				if (laneCommandToAvoid == i)
					continue;
				else
					possibleCommands.add(i);
			}
		}
		
		int index = randomGenerator.nextInt(possibleCommands.size());
		this.laneCommand = possibleCommands.get(index);
	}

	public int getLaneCommand() {
		return laneCommand;
	}

	public boolean isWaitingForRoute() {
		return waitingForRoute;
	}

	public void setWaitingForRoute(boolean waitingForRoute) {
		this.waitingForRoute = waitingForRoute;
	}
}
