package noobbot.engine;

import java.util.ArrayList;
import java.util.List;

import noobbot.network.ConnectionListener;
import noobbot.network.MsgWrapper;
import noobbot.network.out.SendMsg;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TurboAvailable;

public class Telemetry implements ConnectionListener {
	
	private List<MsgWrapper> allMessages = new ArrayList<MsgWrapper>();
	private double previousInPieceDistance;
	
	public Telemetry() {
		previousInPieceDistance = 0.0f;
    }

	@Override
	public void onRawMessage(MsgWrapper rawMsg) {
		allMessages.add(rawMsg);
	}

	@Override
	public SendMsg onGameTick(int gameTick) {
		if(gameTick % 100 == 0 || hasLineBreak) {
			log(gameTick + "", true, false);
		} else {
			log(".", false, false);
		}
		return null;
	}

	@Override
	public void onYourCar(Car car) {
		log("Our car is: " + car.getName() + " (" + car.getColor() + ")");
	}

	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
	}

	@Override
	public void onGameStart() {
		log("GO!");
	}

	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		// TODO Temporary only; this is not necessarily our car
		CarPosition myPosition = carPositions[0];
		if(myPosition.getAngle() > 50.0f) {
			double speed = 0.0f;
			if(myPosition.getPiecePosition().getInPieceDistance() > 
					previousInPieceDistance) {
				speed = myPosition.getPiecePosition().getInPieceDistance() - 
						previousInPieceDistance;
			}
			log("(Info) Angle: " + myPosition.getAngle() + 
					"\t Speed: " + speed);
			
			previousInPieceDistance = 
					myPosition.getPiecePosition().getInPieceDistance();
		}
	}

	@Override
	public void onCrash(Car car) {
		log("CRASH! (" + car.getName() + ")");
	}

	@Override
	public void onSpawn(Car car) {
		log("Spawn (" + car.getName() + ")");
	}

	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime,
			Ranking ranking) {
		log("Lap completed: " + car.getName() + " " + lapTime.toString());
	}

	@Override
	public void onDnf(Car car, String reason) {
	}

	@Override
	public void onFinish(Car car) {
		log(car.getName() + " finished");
	}

	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
		log("Game finished");
	}

	@Override
	public void onTournamentEnd() {
		log("Tournament finished");
	}
	
	public void outputAllMessages() {
		
		System.out.println("### MESSAGE DUMP ###");
		
		for (int i = 0; i < allMessages.size(); i++) {
			log(">>> "+allMessages.get(i).msgType+" <<<");
			log("" + allMessages.get(i).data);
		}
		
    }
	
	private boolean hasLineBreak = false;
	
	private void log(String msg, boolean breakBefore, boolean breakAfter) {
		if(!hasLineBreak && breakBefore) {
			msg = "\n" + msg;
		}
		if(breakAfter) {
			msg = msg + "\n";
		}
//		System.out.print(msg);
		hasLineBreak = breakAfter;
	}
	
	private void log(String msg) {
		log(msg, true, true);
	}

	@Override
	public void onTurboAvailable(TurboAvailable turbo) {
		// TODO Auto-generated method stub
		
	}
	
}
