package noobbot.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import noobbot.network.pojo.Lane;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;

public class RouteFinder {

	private List<TrackPiece> trackPieces;
	private List<Lane> lanes;

	private boolean containsSwitch;
	private int switchCount;
	
	private double longestStraight;
	
	private Track track;
	
	public double longestStraightLength() {
		return this.longestStraight;
	}
	
	public RouteFinder(Track track) {
		this.track = track;
		trackPieces = Collections.unmodifiableList(
				new ArrayList<TrackPiece>(Arrays.asList(track.getPieces())));
		lanes = Collections.unmodifiableList(
				new ArrayList<Lane>(Arrays.asList(track.getLanes())));
		
		containsSwitch = false;
		
		switchCount = 0;
		
		//Check that if the track contains at least one switch. Sanity check for calculateNextLane
		if (track.getLanes().length > 1) {
			for (TrackPiece piece: trackPieces) {
				if (piece.hasLaneSwitch() == true) {
					containsSwitch = true;
					switchCount++;
				}		
			}
		}
		
		
		//find out last corner
		int lastCornerIndex = 0;
		for (int i = this.trackPieces.size()-1; i > 0; i--) {
			if (this.trackPieces.get(i).getRadius() != 0) {
				lastCornerIndex = i;
				break;
			}
		}
		
		int iterationCount = 0;
		double currentStraightLength = 0.0d;
		int currentPiece = lastCornerIndex;
		while ( iterationCount < this.trackPieces.size()) {
			currentPiece++;
			if (currentPiece >= this.trackPieces.size()) 
				currentPiece = 0;
			
			if (this.trackPieces.get(currentPiece).getRadius() != 0) {
				currentStraightLength = 0.0d;
			}
			else
				currentStraightLength += this.trackPieces.get(currentPiece).getLength();
			
			if (currentStraightLength > this.longestStraight)
				this.longestStraight = currentStraightLength;
			
			iterationCount++;
		}
	}
	
	private int getNextSwitchIndex(int currentPiece) {
		//We have to be sure that there is a switch before going to the while loop
		if (containsSwitch == false)
			return -1;
		
		int i = currentPiece;
		while (true) {
			if (i >= trackPieces.size())
				i = 0;
			
			if (trackPieces.get(i).hasLaneSwitch() == true)
				return i;	
			
			i++;
		}
	}
	
	//Use strategy 0 for shortest route
	public int calculateNextLane(int currentPiece, int currentLane, int strategy) {
		//No need to do calculations if there is no switches
		if (containsSwitch == false)
			return 0;
		
		ArrayList<Route> routes = new ArrayList<Route>();
		Route startRoute = new Route(0.0f, new ArrayList<Integer>());
		
		//Calculate route lengths for one lap	
		calculateRouteLengths(routes, currentPiece, currentLane, switchCount, startRoute);
		
		Collections.sort(routes);
		
		int action = routes.get(strategy).getLanes().get(0) - currentLane;
		
		return action;
		
	}
	
	
	private void calculateRouteLengths(ArrayList<Route> routes, int currentPiece, int currentLane, int switchesToProcess, Route currentRoute) {
		
		if (switchesToProcess == 0) {
			routes.add(currentRoute);
			return;
		}
		
		//Start route calculation from next switch
		currentPiece = getNextSwitchIndex(currentPiece);
		
		for (int lane = currentLane - 1; lane <= currentLane+1; lane++) {
			if (lane < 0 || lane >= lanes.size())
				continue;
			
			Route newRoute = new Route(currentRoute);
			double length = calculateDistanceToNextSwitch(currentPiece+1, lane);
			
			/*double a = trackPieces.get(currentPiece).getLength();
			double b = lanes.get(lane).getDistanceFromCenter()-lanes.get(currentLane).getDistanceFromCenter();
			double switchPenalty = Math.sqrt(a*a+b*b);*/
			double laneSwitchLenght = trackPieces.get(currentPiece).getRouteDistance(currentLane, lane, 
					this.track.getLanes());
			
			newRoute.addNewNode(length+laneSwitchLenght, lane);
		
			calculateRouteLengths(routes, currentPiece+1, lane, switchesToProcess-1, newRoute);
		}
		
		
	}
	
	private double calculateDistanceToNextSwitch(int currentPiece, int currentLane) {
		
		//if we complete a lap in the calculation, start a new one
		if (currentPiece >= trackPieces.size())
			currentPiece = 0;
			
		TrackPiece currentPieceObject = trackPieces.get(currentPiece);
		if (currentPieceObject.hasLaneSwitch() == true)
			return 0.0f;
		
		double currentPieceLenght = calculatePieceLength(currentPieceObject, currentLane, lanes.toArray(new Lane[lanes.size()]));
		return currentPieceLenght + calculateDistanceToNextSwitch(currentPiece+1, currentLane);
	}
	
	public static double calculatePieceLength(TrackPiece currentPieceObject, int currentLane, Lane[] lanes) {
		if (currentPieceObject.getRadius() != 0) {	
			double adjustedRadius = currentPieceObject.getAdjustedRadius(currentLane, lanes);
			return MathHelper.calculateArcLength(Math.abs(currentPieceObject.getAngle()),adjustedRadius);
		}
		else
			return currentPieceObject.getLength();
	}
		
}
