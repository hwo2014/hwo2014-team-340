package noobbot.engine;

import noobbot.ai.BotAI;
import noobbot.network.ConnectionListener;
import noobbot.network.MsgWrapper;
import noobbot.network.out.SendMsg;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TurboAvailable;

public class BotManager implements ConnectionListener {

	private BotAI ai;
	private Telemetry telemetry;
	private RouteFinder routeFinder;
	private RaceState state;
	
	public BotManager(BotAI ai) {
		this.ai = ai;
		this.telemetry = new Telemetry();
		this.state = new RaceState();
		
		ai.setManager(this);
	}
	
	@Override
	public void onRawMessage(MsgWrapper rawMsg) {
		state.onRawMessage(rawMsg);
		telemetry.onRawMessage(rawMsg);
	};
	
	@Override
	public SendMsg onGameTick(int gameTick) {
		state.onGameTick(gameTick);
		telemetry.onGameTick(gameTick);
		return ai.onGameTick(gameTick);
	}

	@Override
	public void onYourCar(Car car) {
		state.onYourCar(car);
		telemetry.onYourCar(car);
		ai.onYourCar(car);
	}

	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
		this.routeFinder = new RouteFinder(track);
		state.onGameInit(track, cars, session);
		telemetry.onGameInit(track, cars, session);
		ai.onGameInit(track, cars, session);
	}

	@Override
	public void onGameStart() {
		state.onGameStart();
		telemetry.onGameStart();
		ai.onGameStart();
	}

	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		state.onCarPositions(carPositions);
		telemetry.onCarPositions(carPositions);
		ai.onCarPositions(carPositions);
	}

	@Override
	public void onCrash(Car car) {
		state.onCrash(car);
		telemetry.onCrash(car);
		ai.onCrash(car);
	}

	@Override
	public void onSpawn(Car car) {
		state.onSpawn(car);
		telemetry.onSpawn(car);
		ai.onSpawn(car);
	}

	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime,
			Ranking ranking) {
		state.onLapFinished(car, lapTime, raceTime, ranking);
		telemetry.onLapFinished(car, lapTime, raceTime, ranking);
		ai.onLapFinished(car, lapTime, raceTime, ranking);
	};

	@Override
	public void onDnf(Car car, String reason) {
		state.onDnf(car, reason);
		telemetry.onDnf(car, reason);
		ai.onDnf(car, reason);
	}

	@Override
	public void onFinish(Car car) {
		state.onFinish(car);
		telemetry.onFinish(car);
		ai.onFinish(car);
	}

	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
		state.onGameEnd(results, bestLaps);
		telemetry.onGameEnd(results, bestLaps);
		ai.onGameEnd(results, bestLaps);
	}

	@Override
	public void onTournamentEnd() {
		state.onTournamentEnd();
		telemetry.onTournamentEnd();
		ai.onTournamentEnd();
	}
	
	public RaceState getState() {
		return state;
	}
	
	public RouteFinder getRouteFinder() {
		return routeFinder;
	}

	@Override
	public void onTurboAvailable(TurboAvailable turbo) {
		state.onTurboAvailable(turbo);
		telemetry.onTurboAvailable(turbo);
		ai.onTurboAvailable(turbo);
	}
	
}
