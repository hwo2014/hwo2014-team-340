package noobbot.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.google.gson.Gson;

import noobbot.network.ConnectionListenerAdapter;
import noobbot.network.out.SendMsg;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.PiecePosition;
import noobbot.network.pojo.PiecePositionLane;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;
import noobbot.network.pojo.TurboAvailable;

public class RaceState extends ConnectionListenerAdapter {

	private int currentTick;
	
	private List<Car> cars =
			new ArrayList<Car>();
	private Map<Car, CarSpecs> carSpecs =
			new HashMap<Car, CarSpecs>();
	private Car myCar;
	private Track track;
	private RaceSession session;
	private Map<Car, CarDataPoint> carData =
			new HashMap<Car, CarDataPoint>();
	
	private boolean isInited = false;
	
	private boolean myTurboAvailable = false;
	private long myTurboLength = 0;
	private double nextTurboFactor = 1.0;
	private double myTurboFactor = 1.0;
	private long myTurboEndTick = Long.MIN_VALUE;
	
	private long numberOfAllCrashes = 0;
	
	@Override
	public SendMsg onGameTick(int gameTick) {
		this.currentTick = gameTick;
		return null;
	}

	@Override
	public void onYourCar(Car car) {
		this.myCar = car;
	}

	@Override
	public void onGameInit(Track track, CarSpecs[] pCars, RaceSession session) {

		isInited = true;
		
		this.track = track;
		
		for(CarSpecs spec : pCars) {
			Car car = spec.getCar();
			carSpecs.put(car, spec);
			cars.add(car);
			carData.put(car, new CarDataPoint());
		}
		
		myTurboAvailable = false;
		myTurboLength = 0;
		myTurboEndTick = Long.MIN_VALUE;
		
		this.session = session;
		
	}
	
	public boolean isGameInited() {
		return isInited;
	}

	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		for(CarPosition position : carPositions) {
			
			Car car = position.getId();
			CarDataPoint data = carData.get(car);
			
			CarPosition previousPosition = data.position;
			
			data.position = position;
			
			if (previousPosition == null || data.position.getPiecePosition().getPieceIndex() != 
					previousPosition.getPiecePosition().getPieceIndex()) {
				data.addLaneUsage(position);
			}

			// Only calculate the rest of the values starting from second tick
			if(previousPosition == null) {
				continue;
			}
			
			// Do not update any values if the car has crashed
			if(data.isCrashed) {
				continue;
			}
			
			double previousVelocity = data.velocity;
			double velocity = previousVelocity;
			
			double distance = getDistance(
					previousPosition.getPiecePosition(), 
					position.getPiecePosition());
			
			velocity = getVelocity(distance, 1);
			
			
			data.velocity = velocity;
			
			if(velocity > data.knownMaxVelocity) {
				data.knownMaxVelocity = velocity;
			}
			
			double acceleration = getAcceleration(previousVelocity, velocity, 1);
			data.acceleration = acceleration;
			
			if(acceleration > 0 && Math.abs(acceleration) > data.knownMaxAcceleration) {
				data.knownMaxAcceleration = Math.abs(acceleration);
			}
			
			// Do not update deceleration if the car just crashed
			if(acceleration < 0 && Math.abs(acceleration) > data.knownMaxDeceleration
					&& !data.isCrashed) {
				data.knownMaxDeceleration = Math.abs(acceleration);
			}
			
			double previousSlipAngleVelocity = data.slipAngleVelocity;
			
			double previousSlipAngle = previousPosition.getAngle();

			// TODO Handle special cases
			double slipAngleVelocity = getVelocity(position.getAngle() - previousSlipAngle, 1);
			data.slipAngleVelocity = slipAngleVelocity;
			
			double slipAngleAcceleration = getAcceleration(previousSlipAngleVelocity, slipAngleVelocity, 1);
			data.slipAngleAcceleration = slipAngleAcceleration;
			
			if(Math.abs(slipAngleAcceleration) > data.knownMaxSlipAngleAcceleration) {
				data.knownMaxSlipAngleAcceleration = Math.abs(slipAngleAcceleration);
			}
			
			TrackPiece piece = track.getPieces()[position.getPiecePosition().getPieceIndex()];
			
			double adjustedRadius = piece.getAdjustedRadius(position.getPiecePosition().getLane().getEndLaneIndex(), track.getLanes());
			
			double angularAcceleration = 0.0d;
			if(Math.abs(adjustedRadius) > 0) {
				angularAcceleration = velocity * velocity / Math.abs(adjustedRadius);	
			}
			data.angularAcceleration = angularAcceleration;
			
			if(angularAcceleration > data.knownMaxAngularAcceleration) {
				data.knownMaxAngularAcceleration = angularAcceleration;
			}
			
			if (Math.abs(position.getAngle()) > data.knownMaxAngle) {
				data.knownMaxAngle = Math.abs(position.getAngle());
			}
			
		}
	}

	@Override
	public void onCrash(Car car) {
		
		numberOfAllCrashes++;
		// Use the crash to possibly determine a new max angular acceleration
		CarDataPoint data = carData.get(car);
		double newMaxAngularAcceleration = data.angularAcceleration * 0.95d;
		if(newMaxAngularAcceleration < data.knownMaxAngularAcceleration) {
			data.knownMaxAngularAcceleration = newMaxAngularAcceleration;
		}
		data.isCrashed = true;
	}
	
	@Override
	public void onSpawn(Car car) {
		carData.get(car).isCrashed = false;
		System.out.println("SPAWNED");
	}
	
	@Override
	public void onTurboAvailable(TurboAvailable turbo) {
		System.out.println("TURBO AVAILABLE! Currently on track: "+!this.isMyCarCrashed());
		if (!this.isMyCarCrashed()) {
			this.myTurboAvailable = true;
			this.myTurboLength = turbo.getTurboDurationTicks();
			this.nextTurboFactor = turbo.getTurboFactor();
		}
		else //Specs do not explicitly mention is the previous turbo still available after available message when crashed
			this.myTurboAvailable = false;
	}
	
	public int getCurrentTick() {
		return currentTick;
	}
	
	public boolean isMyTurboAvailable() {
		return myTurboAvailable;
	}

	public double getMyCurrentTurboFactor() {
		if (isMyTurboActive())
			return this.myTurboFactor;
		else
			return 1.0d;
	}
	
	public void setMyTurboActivated() {
		this.myTurboAvailable = false;
		this.myTurboEndTick = this.currentTick+this.myTurboLength;
		this.myTurboFactor = this.nextTurboFactor;
	}
	
	public boolean isMyTurboActive() {
		return this.myTurboEndTick >= this.currentTick;
	}

	public Car getMyCar() {
		return myCar;
	}

	public Track getTrack() {
		return track;
	}
	
	public CarSpecs getMyCarSpecs() {
		return carSpecs.get(myCar);
	}
	
	public Collection<CarSpecs> getCars() {
		return carSpecs.values();
	}
	
	public CarSpecs getCarSpecs(Car car) {
		return carSpecs.get(car);
	}
	
	public RaceSession getSession() {
		return session;
	}
	
	public boolean isMyCarOnSwitch() {
		return isCarOnSwitch(myCar);
	}
	
	public boolean isMyCarCrashed() {
		return isCarCrashed(myCar);
	}
	
	public boolean isCarCrashed(Car car) {
		return carData.get(car).isCrashed;
	}
	
	public boolean isCarOnSwitch(Car car) {
		return getTrackPiece(car).hasLaneSwitch();
	}
	
	public boolean isMyCarInTurn() {
		return isCarInTurn(myCar);
	}
	
	public boolean isCarInTurn(Car car) {
		return !getTrackPiece(car).isStraight();
	}
	
	// These should not be needed directly. Uncomment if needed
//	public CarPosition getMyPosition() {
//		return getPosition(myCar);
//	}
//	
//	public CarPosition getPosition(Car car) {
//		return carData.get(car).position;
//	}
	
	public double getMyAngle() {
		return getAngle(myCar);
	}
	
	public double getAngle(Car car) {
		return carData.get(car).position.getAngle();
	}
	
	public PiecePosition getMyPiecePosition() {
		return getPiecePosition(myCar);
	}
	
	public PiecePosition getPiecePosition(Car car) {
		return carData.get(car).position.getPiecePosition();
	}
	
	public TrackPiece getMyTrackPiece() {
		return getTrackPiece(myCar);
	}
	
	public TrackPiece getTrackPiece(Car car) {
		return track.getPieces()[getPiecePosition(car).getPieceIndex()];
	}
	
	public int getMostCommonLane(Car car, int pieceIndex) {
		CarDataPoint data = carData.get(car);
		return data.getMostUsedLane(pieceIndex);
	}
	
	public Car getCarInFrontOfMe() {
		TreeMap<Double, Car> carsOnTrack =
				new TreeMap<Double, Car>();
		for(CarSpecs car : getCars()) {
			double position = 0.0d;
			position += getPiecePosition(car.getCar()).getPieceIndex() * 1000;
			position += getPiecePosition(car.getCar()).getInPieceDistance();
			carsOnTrack.put(position, car.getCar());
		}
		boolean foundMyCar = false;
		for(Entry<Double, Car> entry : carsOnTrack.entrySet()) {
			if(foundMyCar) {
				return entry.getValue();
			}
			if(entry.getValue().equals(myCar)) {
				foundMyCar = true;
			}
		}
		
		//return null if we are the only one
		if (carsOnTrack.firstEntry().getValue().equals(myCar))
			return null;
		
		// If our car was the last on the lap, then the first car is in front
		return carsOnTrack.firstEntry().getValue();
	}
	
	public Map<Integer, Car> getCarsByPositions() {
		TreeMap<Double, Car> progresses = new TreeMap<Double, Car>();
		for(CarSpecs carSpec : getCars()) {
			CarDataPoint data = carData.get(carSpec.getCar());
			int lap = data.position.getPiecePosition().getLap();
			int index = data.position.getPiecePosition().getPieceIndex();
			double dist = data.position.getPiecePosition().getInPieceDistance();
			// Random is added in case two cars are in the same position
			double prog = lap * 100000 + index * 1000 + dist + Math.random() * 0.1d;
			progresses.put(prog, carSpec.getCar());
		}
		Map<Integer, Car> positions = new HashMap<Integer, Car>();
		int maxPos = progresses.size();
		int pos = 0;
		for(Entry<Double, Car> entry : progresses.entrySet()) {
			positions.put(maxPos - pos, entry.getValue());
			pos++;
		}
		return positions;
	}
	
	public PiecePositionLane getMyLane() {
		return getLane(myCar);
	}
	
	public PiecePositionLane getLane(Car car) {
		return carData.get(car).position.getPiecePosition().getLane();
	}
	
	public int getMyNextTurnPiece() {
		return getNextTurnPiece(myCar);
	}
	
	public int getNextTurnPiece(Car car) {
		int pieceIndex = getPiecePosition(car).getPieceIndex();
		int origIndex = pieceIndex;
		pieceIndex = (pieceIndex + 1) % track.getPieces().length;
		while(pieceIndex != origIndex) {
			if(!track.getPieces()[pieceIndex].isStraight()) {
				return pieceIndex;
			}
			pieceIndex = (pieceIndex + 1) % track.getPieces().length;
		}
		throw new Error("Straight tracks are not supported");
	}
	
	public double getMyVelocity() {
		return getVelocity(myCar);
	}
	
	public double getVelocity(Car car) {
		return carData.get(car).velocity;
	}
	
	public double getMyAcceleration() {
		return getAcceleration(myCar);
	}
	
	public double getAcceleration(Car car) {
		return carData.get(car).acceleration;
	}
	
	public double getMySlipAngleVelocity() {
		return getSlipAngleVelocity(myCar);
	}
	
	public double getSlipAngleVelocity(Car car) {
		return carData.get(car).slipAngleVelocity;
	}
	
	public double getMySlipAngleAcceleration() {
		return getSlipAngleAcceleration(myCar);
	}
	
	public double getSlipAngleAcceleration(Car car) {
		return carData.get(car).slipAngleAcceleration;
	}
	
	public double getMyAngularAcceleration() {
		return getAngularAcceleration(myCar);
	}
	
	public double getAngularAcceleration(Car car) {
		return carData.get(car).angularAcceleration;
	}
	
	public double getMyKnownMaxVelocity() {
		return getKnownMaxVelocity(myCar);
	}
	
	public double getKnownMaxVelocity(Car car) {
		return carData.get(car).knownMaxVelocity;
	}
	
	public double getMyKnownMaxAcceleration() {
		return getKnownMaxAcceleration(myCar);
	}
	
	public double getKnownMaxAcceleration(Car car) {
		return carData.get(car).knownMaxAcceleration;
	}
	
	public double getMyKnownMaxDeceleration() {
		return getKnownMaxDeceleration(myCar);
	}
	
	public double getKnownMaxDeceleration(Car car) {
		return carData.get(car).knownMaxDeceleration;
	}
	
	public double getMyKnownMaxAngularAcceleration() {
		return getKnownMaxAngularAcceleration(myCar);
	}
	
	public double getKnownMaxAngularAcceleration(Car car) {
		return carData.get(car).knownMaxAngularAcceleration;
	}
	

	public double getHighestKnownMaxAngularAcceleration() {
		double highest = 0;
		for(CarDataPoint data : carData.values()) {
			if(data.knownMaxAngularAcceleration > highest) {
				highest = data.knownMaxAngularAcceleration;
			}
		}
		return highest;
	}
	
	public double getMyKnownMaxAngle() {
		return getKnownMaxAngle(myCar);
	}
	
	public double getKnownMaxAngle(Car car) {
		return carData.get(car).knownMaxAngle;
	}
	
	public double getHighestKnownMaxAngle() {
		double highest = 0;
		for(CarDataPoint data : carData.values()) {
			if(data.knownMaxAngle > highest) {
				highest = data.knownMaxAngle;
			}
		}
		return highest;
	}
	
	public double getKnownMaxSlipAngleAcceleration(Car car) {
		return carData.get(car).knownMaxSlipAngleAcceleration;
	}
	
	public double getMyKnownMaxSlipAngleAcceleration() {
		return getKnownMaxSlipAngleAcceleration(myCar);
	}
	
	public long getNumberOfAllCrashes() {
		return this.numberOfAllCrashes;
	}
	
	// TODO Improve this to take lanes into account
	/**
	 * Note: position2 MUST be after position1, or the result is undefined.
	 * @param position1
	 * @param position2
	 * @return
	 */
	public double getDistance(PiecePosition position1, PiecePosition position2) {
		
		int lap1 = position1.getLap();
		int lap2 = position2.getLap();
				
		int piece1 = position1.getPieceIndex();
		int piece2 = position2.getPieceIndex();
		
		double dist = 0.0d;
		
		if(piece1 == piece2) {	
			dist = position2.getInPieceDistance() - position1.getInPieceDistance();
			
		}
		else if (piece1 == piece2-1) {
			dist = track.getPieces()[position1.getPieceIndex()].getRouteDistance(position1.getLane().getStartLaneIndex(), 
					position1.getLane().getEndLaneIndex(), track.getLanes());
			
			// Add first partial piece
			dist -= position1.getInPieceDistance();
			
			// Add last piece
			dist += position2.getInPieceDistance();
		}
		else { //NOTE THIS DOES NOT WORK FOR SWITCHES, IT JUST A ROUGH ESTIMATE
			
			double piece1Length = getPieceLength(
					track.getPieces()[position1.getPieceIndex()], 
					position1.getLane().getStartLaneIndex(), 
					position1.getLane().getEndLaneIndex());
			
			// Add first partial piece
			dist += piece1Length - position1.getInPieceDistance();
			
			// Add last piece
			dist += position2.getInPieceDistance();
			
			// Move to first full piece
			piece1 = piece1 + 1;
		
			// Add effect of laps
			piece1 = piece1 + lap1 * track.getPieces().length;
			piece2 = piece2 + lap2 * track.getPieces().length;
			
			// Add full pieces
			while(piece1 < piece2) {
				int actualIndex = piece1 % track.getPieces().length;
				dist += getRoughPieceLength(track.getPieces()[actualIndex]);
				piece1++;
			}

		}
		
		return dist;
		
	}
	
	/**
	 * Get distance from position1 to the start of piece2. Length of piece2
	 * has no effect.
	 * @param position1
	 * @param piece2Index
	 * @return
	 */
	public double getDistance(PiecePosition position1, int piece2Index) {
		
		int piece1Index = position1.getPieceIndex();
		
		if(piece1Index == piece2Index) {
			return 0.0d;
		}
		
		double dist = 0.0d;
					
		double piece1Length = getPieceLength(
				track.getPieces()[position1.getPieceIndex()], 
				position1.getLane().getStartLaneIndex(), 
				position1.getLane().getEndLaneIndex());
		
		// Add first partial piece
		dist += piece1Length - position1.getInPieceDistance();
		
		// Move to first full piece
		piece1Index = piece1Index + 1;
		
		// Add full pieces
		while(piece1Index < piece2Index) {
			int actualIndex = piece1Index % track.getPieces().length;
			dist += getRoughPieceLength(track.getPieces()[actualIndex]);
			piece1Index++;
		}

		return dist;
	}
	
	public double getPieceLength(TrackPiece trackPiece, int startLane, int endLane) {
		if(trackPiece.isStraight()) {
			return trackPiece.getLength();
		} else {
			// TODO Use longest route if switching, just in case. Use
			// angle radius + distance from center to determine which route is
			// longest
			return Math.abs(trackPiece.getAngle() / 360d) * 2 * Math.PI * trackPiece.getAdjustedRadius(endLane, track.getLanes());
		}
	}
	
	/**
	 * Returns the piece length using the "middle lane", i.e. when the lane
	 * is not yet known. Do not use this when the lane is known, as the length
	 * might be shorter than actual driving distance.
	 * @param trackPiece
	 * @return
	 */
	public double getRoughPieceLength(TrackPiece trackPiece) {
		if(trackPiece.isStraight()) {
			return trackPiece.getLength();
		} else {
			
			// TODO Use adjusted radius
			return Math.abs(trackPiece.getAngle() / 360d) * 2 * Math.PI * trackPiece.getRadius();
		}
	}
	
	public Map<Car, CarDataPoint> getDataPoints() {
		return carData;
	}
	
	/**
	 * Do not call if race is still ongoing.
	 * @param points
	 */
	public void setDataPoints(Map<Car, CarDataPoint> points) {
		this.carData = points;
	}
	
	private double getVelocity(double distance, int ticks) {
		return distance / (double)ticks;
	}
	
	private double getAcceleration(double velocity1, double velocity2, int ticks) {
		return (velocity2 - velocity1) / (double)ticks;
	}
	
	public void outputMyMeasurements() {
		System.out.print("\n");
		System.out.print(this.currentTick+" / "+ this.getMyPiecePosition().getInPieceDistance() +" "+this.getMyTrackPiece().getAngle()+": \n");
		System.out.print(this.getMyVelocity());
		System.out.print(",");
		System.out.print(this.getMyAngularAcceleration());
		System.out.print(",");
		System.out.print(this.getMyAngle());
		System.out.print(",");
		System.out.print(this.getMySlipAngleAcceleration());
		System.out.print(",");
		System.out.print(this.getMySlipAngleVelocity());
	}
	
	public class CarDataPoint {
		public CarPosition position = null;
		public boolean isCrashed = false;
		public double velocity = 0.0d;
		public double acceleration = 0.0d;
		public double slipAngleVelocity = 0.0d;
		public double slipAngleAcceleration = 0.0d;
		public double angularAcceleration = 0.0d;
		public double knownMaxVelocity = 0.0d;
		public double knownMaxAcceleration = 0.0d;
		public double knownMaxDeceleration = 0.0d;
		public double knownMaxAngularAcceleration = 0.0d;
		public double knownMaxSlipAngleAcceleration = 0.0d;
		public double knownMaxAngle = 0.0d;
		
		public TreeMap<Integer, TreeMap<Integer, Integer>> laneUsagePerPiece = new 
				TreeMap<Integer, TreeMap<Integer, Integer>>();
		
		public void addLaneUsage(CarPosition position) {
			int lane = position.getPiecePosition().getLane().getEndLaneIndex();
			int pieceIndex = position.getPiecePosition().getPieceIndex();
			
			TreeMap<Integer, Integer> laneUsages = this.laneUsagePerPiece.get(pieceIndex); 
			if (laneUsages == null) {
				laneUsages = new TreeMap<Integer, Integer>();
				laneUsages.put(lane, 1);
				this.laneUsagePerPiece.put(pieceIndex, laneUsages);
			}
			else {
				Integer usageCount = laneUsages.get(lane);
				if (usageCount == null) {
					laneUsages.put(lane, 1);
				}
				else
					laneUsages.put(lane, usageCount+1);
			}
		}
		
		public int getMostUsedLane(int pieceIndex) {
			TreeMap<Integer, Integer> laneUsages = this.laneUsagePerPiece.get(pieceIndex);
			if (laneUsages == null || laneUsages.size() == 0)
				return -1;
			
			Map.Entry<Integer, Integer> maxEntry = null;
			for (Map.Entry<Integer, Integer> entry : laneUsages.entrySet())
			{
			    if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
			    {
			        maxEntry = entry;
			    }
			}
			
			
			return maxEntry.getKey();
		}
		
		public CarDataPoint duplicate() {
			Gson gson = new Gson();
			CarDataPoint c = new CarDataPoint();
			c.position = gson.fromJson(gson.toJson(position), CarPosition.class);
			c.isCrashed = isCrashed;
			c.velocity = velocity;
			c.acceleration = acceleration;
			c.slipAngleVelocity = slipAngleVelocity;
			c.slipAngleAcceleration = slipAngleAcceleration;
			c.angularAcceleration = angularAcceleration;
			c.knownMaxVelocity = knownMaxVelocity;
			c.knownMaxAcceleration = knownMaxAcceleration;
			c.knownMaxDeceleration = knownMaxDeceleration;
			c.knownMaxAngularAcceleration = knownMaxAngularAcceleration;
			c.knownMaxAngle = knownMaxAngle;
			// Lane usage would need deep copying and is not supported
			return c;
		}
		
	}

}
