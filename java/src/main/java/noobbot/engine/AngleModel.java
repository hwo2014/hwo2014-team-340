package noobbot.engine;

import java.util.ArrayList;

import noobbot.network.pojo.PiecePosition;
import noobbot.network.pojo.PiecePositionLane;
import noobbot.network.pojo.Track;
import matrix.Matrix;
import matrix.NoSquareException;
import regression.MultiLinear;

public class AngleModel {
	
	private ArrayList<ArrayList<Double>> measurements;
	private ArrayList<Double> accelerations;
	//private ArrayList<Double> angleModel;
	
	private ArrayList<Double> speedy = new ArrayList<Double>();
	
	public static double epsilon = 0.000001d;
	public static double initialTargetAngle = 60.0d;
	
	private double highestKnownAngularAcceleration = -1.0d;
	private double highestKnownAngle = -1.0d;
	
	private double myHighestKnownSlipAngleAcceleration = -1.0d;
	
	
	//NEW CORNER MODEL
	private double aLimit = -1.0d;
	
	private double H = 0.0d;
	private double K = 0.0d;
	private double L = 0.0d;
	
	private int prevTick = -1;
	private double previousAngularAcceleration = 0.0;
	private int previousRadius = 0;
	
	//PENTAG: H: 1.942724715549149 K: -0.006770822497996897 L: -0.06481720269383288 Limit: 0.3179177879944402
	//KEIMOLAH: 2.362046666657635 K: -0.00793460664860178 L: -0.07175225119489656 Limit: 0.3179177879944402
	/*private double aLimit = 0.3179177879944402;//-1.0d;
	
	private double H = 2.362046666657635;//1.942724715549149;
	private double K = -0.00793460664860178;//-0.006770822497996897;
	private double L = -0.07175225119489656;//-0.06481720269383288;*/
	
	private int countOfAngularAccRestTicks = 0;
	
	private ArrayList<double[]> slipangleMeasurement = new ArrayList<double[]>();
	private ArrayList<double[]> slipangleAcceleration = new ArrayList<double[]>();
	/*private boolean coefficientsLocked = true;
	
	private double epsilon = 0.00001d; // used for "zero" comparison
	private double largeEpsilon = 0.1d; // used for comparisons that are not so strict 
										//i.e. it will be hard to get exact zero angle speed values
	private double angleTreshold = 2.0d;
	private double angleSpeedTreshold = 0.2d;
	
	private boolean tickIsAngularRest = false;

	//private int currentTick = 0; //for debugging
	
	
	private double a1Angle = 0.0d;
	private double a2Angle = 0.0d;
	private double a3Angle = 0.0d;
	
	private boolean isValidMeasurement(double value) {
		if (Math.abs(value) > this.epsilon)
			return true;
		
		return false;
	}
	
	private boolean isGoodMeasurement(double value) {
		if (Math.abs(value) > this.largeEpsilon)
			return true;
		
		return false;
	}*/
	
	public double getTargetAngle(boolean useMeasured) {
		if (useMeasured)
			return this.highestKnownAngle;
		
		return AngleModel.initialTargetAngle;
		
	}
	
	public void storeMyHighestKnownSlipAngleAcceleration(double myHighestKnownSlipAngleAcceleration) {
		this.myHighestKnownSlipAngleAcceleration = Math.max(this.myHighestKnownSlipAngleAcceleration, Math.abs(myHighestKnownSlipAngleAcceleration));
	}
	
	public void storeMaxAngleAndAcceleration( double highestKnownAngle, double highestKnownAngularAcceleration) {
		this.highestKnownAngularAcceleration = Math.max(this.highestKnownAngularAcceleration, Math.abs(highestKnownAngularAcceleration));
		this.highestKnownAngle = Math.max(this.highestKnownAngle, Math.abs(highestKnownAngle));
	}
	
	public double getHighestKnownAngularAcceleration() {
		return highestKnownAngularAcceleration;
	}

	public double getHighestKnownAngle() {
		return highestKnownAngle;
	}

	public void resetALimitMeasurement() {
		countOfAngularAccRestTicks = 0;
		previousAngularAcceleration = -1.0;
	}
	
	public void measureVariablesForModel(int gameTick, double angularAcceleration, double angle, 
			double angleAcceleration, double angleSpeed, int turnRadius, boolean isSwitch) {
		
		if (Math.abs(previousAngularAcceleration-angularAcceleration)>1.0d) {//sanity check to avoid bad values from other car bump
			countOfAngularAccRestTicks = 0;
			return;
		}
		
		storeMaxAngleAndAcceleration(angle, angularAcceleration);
		storeMyHighestKnownSlipAngleAcceleration(angleAcceleration);
		
		if (Math.abs(angularAcceleration) > this.epsilon 
				&& Math.abs(angleAcceleration) < this.epsilon && Math.abs(angleSpeed) < this.epsilon
				&& Math.abs(angle) < this.epsilon) {
			//tempLimit = Math.max(tempLimit, Math.abs(angularAcceleration));
			countOfAngularAccRestTicks++;
		}
		else {
			countOfAngularAccRestTicks = 0;
		}
		
		if (gameTick == this.prevTick+1 && 
				countOfAngularAccRestTicks >= 2 /*&& turnRadius == previousRadius*/ 
				&& turnRadius != 0 && !isSwitch)
			this.aLimit = Math.max(Math.abs(previousAngularAcceleration), this.aLimit);
		
		previousAngularAcceleration = angularAcceleration;	
		this.prevTick = gameTick;
		previousRadius = turnRadius;
		
		//if (!this.isMinAngularLimitFound())
		//	return;
		
		//TODO: let use the original acceleration for measurements and adjusted when calculating
		//double adjustedAngularAcceleration = this.getAdjustedAngularAcceleration(angularAcceleration);
		slipangleMeasurement.add(new double[]{angularAcceleration, angle, angleSpeed});
		if (slipangleMeasurement.size() > 3000)
			slipangleMeasurement.remove(0);
		
		slipangleAcceleration.add(new double[]{angleAcceleration});
		if (slipangleAcceleration.size() > 3000)
			slipangleAcceleration.remove(0);
		
	}
	
	public ArrayList<double[]> getAdjustMeasurements() {
		ArrayList<double[]> temp = new ArrayList<double[]>(); 
		
		for (double[] values : slipangleMeasurement) {
			temp.add(new double[]{getAdjustedAngularAcceleration(values[0]), values[1], values[2]});
		}
		
		return temp;
	}
	
	public void calculateCoefficients() {
		
		if (slipangleMeasurement.size() < 40 || slipangleAcceleration.size() < 40 || !this.isMinAngularLimitFound() ||
				this.myHighestKnownSlipAngleAcceleration < AngleModel.epsilon)
			return;
		
		try {
		
		ArrayList<double[]> adjusteMeasurements = this.getAdjustMeasurements();
		final Matrix X = new Matrix(adjusteMeasurements.toArray(new double[adjusteMeasurements.size()][3]));
		
		final Matrix Y = new Matrix(slipangleAcceleration.toArray(new double[slipangleAcceleration.size()][1]));
		final MultiLinear ml = new MultiLinear(X, Y, false);
		final Matrix beta = ml.calculate();
		
		this.H = beta.getValueAt(0, 0);
		this.K = beta.getValueAt(1, 0);
		this.L = beta.getValueAt(2, 0);
		
		this.outputCoefficients();
		}
		catch (Exception e) {
			System.out.println("Coefficient calculation failed: "+e);
		}
	}
	
	/*public void measureVariablesForModel(double angularAcceleration, double angle, 
			double angleAcceleration, double angleSpeed) {
		
		if (coefficientsLocked)
			return;
		
		if (Math.abs(angularAcceleration) > this.epsilon 
				&& Math.abs(angleAcceleration) < this.largeEpsilon && Math.abs(angleSpeed) < this.largeEpsilon) {
			tempLimit = Math.max(tempLimit, Math.abs(angularAcceleration));
			this.tickIsAngularRest = true;
			countOfAngularAccRestTicks++;
		}
		else {
			this.tickIsAngularRest = false;
		}
		
		if (countOfAngularAccRestTicks >= 2)
			this.aLimit = Math.max(tempLimit, this.aLimit);
		
		if (!this.isMinAngularLimitFound())
			return;
		
		
		a1Angle = a2Angle;
		a2Angle = a3Angle;
		a3Angle = Math.abs(angleAcceleration);
		
		//Do the derivates assume something e.g. constant angleAcc?
		if (isGoodMeasurement(a1Angle) && isGoodMeasurement(a2Angle) && isGoodMeasurement(a3Angle) && 
				isGoodMeasurement(angularAcceleration-this.aLimit) && isAngleAccelerationIncreasing()) {
			double da1Angle = a2Angle - a1Angle;
			double da2Angle = a3Angle - a2Angle;
			
			double ddaAngle = da2Angle - da1Angle;
			
			this.L = - ddaAngle / da2Angle;
			this.K = -da2Angle - a3Angle * this.L;
			this.H = (a3Angle + Math.abs(angle) * this.K + Math.abs(angleSpeed) * this.L) / Math.abs(angularAcceleration-this.aLimit);
		
			System.out.println("MODEL READYYYYYY!!!");
			outputCoefficients();
		}
		
	}
	
	public boolean isAngleAccelerationIncreasing() {
		if (a1Angle < a2Angle && a2Angle < a3Angle)
			return true;
		
		return false;
	}
	
	public void measureVariablesForModelOld(double angularAcceleration, double angle, 
			double angleAcceleration, double angleSpeed) {
		//currentTick++;
		
		if (coefficientsLocked)
			return;
		
		//boolean previousTickWasAngularRest = this.tickIsAngularRest;
		//Update limit acc
		if (Math.abs(angularAcceleration) > this.epsilon 
				&& Math.abs(angleAcceleration) < this.largeEpsilon && Math.abs(angleSpeed) < this.largeEpsilon) {
			tempLimit = Math.max(tempLimit, Math.abs(angularAcceleration));
			this.tickIsAngularRest = true;
			countOfAngularAccRestTicks++;
		}
		else {
			this.tickIsAngularRest = false;
		}
		
		if (countOfAngularAccRestTicks >= 2)
			this.aLimit = Math.max(tempLimit, this.aLimit);
		
		if (!this.isMinAngularLimitFound())
			return;
		
		double adjustedAngularAcceleration = this.getAdjustedAngularAcceleration(angularAcceleration);
		//Update H coefficient
		if ( Math.abs(adjustedAngularAcceleration) > this.epsilon &&
				Math.abs(angleAcceleration) > this.largeEpsilon && 
				Math.abs(angle) < angleTreshold && 
				Math.abs(angleSpeed) < this.angleSpeedTreshold)  {
			this.H = angleAcceleration / (angularAcceleration-this.aLimit);
			
			outputCoefficients();
			
			
		}
		
		if (!this.tickIsAngularRest)
			countOfAngularAccRestTicks = 0;
		
		//Angle speed = 0 measurement
		if (Math.abs(angle) > angleTreshold && 
				Math.abs(angleSpeed) < this.largeEpsilon && Math.abs(angleAcceleration) > this.largeEpsilon
				&& Math.abs(adjustedAngularAcceleration) < this.epsilon) {
			this.K = (-angleAcceleration)/angle;
		}
		
		//Note we could also update this in every tick even though it is ready but i'll now disable if we have a value
		if (Math.abs(this.K) > this.epsilon &&
				//Math.abs(angleAcceleration) > this.epsilon &&
				Math.abs(angleSpeed) > this.largeEpsilon && Math.abs(angle) > angleTreshold
				&& Math.abs(adjustedAngularAcceleration) < this.epsilon 
				&& Math.abs(angleAcceleration) < this.largeEpsilon) {
			this.L = (-angle*this.K-angleAcceleration)/angleSpeed;
			
			//coefficientsLocked = true;
			//System.out.println("MODEL READYYYYYY!!!");
			//outputCoefficients();
		}
		
	}*/
	
	public double getMyHighestKnownSlipAngleAcceleration() {
		return myHighestKnownSlipAngleAcceleration;
	}

	public void outputCoefficients() {
		System.out.println("H: "+this.H+" K: "+this.K+" L: "+this.L+" Limit: "+this.aLimit);
	}
	
	public double getALimit() {
		return this.aLimit;
	}
	
	public boolean isMinAngularLimitFound() {
		return this.aLimit > 0.0d;
	}
	
	private double getAdjustedAngularAcceleration(double angularAcceleration) {
		double adjustedAngularAcceleration = 0.0d;
		if (angularAcceleration > 0 ) {
			adjustedAngularAcceleration = angularAcceleration-this.aLimit;
			adjustedAngularAcceleration = Math.max(adjustedAngularAcceleration, 0.0d);
		}
		else {
			adjustedAngularAcceleration = angularAcceleration+this.aLimit;
			adjustedAngularAcceleration = Math.min(adjustedAngularAcceleration, 0.0d);
		}
		
		return adjustedAngularAcceleration;
	}
	
	public double calculateSlipAngleAcceleration(double angularAcceleration, double angle, double angleSpeed) {
		double adjustedAngularAcceleration = this.getAdjustedAngularAcceleration(angularAcceleration);
		return adjustedAngularAcceleration*this.H+angle*this.K+angleSpeed*this.L;
	}
	
	public boolean modelReady() {
		return Math.abs(this.H) > this.epsilon && Math.abs(this.K) > this.epsilon && Math.abs(this.L) > this.epsilon;
	}
	
	public AngleModel() {
		this.measurements = new ArrayList<ArrayList<Double>>();
		this.measurements.add(new ArrayList<Double>());
		this.accelerations = new ArrayList<Double>();
		
		//runTestSimulation(0.463123359819994, 10.249662533402442, -0.0740509182214737, 44);
	}
	
	public void addMeasuredAngle(double angle) {
		this.measurements.get(this.measurements.size()-1).add(angle);
	}
	
	public void addSpeedY(double slipAngle, double speedX) {
		this.speedy.add(Math.tan(-Math.toRadians(slipAngle))*speedX);
	}
	
	public void outputSpeedYs() {
		for (int i = 0; i < this.speedy.size(); i++) {
			if (i > 0)
				System.out.print(",");
			System.out.print(this.speedy.get(i));
		}
	}
	
	public void addAccelerationIfMissing(double acceleration) {
		if (this.measurements.size() > this.accelerations.size())
			this.accelerations.add(acceleration);
		else {
			//Set to the max acc
			if (acceleration > this.accelerations.get(this.accelerations.size()-1)) {
				this.accelerations.set(this.accelerations.size()-1, acceleration);
			}
		}
	}
	
	public void getReadyForNextMeasurement() {
		if ( this.measurements.get(this.measurements.size()-1).size() > 0)
			this.measurements.add(new ArrayList<Double>());
	}
	
	public void outputMeasurements() {
		for (int listIndex = 0; listIndex < this.measurements.size(); listIndex++) {
			// We might be "ready" for new angles so no acceleration for last index
			if (listIndex < this.accelerations.size())
				System.out.print(this.accelerations.get(listIndex));
			for (int i = 0; i < this.measurements.get(listIndex).size(); i++) {
				System.out.print(",");
				System.out.print(this.measurements.get(listIndex).get(i));
			}
			System.out.println("");
		}
	}
	
	public void outputAngles() {
		for (int listIndex = 0; listIndex < this.measurements.size(); listIndex++) {
			// We might be "ready" for new angles so no acceleration for last index
			if (listIndex < this.accelerations.size())
				System.out.print(this.accelerations.get(listIndex));
			if (this.measurements.get(listIndex).size() > 0)
				System.out.print(","+this.measurements.get(listIndex).get(this.measurements.get(listIndex).size()-1));
			System.out.println("");
		}
	}
	
	//Remember to use adjusted radius in here
	public double getTurnTargetAcceleration(double distanceToStraight, double radius, double turnAngle, double maxKnownAngularAcceleration, 
			double maxSlipAngle, double startSlipAngle, double startSlipAngleSpeed) {
		
		double firstAngularAcceleration = Math.signum(turnAngle)*maxKnownAngularAcceleration*1.5d;
		int steps = 50;
		double safeMargin = 1.0f;
		double stepSize;
		if (firstAngularAcceleration > 0 )
			stepSize = (firstAngularAcceleration - this.aLimit)/steps;
		else
			stepSize = (firstAngularAcceleration + this.aLimit)/steps;
		
		for (int i = 0; i < steps; i++) {
			double angularAcceleration = firstAngularAcceleration - stepSize*i;
			//double distance = MathHelper.calculateArcLength(turnAngle, radius);
			double speed = Math.sqrt(Math.abs(angularAcceleration*radius));
			
			if (Math.abs(speed) < 0.00001d) //If we have zero or low speed the tick count would be infinity
				continue;
			
			double ticks = distanceToStraight / speed;
			//ticks += 20.0d; //Add some extra ticks so that we don't crash just after the corner
			double angle = this.calculateAngleMaximum(angularAcceleration, startSlipAngle, startSlipAngleSpeed, ticks);
			
			if (Math.abs(angle) < maxSlipAngle-safeMargin) {
				System.out.println("target angAcc: "+angularAcceleration+" estimated max angle: "+angle+ " current angle: "+startSlipAngle+" current angle speed: "+startSlipAngleSpeed);
				return angularAcceleration;
			}
		}
		
		//System.out.println("Targeting limit "+this.aLimit+" current angle "+startSlipAngle);
		return this.aLimit;
		//System.out.println("Targeting zero. Current limit "+this.aLimit+" current angle "+startSlipAngle);
		//return 0.0d;
		
	}
	
	private double calculateAngleMaximum(double angularAcceleration, double startSlipAngle, double startSlipAngleSpeed, double ticks) {
		double slipAngle = startSlipAngle;
		double slipAngleSpeed = startSlipAngleSpeed;
		
		double maxSlipAngle = 0.0d;
		
		int stepsInStraight = 8; // Use this to extend simulation to include couple steps of straight piece
		//Note: The time is 1 in these equations, use tick+1 to simulate long enough
		for (int i=0; i < ticks+1.0d+stepsInStraight; i++) {
			double tempAngularAcceleration = 0.0d;
			
			if (i < ticks+1.0d)
				tempAngularAcceleration = angularAcceleration;
				
			double slipAngleAcceleration = calculateSlipAngleAcceleration(tempAngularAcceleration,slipAngle, slipAngleSpeed);
			
			slipAngleSpeed += slipAngleAcceleration;
			//slipAngle += slipAngleSpeed + 0.5*slipAngleAcceleration;
			slipAngle += slipAngleSpeed;
			
			maxSlipAngle = Math.max(maxSlipAngle, Math.abs(slipAngle));
			
			//System.out.println(slipAngle+","+slipAngleAcceleration+","+slipAngleSpeed);
		}	
				
		return maxSlipAngle;
	}
	
	public void runTestSimulation(double angularAcceleration, double startSlipAngle, double startSlipAngleSpeed, double ticks) {
		System.out.println("TEST SIMULTAION STARTS");
		calculateAngleMaximum(angularAcceleration, startSlipAngle, startSlipAngleSpeed, ticks);
		System.out.println("TEST SIMULTAION ENDS");
	}
	
	double enginePower = 0.0d;
	double drag = 0.0d;
	private ArrayList<double[]> velocityMeasurements = new ArrayList<double[]>();
	private ArrayList<double[]> accelerationMeasurements = new ArrayList<double[]>();
	
	public boolean accelerationModelReady() {
		return enginePower > epsilon;
	}
	
	public void addAccelerationMeasurements(double velocity1, double velocity2) {
		velocityMeasurements.add(new double[]{ velocity2});
		accelerationMeasurements.add(new double[]{ velocity2-velocity1});
	}
	
	public void calculateAccelerationCoefficients() {
		//enginePower = velocity1;
		//drag = (velocity2 - 2.0d*velocity1)/velocity2;
		
		final Matrix X = new Matrix(velocityMeasurements.toArray(new double[velocityMeasurements.size()][1]));
		
		final Matrix Y = new Matrix(accelerationMeasurements.toArray(new double[accelerationMeasurements.size()][1]));
		final MultiLinear ml = new MultiLinear(X, Y, true);
		Matrix beta;
		try {
			beta = ml.calculate();
			
			this.enginePower = beta.getValueAt(0, 0);
			this.drag = beta.getValueAt(1, 0);
			
			System.out.println("EnginePower: "+enginePower+" Drag"+drag);
		} catch (NoSquareException e) {
			e.printStackTrace();
		}
		
	}
	
	//TODO use the turbo factor for calculations
	public double getNextStepVelocity(double currentVelocity, double throttle, double turboFactor) {
		double newVelocity = currentVelocity+throttle*enginePower*turboFactor+drag*currentVelocity;
		return newVelocity;
	}
	
	public int getTicksToSafeSpeed(double initialVelocity, double safeSpeed) {
		double velocity = initialVelocity;
		int maxIterations = 500;
		for (int ticks = 0; ticks < maxIterations; ticks++) {
			if (velocity < safeSpeed)
				return ticks;
			velocity = getNextStepVelocity(velocity, 0.0d, 1.0d);
		}
		return maxIterations;
	}
	
	//use ticks that are needed to 
	public boolean canThrottle(PiecePosition currentPos, PiecePosition prevPosition,  
			Track track, double maxAngle, double initialSlipAngle, double initialSlipAngleSpeed, 
			double initialVelocity, double safeSpeed, RouteCommander commander, double currentTurboFactor) {
		
		PiecePosition simulatedPos = new PiecePosition(currentPos);
		double slipAngleSpeed = initialSlipAngleSpeed;
		double slipAngle = initialSlipAngle;	
		double velocity = initialVelocity;
		
		//double turboFactor = 1.0d;
		
		double ticksToSafeSpeed = getTicksToSafeSpeed(getNextStepVelocity(velocity, 1.0d, currentTurboFactor), safeSpeed);
		if (velocity < safeSpeed)
			return true;
		
		double largestAngle = 0.0;
		for (int i=0; i < ticksToSafeSpeed+1; i++) {
			
			if (i==0)
				velocity = getNextStepVelocity(velocity, 1.0d, currentTurboFactor);
			else
				velocity = getNextStepVelocity(velocity, 0.0d, 0.0);
			
			PiecePositionLane laneForNextPiece = commander.getLaneForNextPiece(
					simulatedPos.getPieceIndex(), simulatedPos.getLane());
			track.updatePosition(simulatedPos, velocity, laneForNextPiece);
			
			double angularAcceleration = track.getAngularAcceleration(velocity, simulatedPos.getPieceIndex(), 
					simulatedPos.getLane().getStartLaneIndex(), simulatedPos.getLane().getEndLaneIndex());
			
			double slipAngleAcceleration = calculateSlipAngleAcceleration(angularAcceleration,slipAngle, slipAngleSpeed);
			
			slipAngleSpeed += slipAngleAcceleration;
			slipAngle += slipAngleSpeed;
			//slipAngle += slipAngleSpeed + 0.5*slipAngleAcceleration;
			
			if (largestAngle > 55.0)
				System.out.println("Big max angle: "+largestAngle+" AngAcc: "+angularAcceleration+
						" slipAcc: "+slipAngleAcceleration+" slipSpeed: "+slipAngleSpeed+" slipAngle: "+slipAngle);
			
			largestAngle = Math.max(largestAngle, Math.abs(slipAngle));
			
			if (Math.abs(slipAngle) > maxAngle) {
				System.out.println("Largest angle: "+largestAngle);
				return false;
			}
			
			if (velocity < safeSpeed) {
				System.out.println("Largest angle: "+largestAngle);
				return true;
			}
			
			//System.out.println(slipAngle+","+slipAngleAcceleration+","+slipAngleSpeed);
		}
		
		System.out.println("Largest angle: "+largestAngle);
		return true;
	}

}
