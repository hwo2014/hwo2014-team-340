package noobbot.engine;

import java.util.List;

public class MathHelper {
	public static double calculateAverage(List <Double> marks) {
	    if (marks == null || marks.isEmpty()) {
	        return 0;
	    }

	    double sum = 0;
	    for (Double mark : marks) {
	        sum += mark;
	    }

	    return sum / marks.size();
	}
	
	//http://en.wikipedia.org/wiki/Arc_(geometry)
	public static double calculateArcLength(double angle, double radius) {
		return ((Math.abs(angle) / 360)*2*Math.PI*radius);
	}
}
