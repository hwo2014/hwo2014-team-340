package noobbot.engine;

import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;

public class AIState {

	private String aiName;
	
	private double previousSpeed;
	private double currentSpeed;
	
	private double previousAngle;
	private double currentAngle;
	
	private double angularAcceleration;
	private int corneringRadius;
	
	private double previousInPieceDistance;
	private double inPieceDistance;
	
	private TrackPiece currentPiece;
	private TrackPiece previousPiece;

	private boolean brakeTest;
	private int currentLane;
	private int currentPieceIndex;
	
	//Track condition variables
	private double deceleration;	
	private double maxAngle;
	
	public AIState() {		
		this.brakeTest = false;
		
		this.deceleration = 1.0d;
		
		this.previousSpeed = 0.0d;
		this.previousAngle = 0.0d;
		this.previousInPieceDistance = 0.0d;
		
		this.currentLane = -1;
		this.currentPieceIndex = 0;
	}
	
	public String getAiName() {
		return aiName;
	}

	public void setAiName(String aiName) {
		this.aiName = aiName;
	}

	public void updateMaxAngle() {
		double absoluteAngle = Math.abs(this.currentAngle);
		if (absoluteAngle > this.maxAngle)
			this.maxAngle = absoluteAngle; 
	}
	
	//TODO: get rid of this
	//private String botIdentifier = "Kamotse ";
	public void updateCurrentState(CarPosition[] positions, Track track) {
		CarPosition currentCarPosition = null;
		for (CarPosition pos : positions) {
			if (pos.getId().getName().equals(aiName)) {
				currentCarPosition = pos;
			}
		}
		
		if (currentCarPosition == null)
			return;
		
		if (currentSpeed >= 0.0d) {
			previousSpeed = currentSpeed;
		}
		
		this.previousAngle = currentAngle;
		this.previousInPieceDistance = inPieceDistance;
		
		previousInPieceDistance = inPieceDistance;
		previousAngle = currentAngle;
		previousPiece = currentPiece;
		
		
		// Get current values 
		
		currentPieceIndex = currentCarPosition.getPiecePosition().getPieceIndex();
		currentLane = currentCarPosition.getPiecePosition().getLane().getEndLaneIndex();
		this.currentPiece = track.getPieces()[currentPieceIndex];
		//this.nextPiece = track.getNextPiece(currentPieceIndex);
		
		this.inPieceDistance = currentCarPosition.getPiecePosition().getInPieceDistance();
		this.currentAngle = currentCarPosition.getAngle();
		
		this.currentSpeed = 0.0d;
		
		if (previousPiece != null && previousPiece == this.currentPiece) {
			currentSpeed = inPieceDistance - previousInPieceDistance;
		}
		else if (previousPiece != null && previousPiece != this.currentPiece) {
			currentSpeed = previousPiece.getLength() - previousInPieceDistance + inPieceDistance;
		}
		
		if (currentSpeed > 0.0d)
			this.updateMaxAngle();
		
		this.corneringRadius = this.currentPiece.getAdjustedRadius(currentLane, track.getLanes());
		
		this.angularAcceleration = 0.0d;
		if (this.corneringRadius != 0)
			this.angularAcceleration = (currentSpeed*currentSpeed)/Math.abs(this.corneringRadius);
		
		if (this.brakeTest) {
			this.deceleration = (currentSpeed - previousSpeed);
			System.out.println("DECELERATION: " + this.deceleration);
			this.brakeTest = false;
		}
	}
	
	public boolean checkBrakesIfNeeded() {
		if (currentSpeed > 2.0d && this.deceleration > 0.0d) {
			this.brakeTest = true;		
			return true;
		}
		
		return false;
	}
	
	public TrackPiece getCurrentPiece() {
		return currentPiece;
	}

	public int getCurrentPieceIndex() {
		return currentPieceIndex;
	}

	public int getCurrentLane() {
		return currentLane;
	}

	public TrackPiece getPreviousPiece() {
		return previousPiece;
	}

	public double getCurrentSpeed() {
		return currentSpeed;
	}

	public double getCorneringRadius() {
		return corneringRadius;
	}

	public double getDeceleration() {
		return deceleration;
	}

	public double getAngularAcceleration() {
		return angularAcceleration;
	}

	public double getPreviousAngle() {
		return previousAngle;
	}

	public double getCurrentAngle() {
		return currentAngle;
	}

	public double getMaxAngle() {
		return maxAngle;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}
}
