package noobbot.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import noobbot.ai.BotAI;
import noobbot.main.RaceConstants;

public class SplashUI extends JFrame {

	private final static String BG_IMAGE_PATH =
			"https://helloworldopen.com/img/3bb66d5d.masthead-bg-lg.jpg";
	private final static int WIDTH = 1024;
	private final static int HEIGHT = 768;
	
	public SplashUI() {
		
		setTitle("Kamotse HWO");
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);

		// Center on screen
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - getHeight()) / 2);
		setLocation(x, y);
		
	}
	
	public void showUI() {
		
		// Layers
		
		JLayeredPane layeredPane = new JLayeredPane();
		
		// Background layer
		
		BGPanel bgPanel = new BGPanel(BG_IMAGE_PATH);
		bgPanel.setSize(WIDTH, HEIGHT);
		bgPanel.setLocation(0, 0);
		
		layeredPane.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		layeredPane.add(bgPanel, new Integer(1));
		
		// Main content layer
		
		SpringLayout contentLayout = new SpringLayout();
		JPanel contentPanel = new JPanel();
		contentPanel.setOpaque(false);
		contentPanel.setSize((int)(WIDTH * 0.9f), (int)(HEIGHT * 0.9f));
		contentPanel.setLocation((int)(WIDTH * 0.05f), (int)(HEIGHT * 0.025f));
		contentPanel.setLayout(contentLayout);
		
		// Center: race start buttons
		JPanel centerPanel = new JPanel();
		FlowLayout centerLayout = new FlowLayout();
		centerLayout.setHgap(25);
		centerPanel.setOpaque(false);
		centerPanel.setSize((int)(WIDTH * 0.9f), (int)(HEIGHT * 0.9f));
		centerPanel.setLayout(centerLayout);

		JButton singleBotRaceButton = new JButton("Single bot race");
		singleBotRaceButton.setMargin(new Insets(10, 10, 10, 10));
		singleBotRaceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showSingleRace();
			}
		});
		JButton multiBotRaceButton = new JButton("Multi bot race");
		multiBotRaceButton.setMargin(new Insets(10, 10, 10, 10));
		multiBotRaceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showMultiRace();
			}
		});
		
		centerPanel.add(singleBotRaceButton);
		centerPanel.add(multiBotRaceButton);
		
		contentPanel.add(centerPanel);
		
		contentLayout.putConstraint(SpringLayout.VERTICAL_CENTER, centerPanel, 0, SpringLayout.VERTICAL_CENTER, contentPanel);
		contentLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, centerPanel, 0, SpringLayout.HORIZONTAL_CENTER, contentPanel);
		
		layeredPane.add(contentPanel, new Integer(2));
		
		add(layeredPane);

		setVisible(true);
		
	}
	
	private void showSingleRace() {
		
		new RaceUI().showUI(
				RaceConstants.AI, RaceConstants.HOST, RaceConstants.PORT, 
				RaceConstants.TRACK, RaceConstants.PASSWORD, 1, 0);
		
	}
	
	private void showMultiRace() {
		int i = 0;
		for(BotAI ai : RaceConstants.AIS) {
			new RaceUI().showUI(
					ai, RaceConstants.HOST, RaceConstants.PORT, 
					RaceConstants.TRACK, RaceConstants.PASSWORD, 
					RaceConstants.AIS.length,
					i);
			i++;
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	
}
