package noobbot.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import noobbot.ai.BotAI;
import noobbot.engine.BotManager;
import noobbot.engine.RaceState;
import noobbot.engine.RaceState.CarDataPoint;
import noobbot.network.RaceConnection;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.Track;

import com.google.gson.Gson;

public class RaceUI extends JFrame {

	private final static String BG_IMAGE_PATH =
			"https://helloworldopen.com/img/3bb66d5d.masthead-bg-lg.jpg";
	private final static int WIDTH = 1024;
	private final static int HEIGHT = 768;
	
	private RaceConnection conn;
	private Thread connThread;
	private Thread logThread;
	
	private JTextArea logArea;
	private JTextArea trackArea;
	private JSlider tickSlider;
	
	private RaceState logState = new RaceState();
	private int lastLogTick = -1;
	private List<Map<Car, CarDataPoint>> carDataLogs = new
			ArrayList<Map<Car, CarDataPoint>>();
	
	public RaceUI() {
		
		setTitle("Race");
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setResizable(false);
		addWindowListener(new DismissWindowListener());
		
		// Center on screen
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - getHeight()) / 2);
		setLocation(x, y);
		
	}
	
	public void showUI(final BotAI ai, final String host, final int port, 
			final String track, final String password, final int botCount,
			final int myIndex) {
		
		final BotManager botManager = new BotManager(ai);
		
		// Layers
		
		JLayeredPane layeredPane = new JLayeredPane();
		
		// Background layer
		
		BGPanel bgPanel = new BGPanel(BG_IMAGE_PATH);
		bgPanel.setSize(WIDTH, HEIGHT);
		bgPanel.setLocation(0, 0);
		
		layeredPane.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		layeredPane.add(bgPanel, new Integer(1));
		
		// Main content layer
		
		SpringLayout contentLayout = new SpringLayout();
		JPanel contentPanel = new JPanel();
		contentPanel.setOpaque(false);
		contentPanel.setSize((int)(WIDTH * 0.9f), (int)(HEIGHT * 0.9f));
		contentPanel.setLocation((int)(WIDTH * 0.05f), (int)(HEIGHT * 0.025f));
		contentPanel.setLayout(contentLayout);
		
		logArea = new JTextArea("Event log");
		logArea.setMargin(new Insets(5, 5, 5, 5));
		contentPanel.add(logArea);
		
		contentLayout.putConstraint(SpringLayout.EAST, logArea, 0, SpringLayout.EAST, contentPanel);
		contentLayout.putConstraint(SpringLayout.NORTH, logArea, 0, SpringLayout.NORTH, contentPanel);
		
		trackArea = new JTextArea("Track");
		trackArea.setMargin(new Insets(5, 5, 5, 5));
		trackArea.setFont(new Font("Monospaced", Font.PLAIN, 15));
		contentPanel.add(trackArea);
		
		contentLayout.putConstraint(SpringLayout.WEST, trackArea, 0, SpringLayout.WEST, contentPanel);
		contentLayout.putConstraint(SpringLayout.NORTH, trackArea, 0, SpringLayout.NORTH, contentPanel);
		
		tickSlider = new JSlider();
		tickSlider.setMinimum(0);
//		tickSlider.setEnabled(false);
		tickSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int tick = tickSlider.getValue();
				logState.setDataPoints(
						carDataLogs.get(tick));
				logState.onGameTick(tick);
			}
		});
		contentPanel.add(tickSlider);
		
		contentLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, tickSlider, 0, SpringLayout.HORIZONTAL_CENTER, contentPanel);
		contentLayout.putConstraint(SpringLayout.SOUTH, tickSlider, 0, SpringLayout.SOUTH, contentPanel);
				
		layeredPane.add(contentPanel, new Integer(2));
		
		add(layeredPane);

		setVisible(true);
		
		try {
			conn = new RaceConnection(
					botManager, host, port);
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		connThread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					if(myIndex == 0) {
						conn.createCustomRace(
								ai.getName(),
								track, 
								password, 
								botCount);
					} else if(myIndex > 0 || botCount == 0) {
						conn.joinCustomRace(
								ai.getName(),
								track, 
								password, 
								botCount);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
		
		logThread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				while(1 > 0) {
				
					RaceState state = botManager.getState();
					
					if(state.getCurrentTick() > lastLogTick) {
						
						HashMap<Car, CarDataPoint> logMap = new HashMap<Car, CarDataPoint>();
						for(Entry<Car, CarDataPoint> entry : state.getDataPoints().entrySet()) {
							logMap.put(entry.getKey(), entry.getValue().duplicate());
						}
						
						// Add dummy values for all missed ticks
						while(lastLogTick < state.getCurrentTick()) {
							carDataLogs.add(logMap);
							lastLogTick++;
						}
						
						lastLogTick = state.getCurrentTick();
						
						carDataLogs.add(logMap);

						boolean isMax = false;
						if(tickSlider.getValue() == tickSlider.getMaximum()) {
							isMax = true;
						}
						tickSlider.setMaximum(lastLogTick);
						if(isMax) {
							tickSlider.setValue(lastLogTick);
						}
						
					}

					if(state.isGameInited() && !logState.isGameInited()) {
						logState.onGameInit(
								state.getTrack(),
								state.getCars().toArray(new CarSpecs[state.getCars().size()]),
								state.getSession());
						logState.onYourCar(state.getMyCar());
					}
					
					Car myCar = logState.getMyCar();
					Map<Car, CarDataPoint> data = 
							logState.getDataPoints();
					CarDataPoint myData = data.get(myCar);
					
					if(myData == null || myData.position == null) {
						continue;
					}

					String lstr = "";
					lstr += "*** " + logState.getMyCar().getName() + " ***" + "\n";
					lstr += "Tick:\t\t\t" + logState.getCurrentTick() + "\n";
					lstr += "Velocity:\t\t\t" + myData.velocity + "\n";
					lstr += "Acceleration:\t\t\t" + myData.acceleration + "\n";
					lstr += "Slip angle:\t\t\t" + myData.position.getAngle() + "\n";
					lstr += "Slip angle velocity:\t\t" + myData.slipAngleVelocity + "\n";
					lstr += "Slip angle acceleration:\t\t" + myData.slipAngleAcceleration + "\n";
					lstr += "Angular acceleration:\t\t" + myData.angularAcceleration + "\n";
					lstr += "Max known velocity:\t\t" + myData.knownMaxVelocity + "\n";
					lstr += "Max known acceleration:\t\t" + myData.knownMaxAcceleration + "\n";
					lstr += "Max known deceleration:\t\t" + myData.knownMaxDeceleration + "\n";
					lstr += "Max known angular acceleration:\t" + myData.knownMaxAngularAcceleration;
					lstr += "\n\n";
					lstr += (logState.isMyCarCrashed() ? "I am CRASHED" : "I am on track") + "\n";
					if(logState.getCarInFrontOfMe() != null) {
						lstr += "In front of me: " + logState.getCarInFrontOfMe().getName() + "\n";
					} else {
						lstr += "I am first\n";
					}
					for(Entry<Integer, Car> entry : logState.getCarsByPositions().entrySet()) {
						lstr += entry.getKey() + " " + entry.getValue().getName() + "\n";
					}
					
					String tstr = "";
					
					// Draw the track piees
					for(int pieceIndex = 0; pieceIndex < logState.getTrack().getPieces().length; pieceIndex++) {
						if(logState.getTrack().getPieces()[pieceIndex].hasLaneSwitch()) {
							tstr += "X";
						} else {
							tstr += "-";
						}
					}
					tstr += "\n";
					
					// Draw each lane
					for(int lane = 0; lane < logState.getTrack().getLanes().length; lane++) {
						// Draw all the track pieces
						for(int piece = 0; piece < logState.getTrack().getPieces().length; piece++) {
							boolean foundCar = false;
							String pieceStr = " ";
							// Draw the cars in this piece on this lane
							for(CarSpecs car : logState.getCars()) {
								int carLane = logState.getPiecePosition(car.getCar()).getLane().getStartLaneIndex();
								int carPiece = logState.getPiecePosition(car.getCar()).getPieceIndex();
								if(carLane == lane && carPiece == piece) {
									if(!foundCar) {
										pieceStr = car.getCar().getName().substring(0, 1);
										if(logState.isCarCrashed(car.getCar())) {
											pieceStr = pieceStr.toLowerCase();
										} else {
											pieceStr = pieceStr.toUpperCase();
										}
										foundCar = true;
									} else {
										pieceStr = "M";
									}
								}
							}
							if(!foundCar) {
								tstr += pieceStr;
							} else {
								tstr += pieceStr;
							}
						}
						tstr += "\n";
					}

					trackArea.setText(tstr);
					
					logArea.setText(lstr);
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
					
				}
				
			}
			
		});
		
		logThread.start();
		
		connThread.start();
				
	}
	
	public class DismissWindowListener implements WindowListener {

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowClosing(WindowEvent e) {
			logThread.stop();
			connThread.stop();
			dispose();
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
