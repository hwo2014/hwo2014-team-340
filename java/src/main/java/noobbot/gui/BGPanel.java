package noobbot.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class BGPanel extends JPanel {

	private Image bgImage;
	
	public BGPanel(String path) {
		BufferedImage image = null;
		try {
			bgImage = ImageIO.read(new File("bg.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		int imageW = bgImage.getWidth(null);
		int imageH = bgImage.getHeight(null);
		float imageAspect = 1.0f;
		if(imageW > 0 && imageH > 0) {
			imageAspect = (float)imageW / (float)imageH;
		}
		int panelW = getWidth();
		int panelH = getHeight();
		float panelAspect = (float)panelW / (float)panelH;
		int drawW = panelW;
		int drawH = panelH;
		if(imageAspect > panelAspect) {
			drawH = panelH;
			drawW = (int)(drawH * imageAspect);
		} else {
			drawW = panelW;
			drawH = (int)(drawW / imageAspect);
		}
		g2d.drawImage(bgImage, 0, 0, drawW, drawH, null);
	}
	
}
