package noobbot.ai;

import java.text.DecimalFormat;
import java.util.ArrayList;

import noobbot.engine.RaceState;
import noobbot.network.out.SendMsg;
import noobbot.network.out.Throttle;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.TrackPiece;

public class Mika4AI extends BotAI {

	// uudet viralliset
	double vmax = -1;

	boolean ekatArvotOnJo = false;
	boolean tokatArvotOnJo = false;
	int moneskoTick = -1;
	double y1 = -1;
	double y2 = -1;
	double x1 = -1;
	double x2 = -1;
	double amax = -1;
	double lambda = -1;

	private static int currentLane = -1;
	private static int currentPieceIndex = 0;

	double THROTTLE = 1;
	static TrackPiece[] trackPieces;
	RaceState raceState;
	Car myCar;

	@Override
	public String getDescription() {
		return "This is not the default AI.";
	}

	ArrayList<Double> maxVeloInCorners;

	@Override
	public SendMsg onGameTick(int gameTick) {

		maxVeloInCorners = new ArrayList<Double>();
		for (int i = 0; i < 50; i++) {
			maxVeloInCorners.add((double) 4);
		}

		raceState = getManager().getState();
		myCar = raceState.getMyCar();
		trackPieces = raceState.getTrack().getPieces();

		// RouteFinder
		currentPieceIndex = raceState.getMyPiecePosition().getPieceIndex();
		currentLane = raceState.getMyPiecePosition().getLane()
				.getEndLaneIndex();

		if (raceState.getAcceleration(myCar) != 0 && !ekatArvotOnJo) {
			y1 = raceState.getAcceleration(myCar);
			x1 = gameTick - 1;

			moneskoTick = gameTick;
			ekatArvotOnJo = true;
		}

		if (moneskoTick == gameTick - 1 && ekatArvotOnJo) {
			y2 = raceState.getAcceleration(myCar);
			x2 = gameTick - 1;

			// lasketaan amax ja lambda
			calculateCoefficients(y1, y2, x1, x2);
			// HUOM amax ja lambda arvoja voidaan kayttaa vasta gameTick 3 ->
		}

		// tutkitaan vmax arvoa
		if (raceState.getVelocity(myCar) > vmax) {
			vmax = raceState.getVelocity(myCar);
		}

		int nextCornerIndex = getNextCornerIndex();
		// ollaan cornerissa
		if (trackPieces[currentPieceIndex].getAngle() != 0) {
			// pohditaan seuraavaa corneria
			if (distanceToNextCorner() < distanceNeeded(maxVeloInCorners
					.get(nextCornerIndex))) {
				// Ollaan saapumassa seuraavaan corneriin (taman cornerin
				// jalkeen)
				if (raceState.getVelocity(myCar) > maxVeloInCorners
						.get(nextCornerIndex)) {
					THROTTLE = 0;
				} else {
					// ollaan kaytannossa jo seuraavassa cornerissa
					THROTTLE = maxVeloInCorners.get(currentPieceIndex) / vmax;
				}
			} else {
				// ollaan aiemmassa cornerissa
				if (raceState.getPiecePosition(myCar).getInPieceDistance() > raceState
						.getPieceLength(trackPieces[currentPieceIndex],
								currentLane, currentLane) * 0.5) {
					THROTTLE = 1;
				} else {
					THROTTLE = maxVeloInCorners.get(currentPieceIndex) / vmax;
				}
			}

		} else {
			// pohditaan seuraavaa corneria
			if (distanceToNextCorner() < distanceNeeded(maxVeloInCorners
					.get(nextCornerIndex))) {
				// Ollaan saapumassa corneriin (ei olla viela)
				if (raceState.getVelocity(myCar) > maxVeloInCorners
						.get(nextCornerIndex)) {
					THROTTLE = 0;
				} else {
					// ollaan kaytannossa jo cornerissa
					THROTTLE = maxVeloInCorners.get(currentPieceIndex) / vmax;
				}
			} else {
				// ei tarvitse viela jarruttaa
				THROTTLE = 1;
			}

		}

		// TODO tehdaan jtn etta paastaan kaannoksien lapi oikein (monta
		// kaannosta putkeen ja max velo on se mika pitaakin)

		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		df.setGroupingUsed(false);

		System.out.println(""
				+ gameTick
				+ " "
				+ df.format(THROTTLE)
				+ " "
				+ df.format(raceState.getVelocity(myCar))
				+ " "
				+ df.format(raceState.getAcceleration(myCar))
				+ " "
				+ df.format(amax)
				+ " "
				+ df.format(vmax)
				+ " "
				+ df.format(getNextCornerIndex())
				+ " "
				+ df.format(maxVeloInCorners.get(nextCornerIndex))
				+ " "
				+ df.format(distanceToNextCorner())
				+ " "
				+ df.format(distanceNeeded(maxVeloInCorners
						.get(nextCornerIndex)))
				+ " "
				+ df.format(ticksNeeded(maxVeloInCorners.get(nextCornerIndex)))
				+ " "
				+ df.format(raceState.getTrack()
						.getNextPiece(currentPieceIndex).getAngle())
						+ " " + df.format(raceState.getMyTrackPiece().getAngle()));

		if (THROTTLE > 1) {
			return new Throttle(1, gameTick);
		} else if (THROTTLE < 0) {
			return new Throttle(0, gameTick);
		} else {
			return new Throttle(THROTTLE, gameTick);
		}

	}

	public double convertVelocityToThrottle(double velo) {
		if (vmax < 0) {
			double throttle = velo / vmax;
			if (throttle > 1) {
				return 1;
			} else if (throttle < 0) {
				return 0;
			} else {
				return throttle;
			}
		} else {
			// ei ole viela vmax-arvoa
			return 1;
		}
	}

	public double distanceNeeded(double targetVelo) {
		double ticks = ticksNeeded(targetVelo);
		return (raceState.getVelocity(myCar) * ticks - 0.5 * Math.abs(amax)
				* ticks * ticks * Math.pow(Math.E, (-Math.abs(lambda) * ticks)));
	}

	public double ticksNeeded(double targetVelo) {
		if (targetVelo > 0) {
			return (Math.log(vmax / Math.abs(targetVelo)) / Math.abs(lambda));
		} else {
			return -1;
		}
	}

	public int getNextCornerIndex() {
		int nextCornerIndex = -1;
		for (int i = currentPieceIndex + 1; i < trackPieces.length; i++) {
			if (trackPieces[i].getAngle() != 0) {
				// this piece is a corner (the next corner)
				nextCornerIndex = i;
				break;
			}
		}

		// next corner is behind start line
		if (nextCornerIndex == -1) {
			for (int i = 0; i < trackPieces.length; i++) {
				if (trackPieces[i].getAngle() != 0) {
					// this piece is a corner (the next corner)
					nextCornerIndex = i;
					break;
				}
			}
		}

		return nextCornerIndex;
	}

	public double distanceToNextCorner() {
		// distance from current position to end of the piece
		// double distance = trackPieces[currentPieceIndex].getLength()
		// - raceState.getPiecePosition(myCar).getInPieceDistance();
		double distance = raceState.getPieceLength(
				trackPieces[currentPieceIndex], currentLane, currentLane)
				- raceState.getPiecePosition(myCar).getInPieceDistance();

		// if corner is behind start line
		if (getNextCornerIndex() < currentPieceIndex) {
			for (int i = currentPieceIndex + 1; i < trackPieces.length; i++) {
				// add all next track piece lengths before start line
				// distance += trackPieces[i].getLength();
				distance += raceState.getPieceLength(trackPieces[i],
						currentLane, currentLane);
			}
			for (int i = 0; i < getNextCornerIndex(); i++) {
				// distance += trackPieces[i].getLength();
				distance += raceState.getPieceLength(trackPieces[i],
						currentLane, currentLane);
			}
		} else {
			// corner is on this lap
			for (int i = currentPieceIndex + 1; i < trackPieces.length; i++) {
				if (trackPieces[i].getAngle() != 0) {
					// this piece is a corner (the next corner)
					break;
				} else {
					// this piece isn't a corner
					distance += trackPieces[i].getLength();
				}
			}
		}

		return distance;
	}

	public void calculateCoefficients(double y1, double y2, double x1, double x2) {
		// lasketaan amax ja lambda
		double a = ((Math.log(y1) + Math.log(y2)) * (x1 * x1 + x2 * x2) - (x1 + x2)
				* (x1 * Math.log(y1) + x2 * Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));
		amax = Math.pow(Math.E, a);

		lambda = Math.abs(2 * (x1 * Math.log(y1) + x2 * Math.log(y2))
				- (x1 + x2) * (Math.log(y1) + Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));

		System.out.println("y1 = " + y1 + " y2 = " + y2 + " x1 = " + x1
				+ " x2 = " + x2);
		System.out.println("amax = " + amax + " lambda = " + lambda);
	}

	@Override
	protected String getBaseName() {
		return "khzBot2";
	}
}
