package noobbot.ai;

import noobbot.engine.RaceState;
import noobbot.network.out.SendMsg;
import noobbot.network.out.Throttle;
import noobbot.network.pojo.Car;

public class Mika5AI extends BotAI {

	// Heti kun slipAngle alkaa kaannoksessa tippumaan throttle = 1

	private double THROTTLE;


	@Override
	public String getDescription() {
		return "This is not the default AI.";
	}

	boolean ekatArvotOnJo = false;
	boolean tokatArvotOnJo = false;
	int moneskoTick = -1;
	double y1 = -1;
	double y2 = -1;
	double x1 = -1;
	double x2 = -1;
	double amax = -1;
	double lambda = -1;

	@Override
	public SendMsg onGameTick(int gameTick) {

		if (gameTick < 30) {
			THROTTLE = 1;
		} else {
			THROTTLE = 0;
		}
		
		RaceState raceState = getManager().getState();
		Car myCar = raceState.getMyCar();

		if (raceState.getAcceleration(myCar) != 0 && !ekatArvotOnJo) {
			y1 = raceState.getAcceleration(myCar);
			x1 = gameTick-1;

			moneskoTick = gameTick;
			ekatArvotOnJo = true;
		}

		if (moneskoTick == gameTick -1 && ekatArvotOnJo) {
			y2 = raceState.getAcceleration(myCar);
			x2 = gameTick-1;
			
			//lasketaan amax ja lambda
			calculateCoefficients(y1, y2, x1, x2);
			//HUOM amax ja lambda arvoja voidaan kayttaa vasta gameTick 3 ->
		}

		// laskeLambda(1, 1, 1);

		System.out.println("" + gameTick + " "
				+ raceState.getMyPiecePosition().getInPieceDistance() + " "
				+ raceState.getVelocity(myCar) + " "
				+ raceState.getAcceleration(myCar));

		return new Throttle(THROTTLE, gameTick);
	}

	public boolean ohitetaanko() {

		// onko edessa oleva nopeimmalla kaistalla?

		// onko edessa oleva samalla kaistalla?

		// onko edessa oleva hitaampi
		//
		// if(){
		// //Edessa on joku paikallaan (spawnaus)
		// } else if(){
		// //edessa oleva on hitaampi
		// }

		return false;

	}

	public boolean yritetaankoEstaaOhittaminen() {

		// if(){
		// //takana oleva on nopeampi
		// }

		return false;
	}

	public void calculateCoefficients(double y1, double y2, double x1, double x2) {
		//lasketaan amax ja lambda
		double a = ((Math.log(y1) + Math.log(y2)) * (x1 * x1 + x2 * x2) - (x1 + x2)
				* (x1 * Math.log(y1) + x2 * Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));
		amax = Math.pow(Math.E, a);

		lambda = (2 * (x1 * Math.log(y1) + x2 * Math.log(y2)) - (x1 + x2)
				* (Math.log(y1) + Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));
		
		System.out.println("y1 = " + y1 + " y2 = " + y2 + " x1 = " + x1
				+ " x2 = " + x2);
		System.out.println("amax = " + amax + " lambda = " + lambda);
	}

	@Override
	protected String getBaseName() {
		// TODO Auto-generated method stub
		return null;
	}
}
