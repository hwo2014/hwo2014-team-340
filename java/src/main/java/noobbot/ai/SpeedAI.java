package noobbot.ai;

import noobbot.engine.AIState;
import noobbot.engine.RaceState;
import noobbot.engine.RouteCommander;
import noobbot.network.MsgWrapper;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;

public class SpeedAI extends BotAI {
	
	private Track track;
	private AIState aiState;
	private RouteCommander routeCommander;
	
	private RaceState raceState;
	
	private double newThrottle = 0.888d;
	private double maxAngularAcceleration = -1.0d;
	private double lastMaxAngularAcceleration = .0d;
	
	
	public SpeedAI() {
		this.lastMaxAngularAcceleration = 0.0d;
		this.aiState = new AIState();	
	}
	
	@Override
	public String getBaseName() {
		return "SpeedBot";
	}

	@Override
	public String getDescription() {
		return "This is Tommi's speed AI.";
	}

	@Override
	public void onRawMessage(MsgWrapper rawMsg) {
	}

	@Override
	public SendMsg onGameTick(int gameTick) {
		
		if (this.routeCommander != null && this.routeCommander.handleRouting()) {
			if (this.routeCommander.getLaneCommand() != 0) {
				this.routeCommander.setWaitingForRoute(false);
				System.out.println("Route switch!");
				return new SwitchLane(this.routeCommander.getLaneCommand(), gameTick);
			}
		}
		
		return new Throttle(newThrottle, gameTick);
	}

	@Override
	public void onYourCar(Car car) {
	}

	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
		this.track = track;
		this.routeCommander = new RouteCommander(this.getManager().getState(), track);
		this.aiState.setAiName(this.getManager().getState().getMyCar().getName());
		this.raceState  = this.getManager().getState();
	}

	@Override
	public void onGameStart() {
	}

	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		newThrottle = processPositions(carPositions);
	}

	@Override
	public void onCrash(Car car) {
		if (car.equals(this.raceState.getMyCar())) {
			int absoluteRadius = Math.abs(this.aiState.getCurrentPiece().getRadius());
			if (absoluteRadius > 0 ) {
				if (this.maxAngularAcceleration > this.lastMaxAngularAcceleration || this.maxAngularAcceleration < 0.0d)
					this.maxAngularAcceleration = this.lastMaxAngularAcceleration;
				else
					this.maxAngularAcceleration *= 0.9d;
					
				System.out.println("Piece "+this.aiState.getCurrentPieceIndex()+" Max angular acceleration: "+this.maxAngularAcceleration+ " speed & max angle: "+this.aiState.getCurrentSpeed()+" & "+" & "+this.aiState.getMaxAngle());
			}
			else {
				this.maxAngularAcceleration *= 0.9d;
				System.out.println("Could not determine max angular acceleration");	
			}
		}
	}

	@Override
	public void onSpawn(Car car) {
	}

	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime,
			Ranking ranking) {
		//System.out.println("MAX ANGULAR ACC: "+maxAngularAcceleration);
	}

	@Override
	public void onDnf(Car car, String reason) {
	}

	@Override
	public void onFinish(Car car) {
	}

	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
	}

	@Override
	public void onTournamentEnd() {
	}
	
	private double processPositions(CarPosition[] positions) {
		
		this.aiState.updateCurrentState(positions, track);
		
		//TODO: find own car and not use the first item
		double throttlePosition = 1.0d;
			
		if (this.aiState.getCurrentPiece().getRadius() != 0 && this.aiState.getCurrentSpeed() > 0.0f) {
			if (this.aiState.getPreviousPiece().getRadius() == 0 )
				this.lastMaxAngularAcceleration = 0;
		
			if (this.lastMaxAngularAcceleration < this.aiState.getAngularAcceleration())
				this.lastMaxAngularAcceleration = this.aiState.getAngularAcceleration();
		}
		
		//if we don't know the limit => pedal to medal
		if (maxAngularAcceleration > 0.0d) {
			double targetSpeed = -1.0d;
			
			//use the curve beginning to adjust speed, when we are drifting use the the exit angle
			//Seems to be faster without the angle check
			if (this.aiState.getCurrentPiece().getRadius() != 0 /*&& Math.abs(this.aiState.getCurrentAngle()) < 0.01d*/) {	
				targetSpeed = Math.sqrt(maxAngularAcceleration*Math.abs(this.aiState.getCorneringRadius()));
				
				//if (this.aiState.getCurrentSpeed() > 1.0d)
				//	System.out.println("angAcc: "+this.aiState.getAngularAcceleration());
			}
			
			//if we are above target speed brake
			if ((targetSpeed > 0.0d && this.aiState.getCurrentSpeed() > targetSpeed)) {
				throttlePosition = 0.0f;
				//System.out.print(" TOO FAST FOR CURRENT ");
				//System.out.println("ADJUST Target/Current speed:" + targetSpeed+"/"+this.aiState.getCurrentSpeed() + "Throttle:" + throttlePosition);
			}
					
			if (this.aiState.getDeceleration() < 0.0d) {
				double distanceToTurn[] = track.getDistanceToNextTurn(this.aiState.getCurrentPieceIndex(), this.aiState.getInPieceDistance(), this.aiState.getCurrentLane());	
				//TODO: take account the planned route
				int nextAdjustedRadius = track.getPieces()[(int)distanceToTurn[1]].getAdjustedRadius(this.aiState.getCurrentLane(), track.getLanes());
				
				double nextTurnTargetSpeed = Math.sqrt(maxAngularAcceleration*Math.abs(nextAdjustedRadius));
				//t= (v-v0)/a
				//x = v0*t+0.5*a*t^2
				if (nextTurnTargetSpeed < this.aiState.getCurrentSpeed()) {
					double time = (nextTurnTargetSpeed - this.aiState.getCurrentSpeed()) / this.aiState.getDeceleration();
					double endPosition = this.aiState.getCurrentSpeed()*time+0.5*this.aiState.getDeceleration()*time*time;
					
					if (endPosition > distanceToTurn[0]) {
						throttlePosition = 0.0f;
						//System.out.print(" TOO FAST FOR NEXT ");
					}
				}	
			
			}
			
							
			//At the moment speed is unit is distance/(one)tick, calculate the angle increase rate
			if (this.aiState.getCurrentSpeed() > 0.0f && this.aiState.getCurrentPiece().getRadius() != 0) {
				double angleDelta = ( this.aiState.getCurrentAngle() - this.aiState.getPreviousAngle())/this.aiState.getCurrentSpeed(); 
				
				double absoluteExitAngle = Math.abs(this.aiState.getCurrentAngle()+angleDelta*track.getDistanceToNextStraight(this.aiState.getCurrentPieceIndex(), this.aiState.getInPieceDistance(), this.aiState.getCurrentLane()));
				//System.out.println("Ang: "+ this.aiState.getCurrentAngle()+" ExitAng: "+absoluteExitAngle+" AngDelta: "+angleDelta);
				
				if ( absoluteExitAngle >= Math.abs(this.aiState.getMaxAngle())) {
					throttlePosition = 0.0f;
					//System.out.println("distanceToStraight: "+ track.getDistanceToNextStraight(this.aiState.getCurrentPieceIndex(), this.aiState.getInPieceDistance(), this.aiState.getCurrentLane()));
				}	
			}
		}
		
		if (this.aiState.checkBrakesIfNeeded()) {
			throttlePosition = 0.0f;
		}
		
		return throttlePosition;
	}

}
