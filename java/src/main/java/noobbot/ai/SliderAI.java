package noobbot.ai;

import java.util.ArrayList;

import noobbot.engine.AIState;
import noobbot.engine.AngleModel;
import noobbot.engine.RaceState;
import noobbot.engine.RouteCommander;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.out.Turbo;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;

public class SliderAI extends BotAI {

	private AngleModel angleModel = new AngleModel();
	private ArrayList<Double> targetAngularAccelerations = new ArrayList<Double>();
	private boolean crashOnLap = false;
	//private boolean firstMinimumFound = false;
	private double maxAngularAccelerationFullLap = -1.0d;
	
	private boolean aLimitCandidateFound = false;
	private double cornerMaxSlipAngleAcc = 0.0d;
	private double cornerMaxAngularAcc = 0.0d;
	
	private double throttle;
	private Track track;
	private AIState aiState;
	private RouteCommander routeCommander;
	private RaceState raceState;
	
	private int strategy = 1; //0=safe (do not increase targetAngularAcceleration), 1=search limit
	
	public SliderAI() {
		this.aiState = new AIState();	
		
		//Test values
		//this.targetAngularAccelerations.add(0.90d);
		//this.targetAngularAccelerations.add(0.43d);
		//this.maxAngularAccelerationFullLap = 0.47;
		//this.strategy = 0;
	}
	
	@Override
	public String getBaseName() {
		return "SliderBot";
	}

	@Override
	public String getDescription() {
		return "This is turbo slider AI.";
	}
	
	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
		this.track = track;
		
		this.raceState  = this.getManager().getState();
		
		this.routeCommander = new RouteCommander(this.getManager().getState(), track);
		this.aiState.setAiName(this.getManager().getState().getMyCar().getName());
	}

	@Override
	public SendMsg onGameTick(int gameTick) {
		
		//this.raceState.outputMyMeasurements();
		//this.angleModel.addSpeedY(this.raceState.getMyAngle(), this.raceState.getMyVelocity());
		
		if (this.raceState.getMyTrackPiece().getRadius() == 0)
			this.angleModel.getReadyForNextMeasurement();
		
		if (this.raceState.getMyTrackPiece().getRadius() != 0)
		{
			if (this.raceState.getMyAngle() > 0.0d) {
				this.angleModel.addMeasuredAngle(this.raceState.getMyAngle());
				//System.out.println(this.raceState.getMyAngle());
				this.angleModel.addAccelerationIfMissing(this.raceState.getMyAngularAcceleration());
			}			
		}
		
		/*if (state.getMyPiecePosition().getLap()==0)
			return new Throttle(0.55, gameTick);
		else if (state.getMyPiecePosition().getLap()==1)
			return new Throttle(0.6, gameTick);
		else
			return new Throttle(0.65, gameTick);
		
		if (state.getMyPiecePosition().getLap()==0)
			return new Throttle(0.9, gameTick);
		else if (state.getMyPiecePosition().getLap()==1)
			return new Throttle(0.95, gameTick);
		else
			return new Throttle(1.00, gameTick);*/
		
		/*if (state.getMyPiecePosition().getLap()==0)
			return new Throttle(0.4, gameTick);
		else if (state.getMyPiecePosition().getLap()==1)
			return new Throttle(0.5, gameTick);
		else
			return new Throttle(0.6, gameTick);*/
		
		int currentPieceIndex = this.raceState.getMyPiecePosition().getPieceIndex();
		//TODO: How many turbos available & do not use turbo if it is currently activated this.raceState.getMyTurboActivatedTick()
		if (this.raceState.isMyTurboAvailable() && this.routeCommander.isGoodTimeForTurbo(currentPieceIndex)) {
			this.raceState.setMyTurboActivated();
			return new Turbo("pow",gameTick);
		}
		
		if (this.routeCommander != null && this.routeCommander.handleRouting()) {
			if (this.routeCommander.getLaneCommand() != 0) {
				this.routeCommander.setWaitingForRoute(false);
				System.out.println("Route switch: "+this.routeCommander.getLaneCommand());
				return new SwitchLane(this.routeCommander.getLaneCommand(), gameTick);
			}
		}
		
		return new Throttle(this.throttle, gameTick);
		//return new Throttle(0.92d, gameTick);
	}
	
	@Override
	public void onCrash(Car car) {
		if (car.equals(this.raceState.getMyCar())) {
			cornerMaxSlipAngleAcc = 0.0d;
			this.decreaseTargetAngularAcceleration();
			this.crashOnLap = true;
			this.angleModel.outputCoefficients();
			this.angleModel.resetALimitMeasurement();
			System.out.println("CRASHED Current angAcc target: "+this.getCurrentAngularAccelerationTarget());	
		}
	}
	
	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime, Ranking ranking) {
		if (car.equals(this.raceState.getMyCar())) {
			//System.out.println("\n--- Outputing mesurements ---");
			//this.angleModel.outputSpeedYs();
			//this.angleModel.outputMeasurements();
			//this.angleModel.outputAngles();		
			
			if (this.crashOnLap == false) {
				//this.firstMinimumFound = true;
				this.maxAngularAccelerationFullLap = this.getCurrentAngularAccelerationTarget();
				if (this.strategy == 1)
					increaseTargetAngularAcceleration();	
			}
			
			this.crashOnLap = false;
			System.out.println("Current angAcc target: "+this.getCurrentAngularAccelerationTarget());
			this.angleModel.outputCoefficients();
		}
	}
	
	private void increaseTargetAngularAcceleration() {
		if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last+difference/2.0d);
		}
	}
	
	private double getCurrentAngularAccelerationTarget() {
		
		if (this.strategy == 0)
			return this.maxAngularAccelerationFullLap;
		
		if (this.targetAngularAccelerations.size() > 0)
			return this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-1);
		
		return Double.MAX_VALUE;
	}
	
	private void decreaseTargetAngularAcceleration() {
		if (this.targetAngularAccelerations.size() == 0) {
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration());
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration()/2.0d);
		}
		else if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last-difference/2.0d);
			
			//if (this.firstMinimumFound)
			//	this.targetAngularAccelerations.add(last-difference/2.0d);
			//else
			//this.targetAngularAccelerations.add(last-difference);
		}
	}
	
	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		this.throttle = processPositions(carPositions);
	}
	
	public double getTargetCorneringSpeed(boolean startFromCurrent,double distanceToStraight, double adjustedRadius, double turnAngle) {
		
		if (this.raceState.isMyCarCrashed())
			return -1.0;
		
		//boolean crashedOnce = this.targetAngularAccelerations.size() != 0;
		if (this.angleModel.modelReady() && angleModel.isMinAngularLimitFound()) {
			
			double angle = 0.0d;
			double angleVelocity = 0.0d;
			if (startFromCurrent) {
				angle = this.raceState.getMyAngle();
				angleVelocity = this.raceState.getMySlipAngleVelocity();
			}
			
			double angularAcceleration = this.angleModel.getTurnTargetAcceleration(distanceToStraight, adjustedRadius, turnAngle, 
					this.raceState.getHighestKnownMaxAngularAcceleration(), this.raceState.getHighestKnownMaxAngle(),
					angle, angleVelocity);
			
			return Math.sqrt(Math.abs(angularAcceleration)*Math.abs(adjustedRadius));
		}
		else {
			
			//return Math.sqrt(0.5d*Math.abs(adjustedRadius));
			return Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(adjustedRadius));
		}
		
	}
	
	private double processPositions(CarPosition[] positions) {
		
		this.aiState.updateCurrentState(positions, track);
		this.angleModel.measureVariablesForModel(raceState.getCurrentTick(), this.raceState.getMyAngularAcceleration(), 
				this.raceState.getMyAngle(), this.raceState.getMySlipAngleAcceleration(), 
				this.raceState.getMySlipAngleVelocity(), this.raceState.getMyTrackPiece().getRadius(),
				this.raceState.getMyTrackPiece().hasLaneSwitch());
		
		System.out.println("angAcc "+this.raceState.getMyAngularAcceleration()+" angle"+this.raceState.getMyAngle() );
		
		//TODO: find own car and not use the first item
		double throttlePosition = 1.0d;
		
		int currentPieceIndex = this.raceState.getMyPiecePosition().getPieceIndex();
		
		double targetSpeed = -1.0d;
		//use the curve beginning to adjust speed, when we are drifting use the the exit angle
		//Seems to be faster without the angle check
		if (this.raceState.getMyTrackPiece().getRadius() != 0 /*&& Math.abs(this.aiState.getCurrentAngle()) < 0.01d*/) {	
			double distanceToStraight = track.getDistanceToNextStraight(raceState.getMyPiecePosition().getPieceIndex(), raceState.getMyPiecePosition().getInPieceDistance(), raceState.getMyLane().getEndLaneIndex());
			targetSpeed = this.getTargetCorneringSpeed(true, distanceToStraight, this.raceState.getMyTrackPiece().getAdjustedRadius(this.raceState.getMyLane().getEndLaneIndex(), track.getLanes()), this.raceState.getMyTrackPiece().getAngle());
					//Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(this.aiState.getCorneringRadius()));
			
			//if (this.raceState.getMyVelocity() > 1.0d && !this.raceState.isMyCarCrashed())
				//System.out.println("angAcc: "+this.aiState.getAngularAcceleration()+" Target speed: "+targetSpeed);
			//cornerMaxSlipAngleAcc = Math.max(cornerMaxSlipAngleAcc, raceState.getMySlipAngleAcceleration());
			//cornerMaxAngularAcc = Math.max(cornerMaxAngularAcc, raceState.getMySlipAngleAcceleration());
		}
		/*else {
			
			if (this.track.getPrevPiece(currentPieceIndex).getRadius() != 0) {
				if (cornerMaxSlipAngleAcc < 0.01d) {
					aLimitCandidateFound = true;
					System.out.println("FOUND A CANDIDATE FOR ALIMIT: "+angleModel.getALimit());
				}
				else
					decreaseTargetAngularAcceleration();
			}
			cornerMaxSlipAngleAcc = 0.0d;
			
		}*/
		
		//if (this.raceState.getMyAngle() > 0.1d)
		//	System.out.print(" ANGLE: "+this.raceState.getMyAngle());
		
		//if we are above target speed => brake
		if ((targetSpeed > -0.1d && this.raceState.getMyVelocity() > targetSpeed)) {
			throttlePosition = 0.0f;
			System.out.println(targetSpeed+" TARGET SMALLER THAN CURRENT SPEED  "+this.raceState.getMyVelocity()+" radius "+this.aiState.getCorneringRadius());
			//System.out.println("ADJUST Target/Current speed:" + targetSpeed+"/"+this.raceState.getMyVelocity() + "Throttle:" + throttlePosition);
		}
				
		if (this.aiState.getDeceleration() < 0.0d) {
			double distanceToTurn[] = track.getDistanceToNextTurn(currentPieceIndex, this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex());	
			//TODO: take account the planned route
			TrackPiece nextPiece = track.getPieces()[(int)distanceToTurn[1]];
			int nextAdjustedRadius = nextPiece.getAdjustedRadius(this.raceState.getMyLane().getEndLaneIndex(), track.getLanes());
			
			double distanceToStraight = track.getDistanceToNextStraight((int)distanceToTurn[1], 0.0d, raceState.getMyLane().getEndLaneIndex());
			double nextTurnTargetSpeed = this.getTargetCorneringSpeed(false, distanceToStraight, nextAdjustedRadius, nextPiece.getAngle());
					//Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(nextAdjustedRadius));
			
			//t= (v-v0)/a
			//x = v0*t+0.5*a*t^2
			if (nextTurnTargetSpeed < this.raceState.getMyVelocity()) {
				double time = (nextTurnTargetSpeed - this.raceState.getMyVelocity()) / (this.aiState.getDeceleration()); //-this.raceState.getMyKnownMaxDeceleration()/2.0d
				double endPosition = this.raceState.getMyVelocity()*time+0.5*(this.aiState.getDeceleration())*time*time;
				
				if (endPosition > distanceToTurn[0]) {
					throttlePosition = 0.0f;
					System.out.print(" TOO FAST FOR NEXT! Dist: "+distanceToTurn[0]);
				}
			}	
		
		}
		
						
		//At the moment speed is unit is distance/(one)tick, calculate the angle increase rate
		/*if (this.raceState.getMyVelocity() > 0.0f && this.raceState.getMyTrackPiece().getRadius() != 0) {
			double angleDelta = ( this.aiState.getCurrentAngle() - this.aiState.getPreviousAngle())/this.raceState.getMyVelocity(); 
			
			double absoluteExitAngle = Math.abs(this.aiState.getCurrentAngle()+angleDelta*track.getDistanceToNextStraight(currentPieceIndex, this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex()));
			System.out.println("Ang: "+ this.aiState.getCurrentAngle()+" ExitAng: "+absoluteExitAngle+" AngDelta: "+angleDelta);
			
			if ( absoluteExitAngle >= Math.abs(this.aiState.getMaxAngle())) {
				throttlePosition = 0.0f;
				System.out.println("distanceToStraight: "+ track.getDistanceToNextStraight(currentPieceIndex, this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex()));
			}	
		}*/
		
		if (this.aiState.checkBrakesIfNeeded()) {
			throttlePosition = 0.0f;
		}
		
		return throttlePosition;
	}
	
	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
		//Switch to safe tactic for the race
		//TODO: do not increase target angularAcceleration after the final qualification lap
		this.strategy = 0;
		System.out.println("After session adjustments");
	}

}
