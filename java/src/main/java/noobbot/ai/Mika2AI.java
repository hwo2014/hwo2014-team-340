package noobbot.ai;

import java.text.DecimalFormat;

import noobbot.engine.RouteFinder;
import noobbot.network.MsgWrapper;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;

public class Mika2AI extends BotAI {

	// TODO: lambdan iteraatio

	// TODO: kun crashataan lambdan arvoa pitaa muuttaa (edelliseen talla
	// hetkella)

	// TODO: laske oma radius

	RouteFinder routeFinder;
	Track track;
	private boolean userRouting = true;
	private boolean waitingForRoute = true;
	private int laneCommand = 0;

	private boolean isCurrentlyOnSwitch;
	private int latestSwitchIndex;
	private int currentLane;
	private int currentPieceIndex;

	float THROTTLE = 1;
	TrackPiece[] trackPieces;
	float LAP_LENGTH = 0;
	float currentPosition = 0;
	float previousPosition = 0;
	float ipd_curr = 0;
	float ipd_prev = 0;
	float velo_curr = 0;
	float velo_prev = 0;
	float velo_max;;
	float time = (float) 1 / 60;
	float acceleration = 0;

	// alltime maximum values
	float v0;
	float a0;

	float lambda = (float) 10;
	float lambda_crashValue = 0;
	float lambda_previousValue = 0;

	CarPosition[] cp;

	// ollaan tulossa corneriin eli ei olla viela
	boolean arrivingToCorner = false;
	boolean atc = false;
	// ollaan jo cornerissa
	boolean atCorner = false;
	boolean ac = false;

	public Mika2AI() {
		currentLane = -1;
		currentPieceIndex = 0;
		isCurrentlyOnSwitch = false;
		latestSwitchIndex = -1;
	}

	@Override
	public String getBaseName() {
		return "khzBot";
	}

	@Override
	public String getDescription() {
		return "This is not the default AI.";
	}

	@Override
	public void onRawMessage(MsgWrapper rawMsg) {
	}

	@Override
	public SendMsg onGameTick(int gameTick) {

		if (this.userRouting && currentLane != -1 && this.isNewRouteCommandNeeded(isCurrentlyOnSwitch)) {

			if (laneCommand != 0) {
				// System.out.println("lane command: "+laneCommand);
				waitingForRoute = false;
				return new SwitchLane(laneCommand, gameTick);
			}
		}

		return new Throttle(THROTTLE, gameTick);
	}

	@Override
	public void onYourCar(Car car) {
	}

	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {

		for (TrackPiece trackPiece : track.getPieces()) {
			LAP_LENGTH += trackPiece.getLength();
			// how far is the piece from the startline
			trackPiece.setDistanceFromStart(LAP_LENGTH);
		}
		trackPieces = track.getPieces();
		this.track = track;

		routeFinder = new RouteFinder(track);
	}

	@Override
	public void onGameStart() {
	}

	@Override
	public void onCarPositions(CarPosition[] carPositions) {

		// RouteFinder
		currentPieceIndex = carPositions[0].getPiecePosition().getPieceIndex();
		currentLane = carPositions[0].getPiecePosition().getLane().getEndLaneIndex();
		TrackPiece currentPiece = track.getPieces()[currentPieceIndex];
		// TrackPiece nextPiece = track.getNextPiece(currentPieceIndex);
		isCurrentlyOnSwitch = currentPiece.hasLaneSwitch();

		// previous distance travelled
		previousPosition = currentPosition;

		// current to previous
		ipd_prev = ipd_curr;
		velo_prev = velo_curr;

		cp = carPositions;

		if (carPositions.length > 0) {
			ipd_curr = (float) carPositions[0].getPiecePosition().getInPieceDistance();
		}

		// currentPosition
		currentPosition = 0;
		currentPosition += (float) ipd_curr;

		for (int i = 0; i < carPositions[0].getPiecePosition().getPieceIndex(); i++) {
			// add all previous piecelengths
			currentPosition += (float) trackPieces[i].getLength();
		}

		int lap = 0;

		// add all previous laplengths
		if (carPositions[0].getPiecePosition().getLap() >= 0) {
			lap = carPositions[0].getPiecePosition().getLap();
			currentPosition += (float) (lap * LAP_LENGTH);
			previousPosition += (float) (lap * LAP_LENGTH);
		}

		velo_curr = (float) ((currentPosition - previousPosition) / time);
		acceleration = (float) (2 * (currentPosition - previousPosition - velo_prev * time) / (time * time));

		calc_a0();
		calc_v0();

		if (distanceToNextCorner() < distanceNeeded()) {
			if (atc) {
				velo_max = nextVelocityMax();
				atc = false;
			}
			// arriving to corner
			arrivingToCorner = true;
		} else {
			arrivingToCorner = false;
			atc = true;
		}

		if (trackPieces[carPositions[0].getPiecePosition().getPieceIndex()].getAngle() != 0) {
			// at corner
			atCorner = true;
		} else {
			atCorner = false;
		}

		if (arrivingToCorner) {
			if (velo_curr > velo_max) {
				THROTTLE = 0;
			}
		}

		if (atCorner) {
			convertVelocityToThrottle(velo_max);
		}

		// everywhere else
		if (!arrivingToCorner && !atCorner) {
			THROTTLE = 1;
		}

		if (distanceToNextCorner() == -1) {
			THROTTLE = 1;
		}

		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		df.setGroupingUsed(false);

		System.out.println("" + df.format(THROTTLE) + " " + df.format(velo_curr) + " " + df.format(velo_max) + " "
				+ df.format(nextVelocityMax()) + " " + df.format(distanceToNextCorner()) + " "
				+ df.format(distanceNeeded()) + " " + df.format(currentPosition) + " " + df.format(v0) + " "
				+ df.format(a0) + " " + df.format(lambda));

	}

	public boolean isNewRouteCommandNeeded(boolean isSwitchPiece) {
		if (isSwitchPiece && latestSwitchIndex != currentPieceIndex) {
			waitingForRoute = true;
			latestSwitchIndex = currentPieceIndex;
		}

		if (!waitingForRoute || isSwitchPiece)
			return false;

		// Calculate new route
		laneCommand = routeFinder.calculateNextLane(currentPieceIndex, currentLane, 0);

		return true;
	}

	public float nextVelocityMax() { // next corner
		return (float) (Math.sqrt(nextCornerRadius() * a0 * Math.tan(Math.abs(nextCornerAngle() * Math.PI / 180))));
	}

	public float currentCornerAngle() { // current corner
		return (float) trackPieces[cp[0].getPiecePosition().getPieceIndex()].getAngle();
	}

	public float currentCornerRadius() { // current corner
		// return
		// trackPieces[cp[0].getPiecePosition().getPieceIndex()].getRadius();

		// TODO
		return trackPieces[cp[0].getPiecePosition().getPieceIndex()].getAdjustedRadius(currentLane, track.getLanes());
	}

	public float nextCornerAngle() {
		// if current lap is not first lap
		while (currentPosition > LAP_LENGTH) {
			currentPosition -= LAP_LENGTH;
		}

		float angle = 0;
		if (trackPieces.length > 1) {
			for (int i = 0; i < trackPieces.length; i++) {
				if (trackPieces[i].getDistanceFromStart() > currentPosition && trackPieces[i].getAngle() != 0) {
					angle = (float) trackPieces[i].getAngle();
					break;
				}
			}
		}

		// next corner is behind startline
		if (angle == 0) {
			if (trackPieces.length > 1) {
				for (int i = 0; i < trackPieces.length; i++) {
					if (trackPieces[i].getAngle() != 0) {
						angle = (float) trackPieces[i].getAngle();
						break;
					}
				}
			}

		}

		return angle;
	}

	public float nextCornerRadius() {
		// if current lap is not first lap
		while (currentPosition > LAP_LENGTH) {
			currentPosition -= LAP_LENGTH;
		}

		float radius = 0;
		if (trackPieces.length > 1) {
			for (int i = 0; i < trackPieces.length; i++) {
				if (trackPieces[i].getDistanceFromStart() > currentPosition && trackPieces[i].getAngle() != 0) {
					// radius = trackPieces[i].getRadius();

					// TODO
					radius = trackPieces[i].getAdjustedRadius(currentLane, track.getLanes());

					break;
				}
			}
		}

		// next corner is behind startline
		if (radius == 0) {
			if (trackPieces.length > 1) {
				for (int i = 0; i < trackPieces.length; i++) {
					if (trackPieces[i].getAngle() != 0) {
						// radius = trackPieces[i].getRadius();

						// TODO
						radius = trackPieces[i].getAdjustedRadius(currentLane, track.getLanes());

						break;
					}
				}
			}

		}

		return radius;
	}

	public void convertVelocityToThrottle(float velo) {
		if (velo != 0) {
			float throttle = (float) (velo / v0);

			if (throttle > 1) {
				THROTTLE = 1;
			} else if (throttle < 0) {
				THROTTLE = 0;
			} else {
				THROTTLE = throttle;
			}
		} else {
			System.out.println("---------------- convertVelocityToThrottle: velo == 0");
		}
	}

	public float distanceNeeded() {
		return (float) ((velo_curr * timeNeeded() - 0.5 * Math.abs(a0) * timeNeeded() * timeNeeded()
				* Math.pow(Math.E, (-lambda * timeNeeded()))));
	}

	public float timeNeeded() {
		return (float) Math.abs(Math.log1p(nextVelocityMax() / v0));
	}

	public float distanceToNextCorner() {
		// if current lap is not first lap
		while (currentPosition > LAP_LENGTH) {
			currentPosition -= LAP_LENGTH;
		}
		float distance = -1;
		for (int i = 0; i < trackPieces.length; i++) {
			if (trackPieces[i].getDistanceFromStart() > currentPosition && trackPieces[i].getAngle() != 0) {
				distance = trackPieces[i].getDistanceFromStart() - currentPosition;
				break;
			}
		}

		if (distance == -1) {
			// next corner is behind startline
			for (int i = 0; i < trackPieces.length; i++) {
				if (trackPieces[i].getAngle() != 0) {
					distance = trackPieces[i].getDistanceFromStart() + LAP_LENGTH - currentPosition;
					break;
				}
			}
		}

		return distance;
	}

	@Override
	public void onCrash(Car car) {
		// TODO: lambda_previousValue pitaa olla ennen crashaus kaannosta tai
		// mahdollisesti tutkia lambdaa tiukimmassa kaannoksessa

		// talloin tiedetaan etta lambda_crashValue on liian pieni
		lambda_crashValue = lambda;
		lambda = lambda_previousValue;
	}

	@Override
	public void onSpawn(Car car) {
	}

	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime, Ranking ranking) {
		System.out
				.println("----------------------------------LAP FINISHED--------------------------------------------");
	};

	@Override
	public void onDnf(Car car, String reason) {
	}

	@Override
	public void onFinish(Car car) {
	}

	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
	}

	@Override
	public void onTournamentEnd() {
	}

	public void calc_v0() {
		float uusi = (float) Math.abs(velo_curr / (Math.pow(Math.E, -lambda * time)));
		// rajoitetaan ettei voi menna nopeampaa kuin auton maksiminopeus
		if (uusi > v0 && uusi < 600) {
			v0 = uusi;
		}

		// TODO: taalta myos lambda-arvo jota verrataan a0:n lambdaan?

		// jos tama paalla niin menee kovempaa (auton max speed)
		// v0 = 600;

		// TODO: ei taida saavuttaa koskaan oikeaa lukua (oikea luku on auton
		// maksiminopeus)

	}

	public void calc_a0() {
		// rajoitetaan ettei voi kiihtya nopeampaa kuin auto
		if (Math.abs(acceleration) > a0 && Math.abs(acceleration) < 4000) {
			a0 = Math.abs(acceleration);
		}

		// TODO: jokin raja-arvo uudelle lambdalle (1.05(suomi)...>2(saksa),
		// optimaalinen lambda on 1.212(suomi), mutta koska auton
		// maksiminopeutta ei koskaan saavuteta, niin vaikea arvioida) (ei
		// tarvita jos saadaan iteroitua, tai kaytetaan yhtena ehtona
		// iteroinnissa, jos raja-arvolle loytyy jarkeva selitys)

		// TODO: jokin logiikka lambdan muuttumiselle (eli iteroidaan se oikea
		// arvo sielta, voitaisiin hyodyntaa v0:n lambdaa tai jos loydetaan muu
		// yhteys)

		// TODO: derivoidaan lambda yhtalo a(t)=a0*e^(-lambda*t) -> a'(t) =
		// -lambda*a0*e^(-lambda*t), josta voitaisiin ratkaista lambda

		// TODO: a(0) = a0*e^0 = a0

		// lambda = (float) 1.212;

		float a = Math.abs(acceleration);
		float uusi_lambda_ehdokas = (float) (-Math.log1p(a / a0) / time);
		if (Math.abs(uusi_lambda_ehdokas) < lambda && Math.abs(uusi_lambda_ehdokas) > lambda_crashValue
				&& Math.abs(uusi_lambda_ehdokas) > 1.05) {
			lambda_previousValue = lambda;
			lambda = Math.abs(uusi_lambda_ehdokas);
		}

	}

}
