package noobbot.ai;


import java.util.ArrayList;

import noobbot.engine.AIState;
import noobbot.engine.RaceState;
import noobbot.engine.RouteCommander;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.out.Turbo;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;

public class AllBotsUniteAI extends BotAI {

	// (temporary) maximum velocities in corners
	//ArrayList<Double> maxVeloInCorners;

	// recorded max velocity for conversion of velocity to throttle
	private double vmax = -1;

	// calculation of distance needed to decelerate
	private boolean ekatArvotOnJo = false;
	private int moneskoTick = -1;
	private double y1 = -1;
	private double y2 = -1;
	private double x1 = -1;
	private double x2 = -1;
	private double amax = -1;
	private double lambda = -1;

	private double THROTTLE = 1;
	TrackPiece[] trackPieces;
	RaceState raceState;
	Car myCar;
	
	private ArrayList<Double> targetAngularAccelerations = new ArrayList<Double>();
	private boolean crashOnLap = false;
	private double maxAngularAccelerationFullLap = -1.0d;
	private Track track;
	//private AIState aiState;
	private RouteCommander routeCommander;
	
	private int strategy = 1; //0=safe (do not increase targetAngularAcceleration), 1=search limit

	public AllBotsUniteAI() {
		//this.aiState = new AIState();	
	}
	
	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
		this.track = track;
		
		this.raceState  = this.getManager().getState();
		
		this.routeCommander = new RouteCommander(this.getManager().getState(), track);
		//this.aiState.setAiName(this.getManager().getState().getMyCar().getName());
	}
	
	@Override
	protected String getBaseName() {
		return "AllBotsUnited";
	}

	@Override
	public String getDescription() {
		return "This is hopefully the final AI.";
	}

	@Override
	public SendMsg onGameTick(int gameTick) {

		// (temp) maximum velocities in corners
		/*maxVeloInCorners = new ArrayList<Double>();
		for (int i = 0; i < 50; i++) {
			maxVeloInCorners.add((double) 4);
		}*/

		myCar = raceState.getMyCar();
		trackPieces = raceState.getTrack().getPieces();

		// calulation of amax and lambda values
		if (raceState.getAcceleration(myCar) != 0 && !ekatArvotOnJo) {
			y1 = raceState.getAcceleration(myCar);
			x1 = gameTick - 1;

			moneskoTick = gameTick;
			ekatArvotOnJo = true;
		}

		// calulation of amax and lambda values
		if (moneskoTick == gameTick - 1 && ekatArvotOnJo) {
			y2 = raceState.getAcceleration(myCar);
			x2 = gameTick - 1;

			// calculate amax ja lambda for deceleration
			calculateCoefficients(y1, y2, x1, x2);
			// OBS! amax and lambda values are operable after tick 3
		}

		// save highest velocity
		if (raceState.getVelocity(myCar) > vmax) {
			vmax = raceState.getVelocity(myCar);
		}

		int corneringRadius = this.raceState.getMyTrackPiece().getAdjustedRadius(this.raceState.getMyLane().getEndLaneIndex(), track.getLanes());
		double currentTurnTargetSpeed = Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(corneringRadius));
		
		double distanceToTurn[] = track.getDistanceToNextTurn(raceState.getMyPiecePosition().getPieceIndex(), this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex());	
		//TODO: take account the planned route
		int nextAdjustedRadius = track.getPieces()[(int)distanceToTurn[1]].getAdjustedRadius(this.raceState.getMyLane().getEndLaneIndex(), track.getLanes());
		double nextTurnTargetSpeed = Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(nextAdjustedRadius));
		
		// at corner
		if (trackPieces[raceState.getMyPiecePosition().getPieceIndex()]
				.getAngle() != 0) {
			
			
			
			// next corner
			if (distanceToNextCorner() < distanceNeeded(nextTurnTargetSpeed)) {
				// at corner and arriving to next corner
				if (raceState.getVelocity(myCar) > nextTurnTargetSpeed) {
					THROTTLE = 0;
				} else {
					// at next corner
					THROTTLE = adjustCurrentSpeedToTarget(currentTurnTargetSpeed);
				}
			} else {
				// at previous corner
				THROTTLE = adjustCurrentSpeedToTarget(currentTurnTargetSpeed);
			}

		} else {
			// next corner
			if (distanceToNextCorner() < distanceNeeded(nextTurnTargetSpeed)) {
				// Arriving to corner
				if (raceState.getVelocity(myCar) > nextTurnTargetSpeed) {
					THROTTLE = 0;
				} else {
					// at corner
					THROTTLE = adjustCurrentSpeedToTarget(nextTurnTargetSpeed);
				}
			} else {
				// no need to decelerate yet
				THROTTLE = 1;
			}

		}

		
		int currentPieceIndex = this.raceState.getMyPiecePosition().getPieceIndex();
		if (this.routeCommander.isGoodTimeForTurbo(currentPieceIndex)) {
			this.raceState.setMyTurboActivated();
			System.out.println("USING TURBO. Straight: "+(this.raceState.getMyTrackPiece().getRadius()==0));
			return new Turbo("pow",gameTick);
		}
		
		if (this.routeCommander != null && this.routeCommander.handleRouting()) {
			if (this.routeCommander.getLaneCommand() != 0) {
				this.routeCommander.setWaitingForRoute(false);
				System.out.println("Route switch: "+this.routeCommander.getLaneCommand());
				return new SwitchLane(this.routeCommander.getLaneCommand(), gameTick);
			}
		}
		
		return new Throttle(THROTTLE, gameTick);

	}

	public double adjustCurrentSpeedToTarget(double target) {
		if (raceState.getMyVelocity() > target)
			return 0.0d;
		else
			return 1.0d;
	}

	public double distanceNeeded(double targetVelo) {
		double ticks = ticksNeeded(targetVelo);
		return (raceState.getVelocity(myCar) * ticks - 0.5 * Math.abs(amax)
				* ticks * ticks * Math.pow(Math.E, (-Math.abs(lambda) * ticks)));
	}

	public double ticksNeeded(double targetVelo) {
		if (targetVelo > 0) {
			return (Math.log(raceState.getVelocity(myCar) / Math.abs(targetVelo)) / Math.abs(lambda));
		} else {
			return -1;
		}
	}

	public int getNextCornerIndex() {
		int nextCornerIndex = -1;
		for (int i = raceState.getMyPiecePosition().getPieceIndex() + 1; i < trackPieces.length; i++) {
			if (trackPieces[i].getAngle() != 0) {
				// this piece is a corner (the next corner)
				nextCornerIndex = i;
				break;
			}
		}

		// next corner is behind start line
		if (nextCornerIndex == -1) {
			for (int i = 0; i < trackPieces.length; i++) {
				if (trackPieces[i].getAngle() != 0) {
					// this piece is a corner (the next corner)
					nextCornerIndex = i;
					break;
				}
			}
		}

		return nextCornerIndex;
	}

	public double distanceToNextCorner() {
		// distance from current position to end of the piece
		double distance = raceState.getPieceLength(trackPieces[raceState
				.getMyPiecePosition().getPieceIndex()], raceState
				.getMyPiecePosition().getLane().getEndLaneIndex(), raceState
				.getMyPiecePosition().getLane().getEndLaneIndex())
				- raceState.getPiecePosition(myCar).getInPieceDistance();

		// if corner is behind start line
		if (getNextCornerIndex() < raceState.getMyPiecePosition()
				.getPieceIndex()) {
			for (int i = raceState.getMyPiecePosition().getPieceIndex() + 1; i < trackPieces.length; i++) {
				// add all next track piece lengths before start line
				distance += raceState.getPieceLength(trackPieces[i], raceState
						.getMyPiecePosition().getLane().getEndLaneIndex(),
						raceState.getMyPiecePosition().getLane()
								.getEndLaneIndex());
			}
			for (int i = 0; i < getNextCornerIndex(); i++) {
				distance += raceState.getPieceLength(trackPieces[i], raceState
						.getMyPiecePosition().getLane().getEndLaneIndex(),
						raceState.getMyPiecePosition().getLane()
								.getEndLaneIndex());
			}
		} else {
			// corner is on this lap
			for (int i = raceState.getMyPiecePosition().getPieceIndex() + 1; i < trackPieces.length; i++) {
				if (trackPieces[i].getAngle() != 0) {
					// this piece is a corner (the next corner)
					break;
				} else {
					// this piece isn't a corner
					distance += trackPieces[i].getLength();
				}
			}
		}

		return distance;
	}

	public void calculateCoefficients(double y1, double y2, double x1, double x2) {
		double a = ((Math.log(y1) + Math.log(y2)) * (x1 * x1 + x2 * x2) - (x1 + x2)
				* (x1 * Math.log(y1) + x2 * Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));
		amax = Math.pow(Math.E, a);

		lambda = Math.abs(2 * (x1 * Math.log(y1) + x2 * Math.log(y2))
				- (x1 + x2) * (Math.log(y1) + Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));

	}
	
	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		//this.aiState.updateCurrentState(carPositions, track);
	}	
		
	@Override
	public void onCrash(Car car) {
		if (car.equals(this.raceState.getMyCar())) {
			this.decreaseTargetAngularAcceleration();
			this.crashOnLap = true;
			System.out.println("Current angAcc target: "+this.getCurrentAngularAccelerationTarget());	
		}
	}
	
	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime, Ranking ranking) {
		if (car.equals(this.raceState.getMyCar())) {
			//System.out.println("\n--- Outputing angle mesurements ---");
			//this.angleModel.outputMeasurements();
			//this.angleModel.outputAngles();		
			
			if (this.crashOnLap == false) {
				//this.firstMinimumFound = true;
				this.maxAngularAccelerationFullLap = this.getCurrentAngularAccelerationTarget();
				if (this.strategy == 1)
					increaseTargetAngularAcceleration();	
			}
			
			this.crashOnLap = false;
			System.out.println("Current angAcc target: "+this.getCurrentAngularAccelerationTarget());
		}
	}
	
	private void increaseTargetAngularAcceleration() {
		if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last+difference/2.0d);
		}
	}
	
	private double getCurrentAngularAccelerationTarget() {
		
		if (this.strategy == 0)
			return this.maxAngularAccelerationFullLap;
		
		if (this.targetAngularAccelerations.size() > 0)
			return this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-1);
		
		return Double.MAX_VALUE;
	}
	
	private void decreaseTargetAngularAcceleration() {
		if (this.targetAngularAccelerations.size() == 0) {
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration());
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration()/2.0d);
		}
		else if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last-difference/2.0d);
		}
	}
	
	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
		//Switch to safe tactic for the race
		//TODO: do not increase target angularAcceleration after the final qualification lap
		this.strategy = 0;
		System.out.println("After session adjustments");
	}

}
