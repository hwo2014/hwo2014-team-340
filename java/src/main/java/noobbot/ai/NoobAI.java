package noobbot.ai;

import noobbot.network.out.SendMsg;
import noobbot.network.out.Throttle;

public class NoobAI extends BotAI {

	@Override
	public String getBaseName() {
		return "Noobbot";
	}

	@Override
	public String getDescription() {
		return "This is the default AI!";
	}

	@Override
	public SendMsg onGameTick(int gameTick) {
		return new Throttle(0.5, gameTick);
	}

}
