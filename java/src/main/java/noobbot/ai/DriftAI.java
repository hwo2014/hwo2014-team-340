package noobbot.ai;

import java.util.ArrayList;

import noobbot.engine.AIState;
import noobbot.engine.AngleModel;
import noobbot.engine.RaceState;
import noobbot.engine.RouteCommander;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.out.Turbo;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;

public class DriftAI extends BotAI {

	private AngleModel angleModel = new AngleModel();
	private ArrayList<Double> targetAngularAccelerations = new ArrayList<Double>();
	private boolean crashOnLap = false;
	//private boolean firstMinimumFound = false;
	private double maxAngularAccelerationFullLap = -1.0d;
	
	private double throttle;
	private Track track;
	private AIState aiState;
	private RouteCommander routeCommander;
	private RaceState raceState;
	
	private int strategy = 1; //0=safe (do not increase targetAngularAcceleration), 1=search limit
	
	public DriftAI() {
		this.aiState = new AIState();	
		
		//Test values
		//this.targetAngularAccelerations.add(0.90d);
		//this.targetAngularAccelerations.add(0.43d);
		//this.maxAngularAccelerationFullLap = 0.47;
		//this.strategy = 0;
	}
	
	@Override
	public String getBaseName() {
		return "DriftBot";
	}

	@Override
	public String getDescription() {
		return "This is super drifter AI.";
	}
	
	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
		this.track = track;
		
		this.raceState  = this.getManager().getState();
		
		this.routeCommander = new RouteCommander(this.getManager().getState(), track);
		this.aiState.setAiName(this.getManager().getState().getMyCar().getName());
	}

	@Override
	public SendMsg onGameTick(int gameTick) {
		
		//this.raceState.outputMyMeasurements();
		this.angleModel.addSpeedY(this.raceState.getMyAngle(), this.raceState.getMyVelocity());
		
		if (this.raceState.getMyTrackPiece().getRadius() == 0)
			this.angleModel.getReadyForNextMeasurement();
		
		if (this.raceState.getMyTrackPiece().getRadius() != 0)
		{
			if (this.raceState.getMyAngle() > 0.0d) {
				this.angleModel.addMeasuredAngle(this.raceState.getMyAngle());
				//System.out.println(this.raceState.getMyAngle());
				this.angleModel.addAccelerationIfMissing(this.raceState.getMyAngularAcceleration());
			}			
		}
		
		/*if (state.getMyPiecePosition().getLap()==0)
			return new Throttle(0.55, gameTick);
		else if (state.getMyPiecePosition().getLap()==1)
			return new Throttle(0.6, gameTick);
		else
			return new Throttle(0.65, gameTick);
		
		if (state.getMyPiecePosition().getLap()==0)
			return new Throttle(0.9, gameTick);
		else if (state.getMyPiecePosition().getLap()==1)
			return new Throttle(0.95, gameTick);
		else
			return new Throttle(1.00, gameTick);*/
		
		/*if (state.getMyPiecePosition().getLap()==0)
			return new Throttle(0.4, gameTick);
		else if (state.getMyPiecePosition().getLap()==1)
			return new Throttle(0.5, gameTick);
		else
			return new Throttle(0.6, gameTick);*/
		
		int currentPieceIndex = this.raceState.getMyPiecePosition().getPieceIndex();
		//TODO: How many turbos available & do not use turbo if it is currently activated this.raceState.getMyTurboActivatedTick()
		if (this.raceState.isMyTurboAvailable() && this.routeCommander.isGoodTimeForTurbo(currentPieceIndex)) {
			this.raceState.setMyTurboActivated();
			return new Turbo("pow",gameTick);
		}
		
		if (this.routeCommander != null && this.routeCommander.handleRouting()) {
			if (this.routeCommander.getLaneCommand() != 0) {
				this.routeCommander.setWaitingForRoute(false);
				System.out.println("Route switch: "+this.routeCommander.getLaneCommand());
				return new SwitchLane(this.routeCommander.getLaneCommand(), gameTick);
			}
		}
		
		return new Throttle(this.throttle, gameTick);
		//return new Throttle(0.92d, gameTick);
	}
	
	@Override
	public void onCrash(Car car) {
		if (car.equals(this.raceState.getMyCar())) {
			this.decreaseTargetAngularAcceleration();
			this.crashOnLap = true;
			//System.out.println("Current angAcc target: "+this.getCurrentAngularAccelerationTarget());	
		}
	}
	
	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime, Ranking ranking) {
		if (car.equals(this.raceState.getMyCar())) {
			System.out.println("\n--- Outputing mesurements ---");
			//this.angleModel.outputSpeedYs();
			//this.angleModel.outputMeasurements();
			//this.angleModel.outputAngles();		
			
			if (this.crashOnLap == false) {
				//this.firstMinimumFound = true;
				this.maxAngularAccelerationFullLap = this.getCurrentAngularAccelerationTarget();
				if (this.strategy == 1)
					increaseTargetAngularAcceleration();	
			}
			
			this.crashOnLap = false;
			System.out.println("Current angAcc target: "+this.getCurrentAngularAccelerationTarget());
		}
	}
	
	private void increaseTargetAngularAcceleration() {
		if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last+difference/2.0d);
		}
	}
	
	private double getCurrentAngularAccelerationTarget() {
		
		if (this.strategy == 0)
			return this.maxAngularAccelerationFullLap;
		
		if (this.targetAngularAccelerations.size() > 0)
			return this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-1);
		
		return Double.MAX_VALUE;
	}
	
	private void decreaseTargetAngularAcceleration() {
		if (this.targetAngularAccelerations.size() == 0) {
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration());
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration()/2.0d);
		}
		else if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last-difference/2.0d);
			
			//if (this.firstMinimumFound)
			//	this.targetAngularAccelerations.add(last-difference/2.0d);
			//else
			//this.targetAngularAccelerations.add(last-difference);
		}
	}
	
	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		this.throttle = processPositions(carPositions);
	}
	
	private double processPositions(CarPosition[] positions) {
		
		this.aiState.updateCurrentState(positions, track);
		
		//TODO: find own car and not use the first item
		double throttlePosition = 1.0d;
		
		int currentPieceIndex = this.raceState.getMyPiecePosition().getPieceIndex();
		
		double targetSpeed = -1.0d;
		//use the curve beginning to adjust speed, when we are drifting use the the exit angle
		//Seems to be faster without the angle check
		if (this.raceState.getMyTrackPiece().getRadius() != 0 /*&& Math.abs(this.aiState.getCurrentAngle()) < 0.01d*/) {	
			targetSpeed = Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(this.aiState.getCorneringRadius()));
			
			//if (this.raceState.getMyVelocity() > 1.0d && !this.raceState.isMyCarCrashed())
				//System.out.println("angAcc: "+this.aiState.getAngularAcceleration()+" Target speed: "+targetSpeed);
		}
		else
			System.out.print(" STRAIGHT ");
		
		//if we are above target speed brake
		if ((targetSpeed > 0.0d && this.raceState.getMyVelocity() > targetSpeed)) {
			throttlePosition = 0.0f;
			//System.out.print(" TOO FAST FOR CURRENT ");
			//System.out.println("ADJUST Target/Current speed:" + targetSpeed+"/"+this.raceState.getMyVelocity() + "Throttle:" + throttlePosition);
		}
				
		if (this.aiState.getDeceleration() < 0.0d) {
			double distanceToTurn[] = track.getDistanceToNextTurn(currentPieceIndex, this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex());	
			//TODO: take account the planned route
			int nextAdjustedRadius = track.getPieces()[(int)distanceToTurn[1]].getAdjustedRadius(this.raceState.getMyLane().getEndLaneIndex(), track.getLanes());
			
			double nextTurnTargetSpeed = Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(nextAdjustedRadius));
			
			//t= (v-v0)/a
			//x = v0*t+0.5*a*t^2
			if (nextTurnTargetSpeed < this.raceState.getMyVelocity()) {
				double time = (nextTurnTargetSpeed - this.raceState.getMyVelocity()) / this.aiState.getDeceleration();
				double endPosition = this.raceState.getMyVelocity()*time+0.5*this.aiState.getDeceleration()*time*time;
				
				if (endPosition > distanceToTurn[0]) {
					throttlePosition = 0.0f;
					//System.out.print(" TOO FAST FOR NEXT ");
				}
			}	
		
		}
		
						
		//At the moment speed is unit is distance/(one)tick, calculate the angle increase rate
		/*if (this.raceState.getMyVelocity() > 0.0f && this.raceState.getMyTrackPiece().getRadius() != 0) {
			double angleDelta = ( this.aiState.getCurrentAngle() - this.aiState.getPreviousAngle())/this.raceState.getMyVelocity(); 
			
			double absoluteExitAngle = Math.abs(this.aiState.getCurrentAngle()+angleDelta*track.getDistanceToNextStraight(currentPieceIndex, this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex()));
			System.out.println("Ang: "+ this.aiState.getCurrentAngle()+" ExitAng: "+absoluteExitAngle+" AngDelta: "+angleDelta);
			
			if ( absoluteExitAngle >= Math.abs(this.aiState.getMaxAngle())) {
				throttlePosition = 0.0f;
				System.out.println("distanceToStraight: "+ track.getDistanceToNextStraight(currentPieceIndex, this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex()));
			}	
		}*/
		
		if (this.aiState.checkBrakesIfNeeded()) {
			throttlePosition = 0.0f;
		}
		
		return throttlePosition;
	}
	
	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
		//Switch to safe tactic for the race
		//TODO: do not increase target angularAcceleration after the final qualification lap
		this.strategy = 0;
		System.out.println("After session adjustments");
	}

}
