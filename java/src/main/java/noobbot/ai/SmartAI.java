package noobbot.ai;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import noobbot.engine.RaceState;
import noobbot.engine.RouteFinder;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.pojo.TrackPiece;



public class SmartAI extends BotAI {

	private double lastThrottle;
	
	// (velocity, acceleration/throttle)
	private Map<Double, Double> powerMap =
			new TreeMap<Double, Double>();
	
	@Override
	public SendMsg onGameTick(int gameTick) {
		
		RaceState state = getManager().getState();
		
		// Custom calculation, to be moved to RaceState
		
		if(lastThrottle > 0) {
			double power = state.getMyAcceleration() / lastThrottle;
			if(power > 0) {
				powerMap.put(state.getMyVelocity(), power);
			}
		}
		
		// First check if we need to switch lanes
		
		int laneSwitch = determineLaneSwitch();		
		if(laneSwitch != 0) {
			System.err.println("Needed to switch lane: " + laneSwitch);
			return new SwitchLane(laneSwitch, gameTick);
		}
		
		System.err.println("Did not need to switch lane");
		
		// Data gathering
		if(state.getMyKnownMaxAngularAcceleration() == 0) {
			System.err.println("Gathering data, throttle 1.0f");
			return new Throttle(1.0f, gameTick);
		}
		
		// Determine optimal throttle
		
		double throttle = determineThrottle();
		
		System.err.println("Determined throttle " + throttle);
		
		lastThrottle = throttle;
		
		return new Throttle(throttle, gameTick);
	}

	@Override
	public String getBaseName() {
		return "MiscTestAI";
	}

	@Override
	public String getDescription() {
		return "Bot for trying stuff out.";
	}
	
	private int determineLaneSwitch() {
		RaceState state = getManager().getState();
		RouteFinder routeFinder = getManager().getRouteFinder();
		
		// Cannot switch if track piece doesn't contain switch piece
		if(!state.isMyCarOnSwitch()) {
			return 0;
		}
		
		// Let routefinder determine whether to switch
		return routeFinder.calculateNextLane(
				state.getMyPiecePosition().getPieceIndex(), 
				state.getMyPiecePosition().getLane().getEndLaneIndex(), 0);

	}
	
	private double determineThrottle() {
		RaceState state = getManager().getState();
		
		System.err.println("Determining throttle");
		
		if(state.isMyCarInTurn()) {
			return determineTurnThrottle();
		} else {
			return determineStraightThrottle();
		}
		
	}
	
	private double determineTurnThrottle() {
		RaceState state = getManager().getState();
		
		System.err.println("Determining throttle in turn");
		
		// TODO
		return 0.4f;
	}
	
	private double determineStraightThrottle() {
		RaceState state = getManager().getState();

		System.err.println("Determining throttle on straight");
		
		int nextTurnPieceId = state.getMyNextTurnPiece();
		TrackPiece nextTurnPiece = state.getTrack().getPieces()[nextTurnPieceId];
		
		System.err.println("Next turn piece is " + nextTurnPieceId);
		
		double nextTurnRadius;
		nextTurnRadius = nextTurnPiece.getAdjustedRadius(
				state.getMyLane().getEndLaneIndex(), state.getTrack().getLanes());
		
		System.err.println("Next turn piece has a radius of " + nextTurnRadius);
		
		double nextTurnMaxVelocity = Math.sqrt(
				nextTurnRadius * state.getMyKnownMaxAngularAcceleration());
		
		System.err.println("Max known angular acceleration is " + state.getMyKnownMaxAngularAcceleration());
		
		System.err.println("Maximum velocity for next turn is " + nextTurnMaxVelocity);
		
		double velocityDiff = nextTurnMaxVelocity - state.getMyVelocity();
		
		System.err.println("Velocity should change by " + velocityDiff);
		
		double distanceToTurn = state.getDistance(state.getMyPiecePosition(), 
				nextTurnPieceId);
		
		System.err.println("Distance to turn is " + distanceToTurn);
		
		double optimalAcceleration = velocityDiff / 
				(distanceToTurn / state.getMyVelocity());
		
		System.err.println("Need to accelerate by " + optimalAcceleration);
		
		if(optimalAcceleration < 0) {
			System.err.println("Needed to decelerate, throttling 0.0");
			return 0.0f;
		}
		
		System.err.println("Current velocity is " + state.getMyVelocity());
		
		double throttle = getThrottleForAcceleration(
				state.getMyVelocity(), optimalAcceleration);
		
		System.err.println("Needed throttle is " + throttle);
		
		return throttle;
		
	}
	
	private double getThrottleForAcceleration(double velocity, double acceleration) {
		System.err.println("Determining throttle for acceleration " + 
				acceleration + " at velocity " + velocity);
		double optimalThrottle = 0.5f;
		for(Entry<Double, Double> powerEntry : powerMap.entrySet()) {
			double vel = powerEntry.getKey();
			double pow = powerEntry.getValue();
			if(vel > velocity) {
				System.err.println("Passed wanted value, returning " + optimalThrottle);
				return optimalThrottle;
			}
			optimalThrottle = acceleration / pow;
			optimalThrottle = Math.min(optimalThrottle, 1.0d);
		}
		System.err.println("No higher entries found, returning " + optimalThrottle);
		return optimalThrottle;
	}

}
