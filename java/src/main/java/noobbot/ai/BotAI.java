package noobbot.ai;

import java.util.UUID;

import noobbot.engine.BotManager;
import noobbot.network.ConnectionListener;
import noobbot.network.MsgWrapper;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TurboAvailable;

public abstract class BotAI implements ConnectionListener {

	// TODO Implement placeholder manager in case manager does not declare itself
	private BotManager manager;
	
	private String nameSuffix = "";
	
	public void setManager(BotManager manager) {
		this.manager = manager;
		nameSuffix = UUID.randomUUID().toString().substring(0, 5);
	}

	public final String getName() {
		return getBaseName() + "-" + nameSuffix;
	}
	public abstract String getDescription();
	protected abstract String getBaseName();
	
	public BotManager getManager() {
		return manager;
	}
	
	public void onRawMessage(MsgWrapper rawMsg) {}
	public void onYourCar(Car car) {}
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {}
	public void onGameStart() {}
	public void onCarPositions(CarPosition[] carPositions) {}
	public void onCrash(Car car) {}
	public void onSpawn(Car car) {}
	public void onTurboAvailable(TurboAvailable turbo) {}
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime, Ranking ranking) {}
	public void onDnf(Car car, String reason) {}
	public void onFinish(Car car) {}
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {}
	public void onTournamentEnd() {}
	
}
