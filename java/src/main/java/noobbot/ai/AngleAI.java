package noobbot.ai;

import java.util.UUID;

import noobbot.engine.RaceState;
import noobbot.network.out.SendMsg;
import noobbot.network.out.Throttle;

public class AngleAI extends BotAI {
	
	private double previousAngle;
	
	private double newThrottle = 0.888d;

	public AngleAI() {
		previousAngle = 0.0f;
	}
	
	@Override
	public String getBaseName() {
		return "AngleBot";
	}

	@Override
	public String getDescription() {
		return "This is Tommi's angle AI.";
	}
	@Override
	public SendMsg onGameTick(int gameTick) {
		
		RaceState state = getManager().getState();

		// Determine optimal throttle
		double angle = state.getMyAngle();
		newThrottle = processAngle(angle);

		return new Throttle(newThrottle, gameTick);

	}
	
	private double processAngle(double angle) {
		if(Math.abs(angle) > 
				Math.abs(previousAngle) + 0.01) {
			previousAngle = angle;
			return 0.4d;
		} else {
			previousAngle = angle;
			return 0.888d;
		}
	}

}
