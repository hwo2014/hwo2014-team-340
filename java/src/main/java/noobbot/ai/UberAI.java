package noobbot.ai;


import java.util.ArrayList;

import noobbot.engine.AIState;
import noobbot.engine.AngleModel;
import noobbot.engine.RaceState;
import noobbot.engine.RouteCommander;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.out.Turbo;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TrackPiece;

public class UberAI extends BotAI {

	// (temporary) maximum velocities in corners
	//ArrayList<Double> maxVeloInCorners;

	// recorded max velocity for conversion of velocity to throttle
	private double vmax = -1;
	private AngleModel angleModel = new AngleModel();

	// calculation of distance needed to decelerate
	private boolean ekatArvotOnJo = false;
	private int moneskoTick = -1;
	private double y1 = -1;
	private double y2 = -1;
	private double x1 = -1;
	private double x2 = -1;
	private double amax = -1;
	private double lambda = -1;

	private double THROTTLE = 1;
	TrackPiece[] trackPieces;
	RaceState raceState;
	Car myCar;
	
	private ArrayList<Double> targetAngularAccelerations = new ArrayList<Double>();
	private boolean crashOnLap = false;
	private double maxAngularAccelerationFullLap = -1.0d;
	private Track track;
	//private AIState aiState;
	private RouteCommander routeCommander;
	
	//private int strategy = 1; //0=safe (do not increase targetAngularAcceleration), 1=search limit
	private int curveCount = 0;

	public UberAI() {
		//this.aiState = new AIState();	
	}
	
	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
		this.track = track;
		//track.outputTrack();
		
		this.raceState  = this.getManager().getState();
		
		this.routeCommander = new RouteCommander(this.getManager().getState(), track);
		//this.aiState.setAiName(this.getManager().getState().getMyCar().getName());
	}
	
	@Override
	protected String getBaseName() {
		return "UBER";
	}

	@Override
	public String getDescription() {
		return "The most advanced AI.";
	}

	@Override
	public SendMsg onGameTick(int gameTick) {
		
		int startLane = raceState.getMyPiecePosition().getLane().getStartLaneIndex();
		int endLane = raceState.getMyPiecePosition().getLane().getEndLaneIndex();
		
		if (!this.raceState.isMyCarCrashed() && startLane == endLane && Math.abs(this.raceState.getMyAcceleration()) < 1.0d) {
			double signedAngularAcceleration = this.raceState.getMyAngularAcceleration();
			signedAngularAcceleration *= Math.signum(raceState.getMyTrackPiece().getAngle());
			this.angleModel.measureVariablesForModel(gameTick, signedAngularAcceleration, this.raceState.getMyAngle(), 
					this.raceState.getMySlipAngleAcceleration(), this.raceState.getMySlipAngleVelocity(),
					this.raceState.getMyTrackPiece().getRadius(),
					this.raceState.getMyTrackPiece().hasLaneSwitch());
			
			//if (this.raceState.getMyKnownMaxSlipAngleAcceleration() > AngleModel.epsilon)
			this.angleModel.calculateCoefficients();
			
			//this.angleModel.storeMyHighestKnownSlipAngleAcceleration(this.raceState.getMySlipAngleAcceleration());
			//some times getHighestKnownMaxAngularAcceleration mix return weird values i.e. 10 so I'll use own measurements
			//angleModel.storeMaxAngleAndAcceleration(this.raceState.getHighestKnownMaxAngle(), this.raceState.getMyAngularAcceleration());
		}
		this.raceState.outputMyMeasurements();
		// (temp) maximum velocities in corners
		/*maxVeloInCorners = new ArrayList<Double>();
		for (int i = 0; i < 50; i++) {
			maxVeloInCorners.add((double) 4);
		}*/

		myCar = raceState.getMyCar();
		trackPieces = raceState.getTrack().getPieces();

		// calulation of amax and lambda values
		if (raceState.getAcceleration(myCar) != 0 && !ekatArvotOnJo) {
			y1 = raceState.getAcceleration(myCar);
			x1 = gameTick - 1;

			moneskoTick = gameTick;
			ekatArvotOnJo = true;
		}

		// calulation of amax and lambda values
		if (moneskoTick == gameTick - 1 && ekatArvotOnJo) {
			y2 = raceState.getAcceleration(myCar);
			x2 = gameTick - 1;

			// calculate amax ja lambda for deceleration
			calculateCoefficients(y1, y2, x1, x2);
			// OBS! amax and lambda values are operable after tick 3
		}

		// save highest velocity
		if (raceState.getVelocity(myCar) > vmax) {
			vmax = raceState.getVelocity(myCar);
			System.out.print(" vmax: "+vmax);
		}
		
		//System.out.print("AngularAcc: "+raceState.getMyAngularAcceleration()+" angle: "+raceState.getMyAngle()+"\n");

		//double currentTurnTargetSpeed = Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(this.aiState.getCorneringRadius()));
		double distanceToStraight = track.getDistanceToNextStraight(raceState.getMyPiecePosition().getPieceIndex(), raceState.getMyPiecePosition().getInPieceDistance(), raceState.getMyLane().getEndLaneIndex());
		double adjustedRadius = this.raceState.getMyTrackPiece().getAdjustedRadius(this.raceState.getMyLane().getEndLaneIndex(), track.getLanes());
		double currentTurnTargetSpeed = this.getTargetCorneringSpeed(true, distanceToStraight, adjustedRadius, this.raceState.getMyTrackPiece().getAngle());
		
		double distanceToTurn[] = track.getDistanceToNextTurn(raceState.getMyPiecePosition().getPieceIndex(), this.raceState.getMyPiecePosition().getInPieceDistance(), this.raceState.getMyLane().getEndLaneIndex());	
		double distanceToNextTurn = distanceToTurn[0]; 
		//TODO: take account the planned route
		
		TrackPiece futurePiece = track.getPieces()[(int)distanceToTurn[1]];
		boolean startCalculationsFromCurrentState = false;
		//if (track.getNextPiece(raceState.getMyPiecePosition().getPieceIndex()) == futurePiece)
		int currentPieceIndex = raceState.getMyPiecePosition().getPieceIndex();
		int nextPieceIndex = track.getNextPieceIndex(currentPieceIndex);
		int secondNextPieceIndex = track.getNextPieceIndex(nextPieceIndex);
		if (nextPieceIndex == (int)distanceToTurn[1] || secondNextPieceIndex == (int)distanceToTurn[1])
			startCalculationsFromCurrentState = true;
		
		int nextAdjustedRadius = futurePiece.getAdjustedRadius(this.raceState.getMyLane().getEndLaneIndex(), track.getLanes());
		double nextTurnDistanceToStraight = track.getDistanceToNextStraight((int)distanceToTurn[1], 0.0d, raceState.getMyLane().getEndLaneIndex());
		double nextTurnTargetSpeed = this.getTargetCorneringSpeed(startCalculationsFromCurrentState, nextTurnDistanceToStraight, nextAdjustedRadius, futurePiece.getAngle());
		
		//double nextTurnTargetSpeed = Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(nextAdjustedRadius));
		//currentTurnTargetSpeed = Math.max(currentTurnTargetSpeed, 0.01d);
		//nextTurnTargetSpeed = Math.max(nextTurnTargetSpeed, 0.01d);
		
		if (!this.raceState.isMyCarCrashed())
			System.out.println("Current: "+raceState.getMyAngularAcceleration()+" target: "+currentTurnTargetSpeed*currentTurnTargetSpeed/adjustedRadius+" Future target: "+nextTurnTargetSpeed*nextTurnTargetSpeed/nextAdjustedRadius);
			//System.out.println("Current: "+raceState.getMyVelocity()+" target: "+currentTurnTargetSpeed+" Future target: "+nextTurnTargetSpeed);
		
		// at corner
		if (trackPieces[raceState.getMyPiecePosition().getPieceIndex()]
				.getAngle() != 0) {
			
			
			
			// next corner
			if (distanceToNextTurn < distanceNeeded(nextTurnTargetSpeed)) {
				// at corner and arriving to next corner
				if (raceState.getVelocity(myCar) > nextTurnTargetSpeed) {
					THROTTLE = 0;
					outputCurrentAndTarget(raceState.getMyVelocity(), nextTurnTargetSpeed);
				} else {
					// at next corner
					THROTTLE = adjustCurrentSpeedToTarget(currentTurnTargetSpeed);
					outputCurrentAndTarget(raceState.getMyVelocity(), currentTurnTargetSpeed);
				}
			} else {
				// at previous corner
				/*if (raceState.getPiecePosition(myCar).getInPieceDistance() > raceState
						.getPieceLength(trackPieces[raceState
								.getMyPiecePosition().getPieceIndex()],
								raceState.getMyPiecePosition().getLane()
										.getEndLaneIndex(), raceState
										.getMyPiecePosition().getLane()
										.getEndLaneIndex()) * 0.5) {
					THROTTLE = 1;
				} else {
					THROTTLE = convertVelocityToThrottle(currentTurnTargetSpeed);
				}*/
				THROTTLE = adjustCurrentSpeedToTarget(currentTurnTargetSpeed);
				outputCurrentAndTarget(raceState.getMyVelocity(), currentTurnTargetSpeed);
			}

		} else {
			// next corner
			if (distanceToNextTurn < distanceNeeded(nextTurnTargetSpeed)) {
				// Arriving to corner
				if (raceState.getVelocity(myCar) > nextTurnTargetSpeed) {
					THROTTLE = 0;
					outputCurrentAndTarget(raceState.getMyVelocity(), nextTurnTargetSpeed);
				} else {
					// at corner
					//THROTTLE = convertVelocityToThrottle(currentTurnTargetSpeed);
					THROTTLE = adjustCurrentSpeedToTarget(nextTurnTargetSpeed);
					outputCurrentAndTarget(raceState.getMyVelocity(), nextTurnTargetSpeed);
				}
			} else {
				// no need to decelerate yet
				THROTTLE = 1;
			}

		}
		
		//Find the limit: remember the game tick is reseted for race
		if (gameTick <= 3)
			THROTTLE = 1.0;
		else if (angleModel.getMyHighestKnownSlipAngleAcceleration() < AngleModel.epsilon) {
			if (raceState.getMyTrackPiece().getRadius() == 0)
				THROTTLE = Math.min(1.0d, 0.5d+curveCount*0.1d);
			else {
				if (raceState.getMySlipAngleAcceleration() > AngleModel.epsilon)
					THROTTLE = 0.0;
				else
					THROTTLE = 1.0;
			}
		}
		
		if(raceState.getMyTrackPiece().getRadius() != 0 
				&& track.getNextPiece(raceState.getMyPiecePosition().getPieceIndex()).getRadius()==0)
				this.curveCount++;
		
		//int currentPieceIndex = this.raceState.getMyPiecePosition().getPieceIndex();
		//TODO: How many turbos available & do not use turbo if it is currently activated this.raceState.getMyTurboActivatedTick()
		if (this.routeCommander.isGoodTimeForTurbo(currentPieceIndex)) {
			this.raceState.setMyTurboActivated();
			return new Turbo("pow",gameTick);
		}
		
		if (this.routeCommander != null && this.routeCommander.handleRouting()) {
			if (this.routeCommander.getLaneCommand() != 0) {
				this.routeCommander.setWaitingForRoute(false);
				System.out.println("Route switch: "+this.routeCommander.getLaneCommand());
				return new SwitchLane(this.routeCommander.getLaneCommand(), gameTick);
			}
		}
		
		return new Throttle(THROTTLE, gameTick);

	}
	
	public void outputCurrentAndTarget(double currentSpeed, double targetSpeed) {
		//System.out.println("current: "+currentSpeed+" targeting: "+targetSpeed);
	}
	
	public double adjustCurrentSpeedToTarget(double target) {
		if (Double.isInfinite(target) || Double.isNaN(target))
			return 1.0d;
		
		if (target < AngleModel.epsilon)
			return 0.0d;
		
		if (raceState.getMyVelocity() > target)
			return 0.0d;
		else {
			double ticks = Math.abs(ticksNeeded(target));
			double plannedThrottle = Math.min(1.0d, ticks);
			if (Double.isInfinite(plannedThrottle) || Double.isNaN(plannedThrottle)) {
				System.out.print(" CALCULATION PROBLEM!!!!!!!!!!!");
				plannedThrottle = 1.0d;
			}
			System.out.print(" THROTTLE "+plannedThrottle+" ");
			return plannedThrottle;
		}
			//return 1.0d;
			//return convertVelocityToThrottle(target);
	}

	public double convertVelocityToThrottle(double velo) {
		if (vmax < 0) {
			double throttle = velo / vmax;
			if (throttle > 1) {
				return 1;
			} else if (throttle < 0) {
				return 0;
			} else {
				return throttle;
			}
		} else {
			// no vmax value yet
			return 1;
		}
	}

	public double distanceNeeded(double targetVelo) {
		double ticks = ticksNeeded(targetVelo);
		return (raceState.getVelocity(myCar) * ticks - 0.5 * Math.abs(amax)
				* ticks * ticks * Math.pow(Math.E, (-Math.abs(lambda) * ticks)));
	}

	public double ticksNeeded(double targetVelo) {
		if (targetVelo > 0) {
			return (Math.log(raceState.getVelocity(myCar) / Math.abs(targetVelo)) / Math.abs(lambda));
		} else {
			return -1;
		}
	}

	public int getNextCornerIndex() {
		int nextCornerIndex = -1;
		for (int i = raceState.getMyPiecePosition().getPieceIndex() + 1; i < trackPieces.length; i++) {
			if (trackPieces[i].getAngle() != 0) {
				// this piece is a corner (the next corner)
				nextCornerIndex = i;
				break;
			}
		}

		// next corner is behind start line
		if (nextCornerIndex == -1) {
			for (int i = 0; i < trackPieces.length; i++) {
				if (trackPieces[i].getAngle() != 0) {
					// this piece is a corner (the next corner)
					nextCornerIndex = i;
					break;
				}
			}
		}

		return nextCornerIndex;
	}

	public double distanceToNextCorner() {
		// distance from current position to end of the piece
		double distance = raceState.getPieceLength(trackPieces[raceState
				.getMyPiecePosition().getPieceIndex()], raceState
				.getMyPiecePosition().getLane().getEndLaneIndex(), raceState
				.getMyPiecePosition().getLane().getEndLaneIndex())
				- raceState.getPiecePosition(myCar).getInPieceDistance();

		// if corner is behind start line
		if (getNextCornerIndex() < raceState.getMyPiecePosition()
				.getPieceIndex()) {
			for (int i = raceState.getMyPiecePosition().getPieceIndex() + 1; i < trackPieces.length; i++) {
				// add all next track piece lengths before start line
				distance += raceState.getPieceLength(trackPieces[i], raceState
						.getMyPiecePosition().getLane().getEndLaneIndex(),
						raceState.getMyPiecePosition().getLane()
								.getEndLaneIndex());
			}
			for (int i = 0; i < getNextCornerIndex(); i++) {
				distance += raceState.getPieceLength(trackPieces[i], raceState
						.getMyPiecePosition().getLane().getEndLaneIndex(),
						raceState.getMyPiecePosition().getLane()
								.getEndLaneIndex());
			}
		} else {
			// corner is on this lap
			for (int i = raceState.getMyPiecePosition().getPieceIndex() + 1; i < trackPieces.length; i++) {
				if (trackPieces[i].getAngle() != 0) {
					// this piece is a corner (the next corner)
					break;
				} else {
					// this piece isn't a corner
					distance += trackPieces[i].getLength();
				}
			}
		}

		return distance;
	}

	public void calculateCoefficients(double y1, double y2, double x1, double x2) {
		double a = ((Math.log(y1) + Math.log(y2)) * (x1 * x1 + x2 * x2) - (x1 + x2)
				* (x1 * Math.log(y1) + x2 * Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));
		amax = Math.pow(Math.E, a);

		lambda = Math.abs(2 * (x1 * Math.log(y1) + x2 * Math.log(y2))
				- (x1 + x2) * (Math.log(y1) + Math.log(y2)))
				/ (2 * (x1 * x1 + x2 * x2) - (x1 + x2) * (x1 + x2));

	}
	
	@Override
	public void onCarPositions(CarPosition[] carPositions) {
		//this.aiState.updateCurrentState(carPositions, track);
	}	
		
	@Override
	public void onCrash(Car car) {
		if (car.equals(this.raceState.getMyCar())) {
			if (!this.angleModel.modelReady() || ! angleModel.isMinAngularLimitFound())
				this.decreaseTargetAngularAcceleration();
			this.crashOnLap = true;
			this.angleModel.outputCoefficients();
			System.out.println("CRASH! Current angAcc target: "+this.getCurrentAngularAccelerationTarget());
			this.angleModel.resetALimitMeasurement();
		}
	}
	
	@Override
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime, Ranking ranking) {
		if (car.equals(this.raceState.getMyCar())) {
			//System.out.println("\n--- Outputing angle mesurements ---");
			//this.angleModel.outputMeasurements();
			//this.angleModel.outputAngles();		
			
			if (this.crashOnLap == false) {
				//this.firstMinimumFound = true;
				this.maxAngularAccelerationFullLap = this.getCurrentAngularAccelerationTarget();
				if (!this.angleModel.modelReady() || ! angleModel.isMinAngularLimitFound())
					increaseTargetAngularAcceleration();	
			}
			
			this.crashOnLap = false;
			this.angleModel.outputCoefficients();
			System.out.println("Current angAcc target: "+this.getCurrentAngularAccelerationTarget());
		}
	}
	
	private void increaseTargetAngularAcceleration() {
		if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last+difference/2.0d);
		}
	}
	
	private double getCurrentAngularAccelerationTarget() {
		
		//if (this.strategy == 0)
		//	return this.maxAngularAccelerationFullLap;
		
		if (this.targetAngularAccelerations.size() > 0)
			return this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-1);
		
		return Double.MAX_VALUE;
	}
	
	private void decreaseTargetAngularAcceleration() {
		if (this.targetAngularAccelerations.size() == 0) {
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration());
			this.targetAngularAccelerations.add(this.raceState.getMyKnownMaxAngularAcceleration()/2.0d);
		}
		else if(this.targetAngularAccelerations.size() >= 2) {
			double secondLast = this.targetAngularAccelerations.get(this.targetAngularAccelerations.size()-2); 
			double last = this.getCurrentAngularAccelerationTarget();
			double difference = Math.abs(last-secondLast);
			this.targetAngularAccelerations.add(last-difference/2.0d);
		}
	}
	
	@Override
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps) {
		//Switch to safe tactic for the race
		//TODO: do not increase target angularAcceleration after the final qualification lap
		//this.strategy = 0;
		angleModel.resetALimitMeasurement();
		System.out.println("After session adjustments");
		System.out.println("TOTAL CRASHES: "+this.raceState.getNumberOfAllCrashes());
	}
	
	public double getTargetCorneringSpeed(boolean startFromCurrent,double distanceToStraight, double adjustedRadius, double turnAngle) {
		
		//if (this.raceState.isMyCarCrashed())
		//	return -1.0;
		
		//boolean crashedOnce = this.targetAngularAccelerations.size() != 0;
		if (this.angleModel.modelReady() && angleModel.isMinAngularLimitFound()) {
			
			double angle = 0.0d;
			double angleVelocity = 0.0d;
			if (startFromCurrent) {
				angle = this.raceState.getMyAngle();
				angleVelocity = this.raceState.getMySlipAngleVelocity();
			}

			double targetAngle = angleModel.getTargetAngle(this.raceState.getNumberOfAllCrashes() > 0);
			
			double angularAcceleration = this.angleModel.getTurnTargetAcceleration(distanceToStraight, adjustedRadius, turnAngle, 
					angleModel.getHighestKnownAngularAcceleration(), targetAngle,
					angle, angleVelocity);
			
			//Debug.log
			
			return Math.sqrt(Math.abs(angularAcceleration)*Math.abs(adjustedRadius));
		}
		else {
			
			//return Math.sqrt(0.5d*Math.abs(adjustedRadius));
			return Math.sqrt(this.getCurrentAngularAccelerationTarget()*Math.abs(adjustedRadius));
		}
		
	}

}
