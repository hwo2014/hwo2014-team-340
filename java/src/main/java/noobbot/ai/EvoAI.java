package noobbot.ai;

import noobbot.engine.AngleModel;
import noobbot.engine.RaceState;
import noobbot.engine.RouteCommander;
import noobbot.network.out.SendMsg;
import noobbot.network.out.SwitchLane;
import noobbot.network.out.Throttle;
import noobbot.network.out.Turbo;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.PiecePosition;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.Track;

public class EvoAI extends BotAI {
	
	private RouteCommander routeCommander;
	private RaceState raceState;
	private AngleModel angleModel  = new AngleModel();
	private Track track;
	private double previousVelocity = 0.0d;
	private double previousThrottle = 0.0d;
	private int previousPieceIndex = 0;
	private double previousTurboFactor = 0.0d;
	private PiecePosition previousPosition = null;
	
	private int curveCount = 0;

	@Override
	public String getBaseName() {
		return "EvoBot";
	}

	@Override
	public String getDescription() {
		return "The most advanced AI.";
	}
	
	@Override
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session) {
		this.raceState  = this.getManager().getState();
		this.routeCommander = new RouteCommander(this.getManager().getState(), track);
		track.outputTrack();
		this.track = track;
	}

	double myThrottle = 1.0;
	@Override
	public SendMsg onGameTick(int gameTick) {		

		double accurateVelocity = raceState.getMyVelocity();
		/*System.out.println("Estimated vs measured velocity: "
				+(angleModel.getNextStepVelocity(previousVelocity, previousThrottle, previousTurboFactor)
				-raceState.getMyVelocity()));*/
		double simulatedVelocity = angleModel.getNextStepVelocity(previousVelocity, previousThrottle, previousTurboFactor);
		System.out.println("Measured vs simulated velocity: "+Math.abs(accurateVelocity-simulatedVelocity));
		/*if (angleModel.accelerationModelReady() && Math.abs(accurateVelocity-simulatedVelocity)>1.0) {
			System.out.println("!!! USING SIMULATED VELOCITY: "+simulatedVelocity);
			accurateVelocity = simulatedVelocity;
		}*/
		int startLane = raceState.getMyPiecePosition().getLane().getStartLaneIndex();
		int endLane = raceState.getMyPiecePosition().getLane().getEndLaneIndex();
		
		if (!this.raceState.isMyCarCrashed() && startLane == endLane) {
			double signedAngularAcceleration = track.getAngularAcceleration(accurateVelocity, 
						raceState.getMyPiecePosition().getPieceIndex(), 
						startLane, 
						endLane);
			
			this.angleModel.measureVariablesForModel(gameTick, signedAngularAcceleration, this.raceState.getMyAngle(), 
					this.raceState.getMySlipAngleAcceleration(), this.raceState.getMySlipAngleVelocity(),
					this.raceState.getMyTrackPiece().getRadius(),
					this.raceState.getMyTrackPiece().hasLaneSwitch());
			
			this.angleModel.calculateCoefficients();
		}
		this.raceState.outputMyMeasurements();
		
		
		if (!angleModel.accelerationModelReady()) {
			if (gameTick > 1 &&
					previousPieceIndex == raceState.getMyPiecePosition().getPieceIndex())
				angleModel.addAccelerationMeasurements(previousVelocity, raceState.getMyVelocity());
			
			if (previousPieceIndex != raceState.getMyPiecePosition().getPieceIndex()){
				angleModel.calculateAccelerationCoefficients();
				
			}
		}
		else {
			
			//TODO: implement sanity checks for velo by using own calculations
			double targetAngle = angleModel.getTargetAngle(this.raceState.getNumberOfAllCrashes() > 0)- 4.0d;//; -6.0d;
			
			if (angleModel.canThrottle(raceState.getMyPiecePosition(), previousPosition, track, targetAngle, 
					raceState.getMyAngle(), raceState.getMySlipAngleVelocity(), accurateVelocity, 0.1, 
					routeCommander, raceState.getMyCurrentTurboFactor())) {
				myThrottle = 1.0;
			}
			else {
				myThrottle = 0.0;
			}
		}
		
		if (angleModel.accelerationModelReady() && angleModel.getMyHighestKnownSlipAngleAcceleration() < AngleModel.epsilon) {
			if (raceState.getMyTrackPiece().getRadius() == 0)
				myThrottle = Math.min(1.0d, 0.5d+curveCount*0.1d);
			else {
				if (raceState.getMySlipAngleAcceleration() > AngleModel.epsilon)
					myThrottle = 0.0;
				else
					myThrottle = 1.0;
			}
		}
		
		if(raceState.getMyTrackPiece().getRadius() != 0 
				&& track.getNextPiece(raceState.getMyPiecePosition().getPieceIndex()).getRadius()==0)
				this.curveCount++;
		
		previousPosition = raceState.getMyPiecePosition();
			
		previousVelocity = accurateVelocity;
		previousPieceIndex = raceState.getMyPiecePosition().getPieceIndex();
		previousTurboFactor = raceState.getMyCurrentTurboFactor();
		
		//TODO: use turbo factor for calculations
		int currentPieceIndex = raceState.getMyPiecePosition().getPieceIndex();
		if (this.routeCommander.isGoodTimeForTurbo(currentPieceIndex)) {
			this.raceState.setMyTurboActivated();
			return new Turbo("pow",gameTick);
		}
		
		if (this.routeCommander != null && this.routeCommander.handleRouting()) {
			if (this.routeCommander.getLaneCommand() != 0) {
				this.routeCommander.setWaitingForRoute(false);
				System.out.println("Route switch: "+this.routeCommander.getLaneCommand());
				return new SwitchLane(this.routeCommander.getLaneCommand(), gameTick);
			}
		}
		
		previousThrottle = myThrottle;
		return new Throttle(myThrottle, gameTick);
	}
	
	@Override
	public void onCrash(Car car) {
		if (car.equals(this.raceState.getMyCar())) {
			previousVelocity = 0.0d;
		}
	}

}
