package noobbot;

import java.io.IOException;

import noobbot.ai.AllBotsUniteAI;
import noobbot.ai.BotAI;
import noobbot.ai.DriftAI;
import noobbot.ai.UberAI;
import noobbot.engine.BotManager;
import noobbot.network.RaceConnection;

public class Main {
	
	// The current default AI. This is used when the C.I. server runs the 
	// main class, so only use the best AI here.
	private static BotAI ai = new UberAI();
	
	// Default arguments used if none are provided. These have no effect
	// when run by the C.I. server, but can be used for quick and easy
	// testing.
	private static String[] defaultArgs = new String[] {
			"hakkinen.helloworldopen.com", // Load balancer address
			"8091", // Port
			"Kamotse " + ai.getName(), // Bot name
			"dtahltD0kVr4xw" // Bot key, do not modify
	};
	
    public static void main(String... args) throws IOException {
    	
    	if(args.length == 0) {
    		args = defaultArgs;
    	}
    	
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        
        new Main(host, port, botName);
        
    }

    public Main(String host, int port, String botName) throws IOException {
    	
    	BotManager botManager = new BotManager(ai);
    	
    	RaceConnection conn;
    	try {
    		conn = new RaceConnection(botManager, host, port);
    		conn.sendJoin(botName);
    	} catch(IOException e) {
    		e.printStackTrace();
    		return;
    	}
    	
    }

}
