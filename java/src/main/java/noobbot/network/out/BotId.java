package noobbot.network.out;

public class BotId {
	private String name;
	private String key;
	
	public BotId(String name, String key) {
		this.name = name;
		this.key = key;
	}
	
	public String getName() {
		return name;
	}
	
	public String getKey() {
		return key;
	}
}
