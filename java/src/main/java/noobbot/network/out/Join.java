package noobbot.network.out;

public class Join extends SendMsg {
	
    public final String name;
    public final String key;

    public Join(final String name, final String key, final int gameTick) {
    	super(gameTick);
        this.name = name;
        this.key = key;
    }

    @Override
    public String msgType() {
        return "join";
    }
    
}