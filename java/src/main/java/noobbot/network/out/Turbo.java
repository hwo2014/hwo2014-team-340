package noobbot.network.out;

public class Turbo extends SendMsg {
	
	public final String value;
	
	public Turbo(String value, int gameTick) {
		super(gameTick);
		this.value = value;
	}
	
	@Override
    public Object msgData() {
        return value;
    }
	
    @Override
    public String msgType() {
        return "turbo";
    }
    
}