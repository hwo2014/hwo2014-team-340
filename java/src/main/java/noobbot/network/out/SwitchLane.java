package noobbot.network.out;

public class SwitchLane extends SendMsg {
    
	public final String value;

    public SwitchLane(String value, int gameTick) {
    	super(gameTick);
        this.value = value;
    }
    
    public SwitchLane(int value, int gameTick) {
    	super(gameTick);
    	
    	switch (value) {
    		case 1:
	    		this.value = "Right";
	    		break;
    		default:
    			this.value = "Left";
    			break;
    	}
    }
    
    @Override
    public Object msgData() {
        return value;
    }
    
    @Override
    public String msgType() {
        return "switchLane";
    }
    
}
