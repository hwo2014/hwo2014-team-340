package noobbot.network.out;

public class Ping extends SendMsg {
	
	public Ping(int gameTick) {
		super(gameTick);
	}
	
    @Override
    public String msgType() {
        return "ping";
    }
    
}