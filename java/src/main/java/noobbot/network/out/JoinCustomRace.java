package noobbot.network.out;

public class JoinCustomRace extends CustomRace{

	public JoinCustomRace(BotId botId, String trackName, String password,
			int carCount, int gameTick) {
		super(botId, trackName, password, carCount, gameTick);
	}
	
	@Override
    public String msgType() {
        return "joinRace";
    }

}
