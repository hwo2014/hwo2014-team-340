package noobbot.network.out;

import noobbot.network.MsgWrapper;

import com.google.gson.Gson;

public abstract class SendMsg {
	
	public transient int gameTick;
	
	public SendMsg(int gameTick) {
		this.gameTick = gameTick;
	}
	
	public String toJson() {
        return new Gson().toJson(new MsgWrapper(
        		msgType(), msgData(), gameTick));
    }

    public Object msgData() {
    	return this;
    }

    public abstract String msgType();
    
}
