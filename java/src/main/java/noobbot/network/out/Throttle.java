package noobbot.network.out;

public class Throttle extends SendMsg {
	
    public final double value;

    public Throttle(double value, int gameTick) {
    	super(gameTick);
        this.value = value;
    }

    @Override
    public Object msgData() {
        return value;
    }
    
    @Override
    public String msgType() {
        return "throttle";
    }
    
}