package noobbot.network.out;

public class CustomRace extends SendMsg{

	private BotId botId;
	private String trackName;
	private String password;
	private int carCount;
	
	public CustomRace(BotId botId, String trackName, String password, int carCount,int gameTick) {
		super(gameTick);
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	
    public BotId getBotId() {
		return botId;
	}

	public String getTrackName() {
		return trackName;
	}

	public String getPassword() {
		return password;
	}

	public int getCarCount() {
		return carCount;
	}

	@Override
    public String msgType() {
        return "createRace";
    }

}
