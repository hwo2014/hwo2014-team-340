package noobbot.network;

public class MsgWrapper {
	
    public final String msgType;
    public final Object data;
    public final int gameTick;

    public MsgWrapper(final String msgType, final Object data, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

}
