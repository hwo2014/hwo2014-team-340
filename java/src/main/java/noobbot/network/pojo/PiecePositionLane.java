package noobbot.network.pojo;

public class PiecePositionLane {

	private int startLaneIndex;
	private int endLaneIndex;
	
	public PiecePositionLane(PiecePositionLane lane) {
		this.startLaneIndex = lane.getStartLaneIndex();
		this.endLaneIndex = lane.getEndLaneIndex();
	}
	
	public PiecePositionLane(int startLaneIndex, int endLaneIndex) {
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}

	public int getStartLaneIndex() {
		return startLaneIndex;
	}
	
	public int getEndLaneIndex() {
		return endLaneIndex;
	}
	
}
