package noobbot.network.pojo;

public class Lane {
	
	private int index;
	private int distanceFromCenter;

	public int getIndex() {
		return index;
	}
	
	public int getDistanceFromCenter() {
		return distanceFromCenter;
	}

}
