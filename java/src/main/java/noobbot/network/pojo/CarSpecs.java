package noobbot.network.pojo;

public class CarSpecs {

	private Car id;
	private CarDimensions dimensions;
	
	public Car getCar() {
		return id;
	}
	
	public CarDimensions getCarDimensions() {
		return dimensions;
	}
	
}
