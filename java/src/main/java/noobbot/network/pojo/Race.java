package noobbot.network.pojo;

public class Race {

	private Track track;
	private CarSpecs[] cars;
	private RaceSession raceSession;
	
	public Track getTrack() {
		return track;
	}
	
	public CarSpecs[] getCars() {
		return cars;
	}
	
	public RaceSession getRaceSession() {
		return raceSession;
	}
	
}
