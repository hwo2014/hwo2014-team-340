package noobbot.network.pojo;

public class PiecePosition {
	
	private int pieceIndex;
	private double inPieceDistance;
	private PiecePositionLane lane;
	private int lap;

	public PiecePosition(PiecePosition pos) {
		this.pieceIndex = pos.getPieceIndex();
		this.inPieceDistance = pos.getInPieceDistance();
		this.lane = new PiecePositionLane(pos.getLane());
		this.lap = pos.getLap();
		
	}
	
	//Used for simulation purposes
	public void updatePosition(int pieceIndex, double inPieceDistance, PiecePositionLane lane) {
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
	}
	
	public void updatePosition(double inPieceDistance) {
		this.inPieceDistance = inPieceDistance;
	}

	public int getPieceIndex() {
		return pieceIndex;
	}
	
	public double getInPieceDistance() {
		return inPieceDistance;
	}
	
	public PiecePositionLane getLane() {
		return lane;
	}
	
	public int getLap() {
		return lap;
	}

}
