package noobbot.network.pojo;



public class LapFinished {
	
	private Car car;
	private LapTime lapTime;
	private RaceTime raceTime;
	private Ranking ranking;
	
	public Car getCar() {
		return car;
	}
	
	public LapTime getLapTime() {
		return lapTime;
	}
	
	public RaceTime getRaceTime() {
		return raceTime;
	}
	
	public Ranking getRanking() {
		return ranking;
	}
	
}
