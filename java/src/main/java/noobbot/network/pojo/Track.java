package noobbot.network.pojo;

import noobbot.engine.RouteFinder;


public class Track {
	
    private String id;
    private String name;
    private TrackPiece[] pieces;
    private Lane[] lanes;
    
    public String getId() {
		return id;
	}
    
    public String getName() {
		return name;
	}
    
    public TrackPiece[] getPieces() {
		return pieces;
	}
    
    public void outputTrack() {
    	for (Lane lane : lanes) {
    		System.out.println("Lane "+lane.getIndex()+" "+lane.getDistanceFromCenter());
    	}
    	
    	for (TrackPiece piece: pieces) {
    		System.out.print("\nRadius: "+piece.getRadius()+" Angle: "+piece.getAngle());
    		if (piece.getLength() > 0)
    			System.out.print(" Length: "+piece.getLength());
    		
    		if (piece.hasLaneSwitch() == true) {
				System.out.print(" SWITCH ");
			}		
		}
    }
    
    public int getNextPieceIndex(int currentPiece) {
    	if (currentPiece < pieces.length-1)
    		return currentPiece+1;
    	else
    		return 0;
    }
    
    public int getPrevPieceIndex(int currentPiece) {
    	if (currentPiece == 0)
    		return pieces.length-1;
    	else
    		return currentPiece - 1;
    }
    
    public TrackPiece getNextPiece(int currentPiece) {    	
    	return this.pieces[this.getNextPieceIndex(currentPiece)];
    }
    
    public TrackPiece getPrevPiece(int currentPiece) {    	
    	return this.pieces[this.getPrevPieceIndex(currentPiece)];
    }
    
    //return distance to next turn and its index
    public double[] getDistanceToNextTurn(int currentPiece, double inPieceDistance, int lane) {
    	double distance = this.getDistanceToPieceEnd(currentPiece, inPieceDistance, lane);
    	
    	int i = currentPiece;
    	int iterationCount = 0;
    	while (iterationCount <= pieces.length) {
    		
    		if (i >= pieces.length )
    			i = 0;
    		
    		double currentRadius = this.pieces[i].getRadius();
    		double currentAngle = this.pieces[i].getAngle();
    		
    		if ((this.pieces[i].isStraight() && !this.getNextPiece(i).isStraight()) || 
    				(
	    				!this.pieces[i].isStraight() && 
	    				!this.getNextPiece(i).isStraight() && 
	    				(this.getNextPiece(i).getRadius() != currentRadius 
	    					|| Math.signum(this.getNextPiece(i).getAngle()) != Math.signum(currentAngle)
	    				)
    				)
    			)
    			break;
    		
    		distance += RouteFinder.calculatePieceLength(this.getNextPiece(i), lane, this.lanes); 
    		
    		iterationCount++;
    		i++;
    	}
    	
    	double[] tuple = {distance,i+1};
    	return tuple;
    }
    
    public double getDistanceToPieceEnd(int currentPiece, double inPieceDistance, int lane) {
    	double currentPieceLength = RouteFinder.calculatePieceLength(pieces[currentPiece], lane, this.lanes);
    	return currentPieceLength - inPieceDistance;	
    }
    
    public double getDistanceToNextStraight(int currentPiece, double inPieceDistance, int lane) {
    	
    	double distance = this.getDistanceToPieceEnd(currentPiece, inPieceDistance, lane);
    	
    	int i = currentPiece;
    	int iterationCount = 0;
    	while (iterationCount <= pieces.length) {
    		
    		if (i >= pieces.length )
    			i = 0;
    		
    		if (this.getNextPiece(i).isStraight() )
    			break;
    		
    		distance += RouteFinder.calculatePieceLength(this.getNextPiece(i), lane, this.lanes); 
    		
    		iterationCount++;
    		i++;
    	}
    	
    	return distance;
    }
    
    public Lane[] getLanes() {
		return lanes;
	}
    
 
    /*public double getVelocityFromPositions(PiecePosition currentPos, PiecePosition prevPosition) {
    	double distance = 0.0d;
    	
    	if (currentPos.getPieceIndex() == prevPosition.getPieceIndex()) {
    		distance = currentPos.getInPieceDistance() - prevPosition.getInPieceDistance();
    	}
    	else {
    		distance = currentPos.getInPieceDistance();
    		//if (prevPosition.getLane().getStartLaneIndex() ==  prevPosition.getLane().getEndLaneIndex()) {
    		//	track
    		//}
    		//else {
    		//	
    		//}
    	}
    	
    	return distance; //Tick is assumed to be one so the traveled distance is the speed
    }*/
    
	public double getAngularAcceleration(double velocity, int pieceIndex, int startLane, int endLane) {
		if (startLane == endLane) {
			if (pieces[pieceIndex].getRadius() != 0) {			
				return Math.signum(pieces[pieceIndex].getAngle())*velocity*velocity/pieces[pieceIndex].getAdjustedRadius(startLane, lanes);
			}
			else
				return 0;
		}
		else {
			//FIXME using the average radius is not the correct way. Try to find out the real radius
			if (pieces[pieceIndex].getRadius() != 0) {
				double averageRadius = (pieces[pieceIndex].getAdjustedRadius(startLane, lanes) + 
						pieces[pieceIndex].getAdjustedRadius(endLane, lanes))/2.0d;
				return Math.signum(pieces[pieceIndex].getAngle())*velocity*velocity/averageRadius;
			}
			else
				return 0; //GUESS
		}
	}

	public void updatePosition(PiecePosition simulatedPos,
			double velocity, PiecePositionLane laneForNewPiece) {
		
		double newPosition = simulatedPos.getInPieceDistance()+velocity;
		
		TrackPiece myCurrentPiece = pieces[simulatedPos.getPieceIndex()];
		double routeDistance = myCurrentPiece.getRouteDistance(
				simulatedPos.getLane().getStartLaneIndex(), 
				simulatedPos.getLane().getEndLaneIndex(), lanes);
		if (newPosition < routeDistance)
			simulatedPos.updatePosition(newPosition);
		else
			simulatedPos.updatePosition(this.getNextPieceIndex(simulatedPos.getPieceIndex()), 
					newPosition - routeDistance, laneForNewPiece);
	}
    
}
