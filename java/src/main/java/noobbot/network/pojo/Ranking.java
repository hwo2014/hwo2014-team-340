package noobbot.network.pojo;

public class Ranking {

	private int overall;
	private int fastestLap;
	
	public int getOverall() {
		return overall;
	}
	
	public int getFastestLap() {
		return fastestLap;
	}
	
}
