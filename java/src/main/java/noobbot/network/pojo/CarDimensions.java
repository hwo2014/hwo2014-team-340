package noobbot.network.pojo;

public class CarDimensions {

	private float length;
	private float width;
	private float guideFlagPosition;
	
	public float getLength() {
		return length;
	}
	
	public float getWidth() {
		return width;
	}
	
	public float getGuideFlagPosition() {
		return guideFlagPosition;
	}
	
}
