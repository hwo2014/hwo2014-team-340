package noobbot.network.pojo;

public class GameEnd {
	private BestLap[] bestLaps;
	private GameResult[] results;
	
	public BestLap[] getBestLaps() {
		return bestLaps;
	}
	public GameResult[] getResults() {
		return results;
	}
}
