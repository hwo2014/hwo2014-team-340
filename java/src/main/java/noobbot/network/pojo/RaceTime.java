package noobbot.network.pojo;

public class RaceTime {

	private int laps;
	private int ticks;
	private long millis;
	
	public int getLaps() {
		return laps;
	}
	
	public int getTicks() {
		return ticks;
	}
	
	public long getMillis() {
		return millis;
	}
	
}
