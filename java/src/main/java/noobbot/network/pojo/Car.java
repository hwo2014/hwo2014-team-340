package noobbot.network.pojo;

public class Car {

	private String name;
	private String color;
	
	public String getName() {
		return name;
	}
	
	public String getColor() {
		return color;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || !(obj instanceof Car)) {
			return false;
		}
		return name.equals(((Car)obj).getName());
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
}
