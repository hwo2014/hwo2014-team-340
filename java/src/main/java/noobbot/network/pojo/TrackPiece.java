package noobbot.network.pojo;

import noobbot.engine.MathHelper;

import com.google.gson.annotations.SerializedName;

public class TrackPiece {
	
	private double length;
	@SerializedName("switch")
	private boolean laneSwitch;
	private int radius;
	private double angle;
	
	private float distanceFromStart;
	
	public double getLength() {
		return length;
	}
	
	public boolean hasLaneSwitch() {
		return laneSwitch;
	}
	
	public int getRadius() {
		return radius;
	}
	
	public int getAdjustedRadius(int lane, Lane[] lanes) {
		return (int) (this.radius-Math.signum(this.angle)*lanes[lane].getDistanceFromCenter());
	}
	
	public boolean isStraight() {
		if (this.radius == 0)
			return true;
		
		return false;
	}
	
	public double getAngle() {
		return angle;
	}
	
	public void setDistanceFromStart(float distanceFromStart){
		this.distanceFromStart = distanceFromStart;
	}
	
	public float getDistanceFromStart(){
		return this.distanceFromStart;
	}
	
	public boolean isDistanceExact(int startLane, int endLane) {
		if (startLane == endLane) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public double getRouteDistance(int startLane, int endLane, Lane[] lanes) {
		if (startLane == endLane) {
			if (this.radius == 0)
				return this.length;
			else
				return MathHelper.calculateArcLength(angle, getAdjustedRadius(startLane, lanes));
		}
		else {
			if (this.radius == 0) {
				double laneDifference = lanes[startLane].getDistanceFromCenter()-lanes[endLane].getDistanceFromCenter();
				return Math.sqrt(this.length*this.length+laneDifference*laneDifference);
			}
			else {				
				//use two secants. It's not the true value but should be close enough
				double a = getAdjustedRadius(startLane, lanes);
				double b = (getAdjustedRadius(startLane, lanes) + getAdjustedRadius(endLane, lanes))/2.0d;
				double c = getAdjustedRadius(endLane, lanes);
				return Math.sqrt(a*a+b*b-2*a*b*Math.cos((Math.abs(angle/2) / 360)*2*Math.PI)) +
						Math.sqrt(c*c+b*b-2*c*b*Math.cos((Math.abs(angle/2) / 360)*2*Math.PI));
			}
		}
	}
		
}
