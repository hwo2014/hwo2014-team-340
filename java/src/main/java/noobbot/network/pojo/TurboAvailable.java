package noobbot.network.pojo;

public class TurboAvailable {
	
	private double turboDurationMilliseconds;
	private int turboDurationTicks;
	private double turboFactor;

	
	public double getturboDurationMilliseconds() {
		return this.turboDurationMilliseconds;
	}
	
	public int getTurboDurationTicks() {
		return this.turboDurationTicks;
	}
	
	public double getTurboFactor() {
		return this.turboFactor;
	}
		
}
