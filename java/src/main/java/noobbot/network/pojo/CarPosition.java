package noobbot.network.pojo;



public class CarPosition {
	
	private Car id;
	private float angle;
	private PiecePosition piecePosition;

	public Car getId() {
		return id;
	}
	
	public float getAngle() {
		return angle;
	}
	
	public PiecePosition getPiecePosition() {
		return piecePosition;
	}
	
}
