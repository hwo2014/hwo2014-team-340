package noobbot.network.pojo;

public class LapTime {
	
	private int lap;
	private int ticks;
	private long millis;
	
	@Override
	public String toString() {
	    return String.valueOf(this.millis / 1000.0)+" ("+ticks+" ticks)";
	}

	
	public int getLap() {
		return lap;
	}
	
	public int getTicks() {
		return ticks;
	}
	
	public long getMillis() {
		return millis;
	}
		
}
