package noobbot.network;

import noobbot.network.out.SendMsg;
import noobbot.network.pojo.BestLap;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.CarSpecs;
import noobbot.network.pojo.GameResult;
import noobbot.network.pojo.LapTime;
import noobbot.network.pojo.RaceSession;
import noobbot.network.pojo.RaceTime;
import noobbot.network.pojo.Ranking;
import noobbot.network.pojo.Track;
import noobbot.network.pojo.TurboAvailable;

public class ConnectionListenerAdapter implements ConnectionListener {

	public void onRawMessage(MsgWrapper rawMsg){}
	public SendMsg onGameTick(int gameTick){return null;}
	public void onYourCar(Car car){}
	public void onGameInit(Track track, CarSpecs[] cars, RaceSession session){}
	public void onGameStart(){}
	public void onCarPositions(CarPosition[] carPositions){}
	public void onCrash(Car car){}
	public void onSpawn(Car car){}
	public void onTurboAvailable(TurboAvailable turbo) {}
	public void onLapFinished(Car car, LapTime lapTime, RaceTime raceTime, Ranking ranking){}
	public void onDnf(Car car, String reason){}
	public void onFinish(Car car){}
	public void onGameEnd(GameResult[] results, BestLap[] bestLaps){}
	public void onTournamentEnd(){}
	
}
