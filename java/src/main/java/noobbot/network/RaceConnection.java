package noobbot.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.network.out.BotId;
import noobbot.network.out.CustomRace;
import noobbot.network.out.Join;
import noobbot.network.out.JoinCustomRace;
import noobbot.network.out.Ping;
import noobbot.network.out.SendMsg;
import noobbot.network.pojo.Car;
import noobbot.network.pojo.CarPosition;
import noobbot.network.pojo.GameEnd;
import noobbot.network.pojo.GameInit;
import noobbot.network.pojo.LapFinished;
import noobbot.network.pojo.Race;
import noobbot.network.pojo.TurboAvailable;

import com.google.gson.Gson;

public class RaceConnection {

	private final static String BOT_KEY = "dtahltD0kVr4xw";
	
	private final Gson gson = new Gson();
	
	private String host;
	private int port;
	
	private PrintWriter writer;
	private BufferedReader reader;
	
	private int currentTick = -1;
	private ConnectionListener listener;
	
	public RaceConnection(ConnectionListener listener, 
			String host, int port) throws IOException {
		this.listener = listener;
		this.host = host;
		this.port = port;
		
		final Socket socket = new Socket(host, port);
		
        writer = new PrintWriter(
        		new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        reader = new BufferedReader(
        		new InputStreamReader(socket.getInputStream(), "utf-8"));
        
	}
	
	public void createCustomRace(String botName, String trackName, String password, int carCount) throws IOException {
		
        CustomRace create = new CustomRace(new BotId(botName, BOT_KEY), trackName, "", carCount, 0);
        
        System.err.println("Creating " + host + ":" + port + " as " +
				botName + " / " + BOT_KEY);
        
        send(create);
        
        startListening();
        
	}
	
	public void joinCustomRace(String botName, String trackName, String password, int carCount) throws IOException {

        JoinCustomRace join = new JoinCustomRace(new BotId(botName, BOT_KEY), trackName, "", carCount, 0);
        
        System.err.println("Joining " + host + ":" + port + " as " +
				botName + " / " + BOT_KEY);
        
        send(join);
        
        startListening();
        
	}
	
	// Quick race
	public void sendJoin(String botName) throws IOException {
		
        Join join = new Join(botName, BOT_KEY, 0);
        
        System.err.println("Joining " + host + ":" + port + " as " +
				botName + " / " + BOT_KEY);
        
        send(join);
        
        startListening();
        
	}

	private void startListening() throws IOException {
		System.err.println("Listening for network input");
        String line = null;
        while((line = reader.readLine()) != null) {
        	MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
        	SendMsg response = processMsg(msgFromServer);
        	if(response != null) {
        		send(response);
        	}
        }
        System.err.println("Network connection has closed");
	}
	
	private void send(SendMsg msg) {
//		System.err.println("sending " + msg.toJson());
		writer.println(msg.toJson());
		writer.flush();
	}
	
	private SendMsg processMsg(MsgWrapper msg) {
		
//		System.err.println(msg.msgType + " " + msg.data);
		
		SendMsg response = new Ping(msg.gameTick);
		
		listener.onRawMessage(msg);
		
		if("yourCar".equals(msg.msgType)) {

			final Car yourCar = gson.fromJson(
					gson.toJson(msg.data), Car.class);
			listener.onYourCar(yourCar);
			
		} else if("gameInit".equals(msg.msgType)) {
			
			final GameInit gameInit = gson.fromJson(
					gson.toJson(msg.data), GameInit.class);
			
			final Race race = gameInit.getRace();
			listener.onGameInit(race.getTrack(), race.getCars(), race.getRaceSession());
			
		} else if("carPositions".equals(msg.msgType)) {
			
			final CarPosition[] positions = gson.fromJson(
					gson.toJson(msg.data), CarPosition[].class);
			listener.onCarPositions(positions);
			
		} else if("crash".equals(msg.msgType)) {
			
			final Car crashedCar = gson.fromJson(
					gson.toJson(msg.data), Car.class);
			listener.onCrash(crashedCar);
			
		} else if("spawn".equals(msg.msgType)) {
			
			final Car spawnedCar = gson.fromJson(
					gson.toJson(msg.data), Car.class);
			listener.onSpawn(spawnedCar);
			
		} else if("lapFinished".equals(msg.msgType)) {
			
			final LapFinished lapFinished = gson.fromJson(
					gson.toJson(msg.data), LapFinished.class);
			listener.onLapFinished(lapFinished.getCar(),
					lapFinished.getLapTime(), lapFinished.getRaceTime(), 
					lapFinished.getRanking());
			
		} else if("turboAvailable".equals(msg.msgType)) {
			
			final TurboAvailable turbo = gson.fromJson(
					gson.toJson(msg.data), TurboAvailable.class);
			listener.onTurboAvailable(turbo);
			
		} else if("dnf".equals(msg.msgType)) {
			
			// TODO
			
		} else if("finish".equals(msg.msgType)) {
			
			// TODO
			
		} else if("gameEnd".equals(msg.msgType)) {
			
			final GameEnd gameEnd = gson.fromJson(
					gson.toJson(msg.data), GameEnd.class);
			
			listener.onGameEnd(gameEnd.getResults(), gameEnd.getBestLaps());
			
			currentTick = -1;
			
		} else if("tournamentEnd".equals(msg.msgType)) {
			
			// TODO
			
		} else if("error".equals(msg.msgType)) {
			
			System.err.println("Error: " + msg.data);
			
		}
		
		// We want to first process the input and then choose output
		int gameTick = msg.gameTick;
		
		// Do not advance to the first tick before the gameStart event.
		// Otherwise the server does not recognize the input and the
		// client times out
		if(gameTick == 0 && !"gameStart".equals(msg.msgType)) {
			return response;
		}
		
		//TURBO MESSAGE causes problems when it advances currentTick, dirty fix
		//if(gameTick > currentTick) {
		if ("carPositions".equals(msg.msgType)) {
			currentTick = gameTick;
			response = listener.onGameTick(currentTick);
		}
		
		return response;
		
	}
	
}
