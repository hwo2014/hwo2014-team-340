package noobbot.main;

import noobbot.ai.AllBotsUniteAI;
import noobbot.ai.AngleAI;
import noobbot.ai.BotAI;
import noobbot.ai.DriftAI;
import noobbot.ai.Mika4AI;
import noobbot.ai.Mika5AI;
import noobbot.ai.SpeedAI;
import noobbot.ai.UberAI;

public class RaceConstants {

	public final static String HOST = "hakkinen.helloworldopen.com";
	public final static int PORT = 8091;
	public final static String BOT_NAME = "Kamotse";
	
	public final static String TRACK = "suzuka";
	public final static String PASSWORD = "pass";
	
	public final static BotAI AI = new UberAI();
	
	public final static BotAI[] AIS = new BotAI[] {
		new AllBotsUniteAI(),
		new DriftAI(),
		new UberAI(),
		new SpeedAI()
	};
	
}
