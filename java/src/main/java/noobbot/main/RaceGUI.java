package noobbot.main;

import java.io.IOException;

import noobbot.gui.SplashUI;

public class RaceGUI {

	public static void main(String... args) throws IOException {
    	
    	new RaceGUI(RaceConstants.HOST, RaceConstants.PORT);
    	
    }
    
    public RaceGUI(String host, int port) {
    	
    	SplashUI gui = new SplashUI();
    	
    	gui.showUI();
    	
    }
}
