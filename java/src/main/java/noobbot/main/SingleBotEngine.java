package noobbot.main;

import java.io.IOException;

import noobbot.engine.BotManager;
import noobbot.network.RaceConnection;

public class SingleBotEngine {
	
	RaceConnection conn;
	
    public static void main(String... args) throws IOException {
    	
    	new SingleBotEngine(RaceConstants.HOST, RaceConstants.PORT).race();
    	
    }
    
    public SingleBotEngine(String host, int port) {
    	
    	BotManager botManager = new BotManager(RaceConstants.AI);
    	
    	try {
    		conn = new RaceConnection(botManager, host, port);
    	} catch(IOException e) {
    		e.printStackTrace();
    		return;
    	}
    	
    }
    
    public void race() {
		try {
			conn.createCustomRace(
					RaceConstants.AI.getName(), 
					RaceConstants.TRACK, 
					RaceConstants.PASSWORD, 
					1);
			conn.joinCustomRace(
					RaceConstants.AI.getName(), 
					RaceConstants.TRACK, 
					RaceConstants.PASSWORD, 
					1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
