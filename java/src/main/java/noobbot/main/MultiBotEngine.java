package noobbot.main;

import java.io.IOException;

import noobbot.engine.BotManager;
import noobbot.network.RaceConnection;

public class MultiBotEngine {
	
	final int botCount = RaceConstants.AIS.length;
	final RaceConnection[] conns = new RaceConnection[botCount];
	
	private BotManager[] botManagers;
	
    public static void main(String... args) throws IOException {
    	
    	new MultiBotEngine(RaceConstants.HOST, RaceConstants.PORT).race();
    	
    }
    
    public MultiBotEngine(String host, int port) {
    	
    	botManagers = new BotManager[botCount];
    	for(int i = 0; i < botCount; i++) {
    		botManagers[i] = new BotManager(RaceConstants.AIS[i]);
    	}
    	
		for(int i = 0; i < botCount; i++) {
			try {
				conns[i] = new RaceConnection(botManagers[i], host, port);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    	
    }
    
    public void race() {
    	
    	for(int i = 0; i < botCount; i++) {

    		final int index = i;
    		// Join the game in a thread as the connections are blocking
			new Thread(new Runnable() {
				@Override
				public void run() {	
					try {
						String name = RaceConstants.AIS[index].getName();
						if(index == 0) {
							conns[index].createCustomRace(
									name,
									RaceConstants.TRACK, 
									RaceConstants.PASSWORD, 
									botCount - 1);
						} else {
							conns[index].joinCustomRace(
									name,
									RaceConstants.TRACK, 
									RaceConstants.PASSWORD, 
			        				botCount - 1);
						}
					} catch(IOException e) {
						e.printStackTrace();
						return;
					}
				}
			}).start();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
    	}
    }

}
