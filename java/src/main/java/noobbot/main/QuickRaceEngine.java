package noobbot.main;

import java.io.IOException;

import noobbot.engine.BotManager;
import noobbot.network.RaceConnection;

public class QuickRaceEngine {
	
	private RaceConnection conn;
	
    public static void main(String... args) throws IOException {
    	
    	new QuickRaceEngine(RaceConstants.HOST, RaceConstants.PORT).race();
    	
    }
    
    public QuickRaceEngine(String host, int port) {
    	
    	BotManager botManager = new BotManager(RaceConstants.AI);
    	
    	try {
    		conn = new RaceConnection(botManager, host, port);
    	} catch(IOException e) {
    		e.printStackTrace();
    		return;
    	}
    	
    }
    
    public void race() {
    	try {
			conn.sendJoin(RaceConstants.AI.getName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
